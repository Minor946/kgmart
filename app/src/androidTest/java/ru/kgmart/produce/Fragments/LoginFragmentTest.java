package ru.kgmart.produce.Fragments;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.kgmart.produce.LoginActivity;
import ru.kgmart.produce.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class LoginFragmentTest {

    @Rule
    public ActivityTestRule<LoginActivity> mActivityRule =
            new ActivityTestRule<>(LoginActivity.class);


    @Test
    public void testLogin() {
        onView(withId(R.id.email))
                .perform(typeText("mr.946@mail.ru"), closeSoftKeyboard());

        onView(withId(R.id.password))
                .perform(typeText("123n"), closeSoftKeyboard());

        onView(withId(R.id.email_sign_in_button))
                .perform(click());
    }




    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before LoginActivity.class");
    }

    @AfterClass
    public  static void afterClass() {
        System.out.println("After LoginActivity.class");
    }

}