package ru.kgmart.produce;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.datetimepicker.date.DatePickerDialog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.metrics.AddTrace;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.ClaimPhotoFragment;
import ru.kgmart.produce.Fragments.GoodsFormFragment;
import ru.kgmart.produce.Fragments.MainFragment;
import ru.kgmart.produce.Models.BookkeepingsModel;
import ru.kgmart.produce.Models.RestResponse;


/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "TestLog";
    private static final int REQUEST_CODE_IMAGE = 2;
    boolean doubleBackToExitPressedOnce = false;
    FloatingActionButton fab;
    Toolbar toolbar;
    String select_date = null;


    @Override
    @AddTrace(name = "onCreateTraceMain", enabled = true)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.app_name));

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openBottomSheet(view);
            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (FirebaseInstanceId.getInstance().getToken() != null) {
            MyFirebaseInstanceIDService.sendRegistrationToServer(Objects.requireNonNull(FirebaseInstanceId.getInstance().getToken()), getApplicationContext());
        } else {
            Log.d(TAG, "onCreate: token_push" + FirebaseInstanceId.getInstance().getToken());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                Log.d(TAG, "onDrawerClosed: close");
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d(TAG, "onDrawerClosed: open");
                CommonComponents.updateNotificationCount(getApplicationContext(), navigationView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        CommonComponents.initNavigation(getApplicationContext(), navigationView);

        MainFragment mainFragment = new MainFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainFrame, mainFragment, "Main")
                .commit();
    }


    public void openBottomSheet(View v) {

        View view = getLayoutInflater().inflate(R.layout.bottom_menu_select, null);

        LinearLayout ll_photo = (LinearLayout) view.findViewById(R.id.ll_schedule_photo);
        LinearLayout ll_goods = (LinearLayout) view.findViewById(R.id.ll_add_goods);
        LinearLayout ll_wallet = (LinearLayout) view.findViewById(R.id.ll_add_wallet);

        final Dialog mBottomSheetDialog = new Dialog(MainActivity.this, R.style.MaterialDialogSheet);
        mBottomSheetDialog.setContentView(view);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
        mBottomSheetDialog.show();

        ll_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                openFragment(new ClaimPhotoFragment(), "claimSchedule");
            }
        });

        ll_goods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
                openFragment(new GoodsFormFragment(), "goodForm");
            }
        });

        ll_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> disabledDate = new ArrayList<>();
                APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);
                Call<RestResponse<BookkeepingsModel>> callDisabled = apiOther.getDisabledDate(CommonComponents.getUserKey(getApplicationContext()));
                callDisabled.enqueue(new Callback<RestResponse<BookkeepingsModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Response<RestResponse<BookkeepingsModel>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                disabledDate.addAll(response.body().getData().getNewBookkeping().getDisabledDate());
                            } catch (Exception e) {
                                Log.e(TAG, "onResponse: ", e);
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: ", t);
                    }
                });
                Calendar maxDate = Calendar.getInstance();
                maxDate.set(Calendar.MONTH, maxDate.get(Calendar.MONTH) + 2);
                DatePickerDialog finishPicker = new DatePickerDialog();
                finishPicker.setMinDate(Calendar.getInstance());
                finishPicker.setMaxDate(maxDate);

                finishPicker.show(getFragmentManager(), "datePicker");
                finishPicker.registerOnDateChangedListener(new DatePickerDialog.OnDateChangedListener() {
                    @Override
                    public void onDateChanged() {
                        String selectDate = CommonComponents.getMysqlDate(finishPicker.getSelectedDay().getYear(),
                                finishPicker.getSelectedDay().getMonth(),
                                finishPicker.getSelectedDay().getDay(), "-");
                        if (disabledDate.contains(selectDate)) {
                            select_date = null;
                            Toast.makeText(getApplicationContext(), "Эта дата занята, выберите другую", Toast.LENGTH_SHORT).show();
                        } else {
                            select_date = selectDate;
                        }
                    }
                });
                finishPicker.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                        if (!TextUtils.isEmpty(select_date)) {
                            BookkeepingsModel model = new BookkeepingsModel();
                            BookkeepingsModel.Details details = new BookkeepingsModel.Details();
                            details.setAmount("0");
                            details.setBooking_date(select_date);
                            model.setNewBookkeping(details);
                            Call<RestResponse<BookkeepingsModel>> call = apiOther.claimBooking(model, CommonComponents.getUserKey(getApplicationContext()));
                            call.enqueue(new Callback<RestResponse<BookkeepingsModel>>() {
                                @Override
                                public void onResponse(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Response<RestResponse<BookkeepingsModel>> response) {
                                    mBottomSheetDialog.dismiss();
                                    if (response.isSuccessful()) {
                                        Toast.makeText(getApplicationContext(), "Заявка принята. Будет отправлено уведомление о потверждении", Toast.LENGTH_LONG).show();

                                    } else {
                                        Toast.makeText(getApplicationContext(), "Недостаточно средст", Toast.LENGTH_LONG).show();
                                    }
                                    Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
                                }

                                @Override
                                public void onFailure(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Throwable t) {
                                    Log.e(TAG, "onFailure: ", t);
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            String fragment = getSupportFragmentManager().findFragmentById(R.id.mainFrame).getClass().getName();
            if (fragment.equals(MainFragment.class.getName())) {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    this.finishAffinity();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Нажмите еще раз для выхода из приложения", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                getSupportFragmentManager().popBackStack();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
            case R.id.action_faq:
                startActivity(new Intent(getApplicationContext(), FaqActivity.class));
                break;
            case R.id.action_my_photo_session:
                startActivity(new Intent(getApplicationContext(), SchedulePhotoActivity.class));
                break;
            case R.id.action_my_profile:
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.action_statistic:
                startActivity(new Intent(getApplicationContext(), StatisticActivity.class));
                break;
            case R.id.action_my_goods:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_ALL));
                startActivity(intent);
                break;
            case R.id.action_need_to_carry:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_TO_CARRY));
                startActivity(intent);
                break;
            case R.id.action_check_available:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_AVAILABLE));
                startActivity(intent);
                break;
            case R.id.action_my_defects:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_DEFECT));
                intent.putExtra("sub_type", String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK));
                startActivity(intent);
                break;
            case R.id.action_sales:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_SALES));
                startActivity(intent);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void openFragment(Fragment fragment, String tag) {
        fab.hide();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);

        transaction.replace(R.id.mainFrame, fragment, tag);
        transaction.addToBackStack("Main");
        transaction.commit();
    }

    public FloatingActionButton getFab() {
        return this.fab;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMAGE) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("goodForm");
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
