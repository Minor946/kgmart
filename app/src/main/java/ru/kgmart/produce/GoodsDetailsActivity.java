package ru.kgmart.produce;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.perf.metrics.AddTrace;

import java.util.Objects;

import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.GoodsDetailsFragment;

/**
 * Created on 6/28/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsDetailsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST_CODE_IMAGE = 2;

    @Override
    @AddTrace(name = "onCreateTraceGood", enabled = true)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods_details);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle("Продукт");

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                CommonComponents.updateNotificationCount(getApplicationContext(), navigationView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        Bundle extras = getIntent().getExtras();
        String product_id = null;

        if (extras != null) {
            product_id = extras.getString("product_id");
        }

        navigationView.setNavigationItemSelectedListener(this);

        CommonComponents.initNavigation(getApplicationContext(), navigationView);
        if (product_id != null) {
            GoodsDetailsFragment fragment = new GoodsDetailsFragment();
            Bundle bundle = new Bundle();
            bundle.putString("product_id", product_id);
            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainFrame, fragment, "GoodsDetails")
                    .commit();
        } else {
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
            case R.id.action_faq:
                startActivity(new Intent(getApplicationContext(), FaqActivity.class));
                break;
            case R.id.action_my_photo_session:
                startActivity(new Intent(getApplicationContext(), SchedulePhotoActivity.class));
                break;
            case R.id.action_my_profile:
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.action_statistic:
                startActivity(new Intent(getApplicationContext(), StatisticActivity.class));
                break;
            case R.id.action_my_goods:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_ALL));
                startActivity(intent);
                break;
            case R.id.action_need_to_carry:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_TO_CARRY));
                startActivity(intent);
                break;
            case R.id.action_check_available:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_AVAILABLE));
                startActivity(intent);
                break;
            case R.id.action_my_defects:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_DEFECT));
                intent.putExtra("sub_type", String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK));
                startActivity(intent);
                break;
            case R.id.action_sales:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_SALES));
                startActivity(intent);
                break;

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);

        transaction.replace(R.id.mainFrame, fragment, tag);
        transaction.addToBackStack("GoodsDetails");
        transaction.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMAGE) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("goodForm");
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
