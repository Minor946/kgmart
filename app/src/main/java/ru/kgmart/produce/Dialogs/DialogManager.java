package ru.kgmart.produce.Dialogs;

import android.content.Context;
import android.graphics.Color;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Objects;

import ru.kgmart.produce.R;

/**
 * Created on 4/19/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class DialogManager {


  public static MaterialDialog newDialogLoading(Context context) {
    MaterialDialog.Builder builder = new MaterialDialog.Builder(Objects.requireNonNull(context))
            .autoDismiss(false)
            .cancelable(false)
            .customView(R.layout.dialog_loading, true);
    return builder.build();
  }

  public static MaterialDialog.Builder pinCodeDialogBuilder(Context context) {
    return new MaterialDialog.Builder(Objects.requireNonNull(context))
            .content(R.string.content_change_pin)
            .positiveText(R.string.change_pin)
            .autoDismiss(false)
            .negativeText(R.string.remove_pin);
  }

  public static MaterialDialog.Builder goodAvailableDialog(Context context, String content) {
    return new MaterialDialog.Builder(Objects.requireNonNull(context))
            .title("Подтвердите")
            .customView(R.layout.dialog_avalable, true)
            .positiveText("Будет")
            .autoDismiss(false)
            .cancelable(true)
            .negativeText("Удалить")
            .negativeColor(Color.BLACK);
  }
}
