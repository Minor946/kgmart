package ru.kgmart.produce;

import android.content.Context;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Models.PushListModel;
import ru.kgmart.produce.Models.PushTokenModel;
import ru.kgmart.produce.Models.RestResponse;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(refreshedToken, getApplicationContext());
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    public static void sendRegistrationToServer(String token, Context context) {
        Log.d(TAG, "sendRegistrationToServer: " + token);

        APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);

        PushTokenModel pushTokenModel = new PushTokenModel();

        pushTokenModel.setToken(token.trim());
        pushTokenModel.setType(0);

        Call<RestResponse<PushListModel>> call = apiOther.saveToken(pushTokenModel, CommonComponents.getUserKey(context));
        call.enqueue(new Callback<RestResponse<PushListModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<PushListModel>> call, @NonNull Response<RestResponse<PushListModel>> response) {
                Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<PushListModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }
}