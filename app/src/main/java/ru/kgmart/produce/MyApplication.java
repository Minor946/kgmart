package ru.kgmart.produce;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import ru.kgmart.produce.Components.CommonComponents;

/**
 * Created on 5/4/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class MyApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        Crashlytics.setString("user_id", CommonComponents.getUserID(getApplicationContext()));
    }
}
