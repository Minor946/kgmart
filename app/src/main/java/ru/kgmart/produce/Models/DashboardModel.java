package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 4/19/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DashboardModel {

    @SerializedName("access_balance")
    @Expose
    @JsonProperty("access_balance")
    private Integer accessBalance;
    @SerializedName("wait_balance")
    @Expose
    @JsonProperty("wait_balance")
    private Integer waitBalance;
    @SerializedName("all_goods")
    @Expose
    @JsonProperty("all_goods")
    private Integer allGoods;
    @SerializedName("on_consigment")
    @Expose
    @JsonProperty("on_consigment")
    private Integer onConsigment;
    @SerializedName("reception_rate")
    @Expose
    @JsonProperty("reception_rate")
    private Integer receptionRate;
    @SerializedName("on_not_available")
    @Expose
    @JsonProperty("on_not_available")
    private Integer onNotAvailable;
    @SerializedName("on_available")
    @Expose
    @JsonProperty("on_available")
    private Integer onAvailable;
    @SerializedName("count_sold")
    @Expose
    @JsonProperty("count_sold")
    private Integer countSold;
    @SerializedName("summary_sold")
    @Expose
    @JsonProperty("summary_sold")
    private Integer summarySold;
    @SerializedName("count_not_sold")
    @Expose
    @JsonProperty("count_not_sold")
    private Integer countNotSold;
    @SerializedName("summary_not_sold")
    @Expose
    @JsonProperty("summary_not_sold")
    private Integer summaryNotSold;
    @SerializedName("count_defects")
    @Expose
    @JsonProperty("count_defects")
    private Integer countDefects;
    @SerializedName("count_no_carry")
    @Expose
    @JsonProperty("count_no_carry")
    private Integer countNoCarry;
    @SerializedName("count_no_available")
    @Expose
    @JsonProperty("count_no_available")
    private Integer countNoAvailable;
    @SerializedName("version")
    @Expose
    @JsonProperty("version")
    private Integer version;
    @SerializedName("mount")
    @Expose
    private String mount;
    @SerializedName("alert")
    @Expose
    @JsonProperty("alert")
    private String alert;
    @SerializedName("ads_show")
    @Expose
    @JsonProperty("ads_show")
    private Boolean adsShow;
    @SerializedName("ads_url")
    @Expose
    @JsonProperty("ads_url")
    private String adsUrl;
    @SerializedName("graphics")
    @Expose
    private List<GraphicModel> graphics = null;

    public Integer getAccessBalance() {
        return accessBalance;
    }

    public void setAccessBalance(Integer accessBalance) {
        this.accessBalance = accessBalance;
    }

    public Integer getWaitBalance() {
        return waitBalance;
    }

    public void setWaitBalance(Integer waitBalance) {
        this.waitBalance = waitBalance;
    }

    public Integer getAllGoods() {
        return allGoods;
    }

    public void setAllGoods(Integer allGoods) {
        this.allGoods = allGoods;
    }

    public Integer getOnConsigment() {
        return onConsigment;
    }

    public void setOnConsigment(Integer onConsigment) {
        this.onConsigment = onConsigment;
    }

    public Integer getOnNotAvailable() {
        return onNotAvailable;
    }

    public void setOnNotAvailable(Integer onNotAvailable) {
        this.onNotAvailable = onNotAvailable;
    }

    public Integer getOnAvailable() {
        return onAvailable;
    }

    public void setOnAvailable(Integer onAvailable) {
        this.onAvailable = onAvailable;
    }

    public Integer getCountSold() {
        return countSold;
    }

    public void setCountSold(Integer countSold) {
        this.countSold = countSold;
    }

    public Integer getSummarySold() {
        return summarySold;
    }

    public void setSummarySold(Integer summarySold) {
        this.summarySold = summarySold;
    }

    public Integer getCountNotSold() {
        return countNotSold;
    }

    public void setCountNotSold(Integer countNotSold) {
        this.countNotSold = countNotSold;
    }

    public Integer getSummaryNotSold() {
        return summaryNotSold;
    }

    public void setSummaryNotSold(Integer summaryNotSold) {
        this.summaryNotSold = summaryNotSold;
    }

    public Integer getCountDefects() {
        return countDefects;
    }

    public void setCountDefects(Integer countDefects) {
        this.countDefects = countDefects;
    }

    public Integer getCountNoCarry() {
        return countNoCarry;
    }

    public void setCountNoCarry(Integer countNoCarry) {
        this.countNoCarry = countNoCarry;
    }

    public Integer getCountNoAvailable() {
        return countNoAvailable;
    }

    public void setCountNoAvailable(Integer countNoAvailable) {
        this.countNoAvailable = countNoAvailable;
    }

    public Integer getReceptionRate() {
        return receptionRate;
    }

    public void setReceptionRate(Integer receptionRate) {
        this.receptionRate = receptionRate;
    }

    public String getMount() {
        return mount;
    }

    public void setMount(String mount) {
        this.mount = mount;
    }

    public List<GraphicModel> getGraphics() {
        return graphics;
    }

    public void setGraphics(List<GraphicModel> graphics) {
        this.graphics = graphics;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public Boolean getAdsShow() {
        return adsShow;
    }

    public void setAdsShow(Boolean adsShow) {
        this.adsShow = adsShow;
    }

    public String getAdsUrl() {
        return adsUrl;
    }

    public void setAdsUrl(String adsUrl) {
        this.adsUrl = adsUrl;
    }
}
