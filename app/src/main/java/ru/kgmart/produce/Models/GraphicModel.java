package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/19/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GraphicModel {

    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("month_end")
    @Expose
    @JsonProperty("month_end")
    private String monthEnd;
    @SerializedName("sum")
    @Expose
    @JsonProperty("sum")
    private String sum;
    @SerializedName("count_sold")
    @Expose
    @JsonProperty("count_sold")
    private Integer countSold;
    @SerializedName("count_not_sold")
    @Expose
    @JsonProperty("count_not_sold")
    private Integer countNotSold;
    @SerializedName("sum_income")
    @Expose
    @JsonProperty("sum_income")
    private Integer sumIncome;
    @SerializedName("sum_sold")
    @Expose
    @JsonProperty("sum_sold")
    private Integer sumSold;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getCountSold() {
        return countSold;
    }

    public void setCountSold(Integer countSold) {
        this.countSold = countSold;
    }

    public Integer getCountNotSold() {
        return countNotSold;
    }

    public void setCountNotSold(Integer countNotSold) {
        this.countNotSold = countNotSold;
    }

    public String getMonthEnd() {
        return monthEnd;
    }

    public void setMonthEnd(String monthEnd) {
        this.monthEnd = monthEnd;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }

    public Integer getSumIncome() {
        return sumIncome;
    }

    public void setSumIncome(Integer sumIncome) {
        this.sumIncome = sumIncome;
    }

    public Integer getSumSold() {
        return sumSold;
    }

    public void setSumSold(Integer sumSold) {
        this.sumSold = sumSold;
    }
}
