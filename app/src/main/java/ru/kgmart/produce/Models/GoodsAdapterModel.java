package ru.kgmart.produce.Models;

/**
 * Created on 4/25/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsAdapterModel {

    private ProductsModels productsModels;

    private WarehouseModel warehouseModel;

    private ReceptionModel receptionModel;

    private ItemAvailableModel itemAvailableModel;

    private ReturnDefectModel returnDefectModel;

    private ReceptionDefectModel receptionDefectModel;

    private ConsigmentModel consigmentModel;

    private TciModel tciModel;

    private StockModel stockModel;

    public ProductsModels getProductsModels() {
        return productsModels;
    }

    public void setProductsModels(ProductsModels productsModels) {
        this.productsModels = productsModels;
    }

    public WarehouseModel getWarehouseModel() {
        return warehouseModel;
    }

    public void setWarehouseModel(WarehouseModel warehouseModel) {
        this.warehouseModel = warehouseModel;
    }

    public ReceptionModel getReceptionModel() {
        return receptionModel;
    }

    public void setReceptionModel(ReceptionModel receptionModel) {
        this.receptionModel = receptionModel;
    }

    public ItemAvailableModel getItemAvailableModel() {
        return itemAvailableModel;
    }

    public void setItemAvailableModel(ItemAvailableModel itemAvailableModel) {
        this.itemAvailableModel = itemAvailableModel;
    }

    public ReturnDefectModel getReturnDefectModel() {
        return returnDefectModel;
    }

    public void setReturnDefectModel(ReturnDefectModel returnDefectModel) {
        this.returnDefectModel = returnDefectModel;
    }

    public ReceptionDefectModel getReceptionDefectModel() {
        return receptionDefectModel;
    }

    public void setReceptionDefectModel(ReceptionDefectModel receptionDefectModel) {
        this.receptionDefectModel = receptionDefectModel;
    }

    public ConsigmentModel getConsigmentModel() {
        return consigmentModel;
    }

    public void setConsigmentModel(ConsigmentModel consigmentModel) {
        this.consigmentModel = consigmentModel;
    }

    public TciModel getTciModel() {
        return tciModel;
    }

    public void setTciModel(TciModel tciModel) {
        this.tciModel = tciModel;
    }

    public StockModel getStockModel() {
        return stockModel;
    }

    public void setStockModel(StockModel stockModel) {
        this.stockModel = stockModel;
    }
}
