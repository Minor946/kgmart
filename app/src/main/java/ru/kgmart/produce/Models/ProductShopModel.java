package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created on 7/3/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductShopModel {

    @JsonProperty("ProductShop")
    private ArrayList<ProductShopItemModel> productShopItemModels;

    public ArrayList<ProductShopItemModel> getProductShopItemModels() {
        return productShopItemModels;
    }

    public void setProductShopItemModels(ArrayList<ProductShopItemModel> productShopItemModels) {
        this.productShopItemModels = productShopItemModels;
    }
}
