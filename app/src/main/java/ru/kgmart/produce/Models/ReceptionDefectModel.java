package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 5/8/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReceptionDefectModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("order_item_status")
    @Expose
    private String orderItemStatus;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("number_in_line")
    @Expose
    private String numberInLine;
    @SerializedName("ln")
    @Expose
    private String ln;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("created_author")
    @Expose
    private String createdAuthor;
    @SerializedName("updated_author")
    @Expose
    private String updatedAuthor;
    @SerializedName("tci_user_id")
    @Expose
    private String tciUserId;
    @SerializedName("defect_quantity")
    @Expose
    private String defectQuantity;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("status_tci_reception")
    @Expose
    private String statusTciReception;
    @SerializedName("return_status")
    @Expose
    private String returnStatus;
    @SerializedName("check_status")
    @Expose
    private String checkStatus;
    @SerializedName("incoming_status")
    @Expose
    private String incomingStatus;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("okm_status")
    @Expose
    private String okmStatus;
    @SerializedName("defect_print")
    @Expose
    private String defectPrint;
    @SerializedName("defect_confirm")
    @Expose
    private String defectConfirm;
    @SerializedName("approval")
    @Expose
    private String approval;
    @SerializedName("illation")
    @Expose
    private String illation;
    @SerializedName("dispatch_transit")
    @Expose
    private String dispatchTransit;
    @SerializedName("no_edit")
    @Expose
    private String noEdit;
    @SerializedName("go_self")
    @Expose
    private String goSelf;
    @SerializedName("accessory")
    @Expose
    private List<Integer> accessory = null;
    @SerializedName("structure")
    @Expose
    private List<Integer> structure = null;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("note2")
    @Expose
    private Object note2;
    @SerializedName("note3")
    @Expose
    private String note3;
    @SerializedName("note4")
    @Expose
    private Object note4;
    @SerializedName("note5")
    @Expose
    private Object note5;
    @SerializedName("defect_note")
    @Expose
    @JsonProperty("defect_note")
    private String defectNote;
    @SerializedName("reject_note")
    @Expose
    private Object rejectNote;
    @SerializedName("size_note")
    @Expose
    private String sizeNote;
    @SerializedName("correct_note")
    @Expose
    private Object correctNote;
    @SerializedName("check_date")
    @Expose
    private String checkDate;
    @SerializedName("payment_date")
    @Expose
    private Object paymentDate;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("color_name")
    @Expose
    private String colorName;
    @SerializedName("size_name")
    @Expose
    private String sizeName;
    @SerializedName("accessory_name")
    @Expose
    private String accessoryName;
    @SerializedName("structure_name")
    @Expose
    private String structureName;

    public String getDefectNote() {
        return defectNote;
    }

    public void setDefectNote(String defectNote) {
        this.defectNote = defectNote;
    }
}
