package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemAvailableModel {

    @SerializedName("item_id")
    @Expose
    @JsonProperty("item_id")
    private String itemId;
    @SerializedName("color_id")
    @Expose
    @JsonProperty("color_id")
    private String colorId;
    @SerializedName("color_name")
    @Expose
    @JsonProperty("color_name")
    private String colorName;
    @SerializedName("size_id")
    @Expose
    @JsonProperty("size_id")
    private String sizeId;
    @SerializedName("size_name")
    @Expose
    @JsonProperty("size_name")
    private String sizeName;
    @SerializedName("quantity")
    @Expose
    @JsonProperty("quantity")
    private String quantity;
    @SerializedName("created")
    @Expose
    private String created;

    @JsonProperty("status")
    private String status;

    @JsonProperty("product_id")
    private String productId;


    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
