package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 5/4/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoClaimModel {

    @JsonProperty("PhotoClaim")
    private PhotoClaimModel.Details newClaimDetail;

    public Details getNewClaimDetail() {
        return newClaimDetail;
    }

    public void setNewClaimDetail(Details newClaimDetail) {
        this.newClaimDetail = newClaimDetail;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Details {

        @JsonProperty("id")
        private String id;
        @JsonProperty("user_id")
        private String userId;
        @JsonProperty("shop_id")
        private String shopId;
        @JsonProperty("quantity")
        private String quantity;
        @JsonProperty("status")
        private String status;
        @JsonProperty("place")
        private String place;
        @JsonProperty("date")
        private String date;
        @JsonProperty("created_date")
        private String createdDate;
        @JsonProperty("note")
        private String note;
        @JsonProperty("note_shop")
        private String noteShop;
        @JsonProperty("note_admin")
        private String noteAdmin;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public Object getNoteShop() {
            return noteShop;
        }

        public void setNoteShop(String noteShop) {
            this.noteShop = noteShop;
        }

        public Object getNoteAdmin() {
            return noteAdmin;
        }

        public void setNoteAdmin(String noteAdmin) {
            this.noteAdmin = noteAdmin;
        }
    }

}
