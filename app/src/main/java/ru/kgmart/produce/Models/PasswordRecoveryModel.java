package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordRecoveryModel {

    @JsonProperty("UserRecoveryForm")
    private Details UserRecoveryForm;

    @JsonIgnore
    private String message;

    public Details getUserRecoveryForm() {
        return UserRecoveryForm;
    }

    public void setUserRecoveryForm(Details userRecoveryForm) {
        UserRecoveryForm = userRecoveryForm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Details {

        @JsonProperty("login_or_email")
        private String loginOrEmail;

        public String getLoginOrEmail() {
            return loginOrEmail;
        }

        public void setLoginOrEmail(String loginOrEmail) {
            this.loginOrEmail = loginOrEmail;
        }
    }
}
