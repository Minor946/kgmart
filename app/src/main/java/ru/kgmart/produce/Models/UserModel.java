package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("create_at")
    @Expose
    private String createAt;
    @SerializedName("lastvisit_at")
    @Expose
    private String lastvisitAt;
    @SerializedName("balance")
    @Expose
    private Integer balance;
    @SerializedName("bonus")
    @Expose
    private Integer bonus;
    @SerializedName("bonus_sum")
    @Expose
    private Integer bonusSum;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }

    public String getLastvisitAt() {
        return lastvisitAt;
    }

    public void setLastvisitAt(String lastvisitAt) {
        this.lastvisitAt = lastvisitAt;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public Integer getBonusSum() {
        return bonusSum;
    }

    public void setBonusSum(Integer bonusSum) {
        this.bonusSum = bonusSum;
    }
}
