package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created on 5/8/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscountItemShopModel {

    @JsonProperty("items")
    private ArrayList<String> items;
    @JsonProperty("value")
    private String discountValue;
    @JsonProperty("type")
    private String discountType;
    @JsonProperty("start_date")
    private String discountBegin;
    @JsonProperty("end_date")
    private String discountEnd;
    @JsonProperty("note")
    private String discountDesc;
    @JsonProperty("endless")
    private String endless;

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }

    public String getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(String discountValue) {
        this.discountValue = discountValue;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getDiscountBegin() {
        return discountBegin;
    }

    public void setDiscountBegin(String discountBegin) {
        this.discountBegin = discountBegin;
    }

    public String getDiscountEnd() {
        return discountEnd;
    }

    public void setDiscountEnd(String discountEnd) {
        this.discountEnd = discountEnd;
    }

    public String getDiscountDesc() {
        return discountDesc;
    }

    public void setDiscountDesc(String discountDesc) {
        this.discountDesc = discountDesc;
    }

    public String getEndless() {
        return endless;
    }

    public void setEndless(String endless) {
        this.endless = endless;
    }
}
