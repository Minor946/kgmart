package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FaqListModel {

    @SerializedName("Faq")
    @Expose
    @JsonProperty("Faq")
    private List<FaqModel> faq = null;

    public List<FaqModel> getFaq() {
        return faq;
    }

    public void setFaq(List<FaqModel> faq) {
        this.faq = faq;
    }

}
