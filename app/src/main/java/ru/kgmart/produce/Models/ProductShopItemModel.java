package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created on 7/3/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductShopItemModel {

    @JsonProperty("category_id")
    private String category_id;
    @JsonProperty("product_id")
    private String product_id;
    @JsonProperty("number_in_line")
    private String number_in_line;
    @JsonProperty("name")
    private String name;
    @JsonProperty("name2")
    private String name2;
    @JsonProperty("description")
    private String description;
    @JsonProperty("colors")
    private List<String> colors;
    @JsonProperty("sizes")
    private List<String> sizes;
    @JsonProperty("wholesale")
    private String wholesale;
    @JsonProperty("trade_price")
    private String trade_price;
    @JsonProperty("visible")
    private String visible;
    @JsonProperty("weight")
    private String weight;
    @JsonProperty("price")
    private String price;

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getNumber_in_line() {
        return number_in_line;
    }

    public void setNumber_in_line(String number_in_line) {
        this.number_in_line = number_in_line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getColors() {
        return colors;
    }

    public void setColors(List<String> colors) {
        this.colors = colors;
    }

    public List<String> getSizes() {
        return sizes;
    }

    public void setSizes(List<String> sizes) {
        this.sizes = sizes;
    }

    public String getWholesale() {
        return wholesale;
    }

    public void setWholesale(String wholesale) {
        this.wholesale = wholesale;
    }

    public String getTrade_price() {
        return trade_price;
    }

    public void setTrade_price(String trade_price) {
        this.trade_price = trade_price;
    }

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }
}
