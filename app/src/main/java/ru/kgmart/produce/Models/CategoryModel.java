package ru.kgmart.produce.Models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CategoryModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("parent_id")
    @Expose
    private String parentId;
    @SerializedName("is_menu")
    @Expose
    private String isMenu;
    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title2")
    @Expose
    private String title2;
    @SerializedName("title_h1")
    @Expose
    @JsonProperty("title_h1")
    private String titleH1;
    @SerializedName("sort")
    @Expose
    private String sort;
    @SerializedName("image")
    @Expose
    private Object image;
    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("size_chart")
    @Expose
    private Object sizeChart;
    @SerializedName("meta_title")
    @Expose
    private Object metaTitle;
    @SerializedName("meta_keywords")
    @Expose
    private Object metaKeywords;
    @SerializedName("meta_description")
    @Expose
    private Object metaDescription;
    @SerializedName("add_product")
    @Expose
    private Object addProduct;
    @SerializedName("gost")
    @Expose
    private Object gost;
    @SerializedName("eac")
    @Expose
    private Object eac;
    @SerializedName("spec")
    @Expose
    private Object spec;
    @SerializedName("hits")
    @Expose
    private Object hits;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleH1() {
        return titleH1;
    }

    public void setTitleH1(String titleH1) {
        this.titleH1 = titleH1;
    }

}
