package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/18/18.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PasswordChangeModel {

    @JsonProperty("UserChangePassword")
    private Details newPasswordDetails;

    public Details getNewPasswordDetails() {
        return newPasswordDetails;
    }

    public void setNewPasswordDetails(Details newPasswordDetails) {
        this.newPasswordDetails = newPasswordDetails;
    }

    public class Details {

        private String oldPassword;
        private String password;
        private String verifyPassword;

        public String getOldPassword() {
            return oldPassword;
        }

        public void setOldPassword(String oldPassword) {
            this.oldPassword = oldPassword;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getVerifyPassword() {
            return verifyPassword;
        }

        public void setVerifyPassword(String verifyPassword) {
            this.verifyPassword = verifyPassword;
        }
    }
}
