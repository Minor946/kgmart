package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 5/8/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnDefectModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("author_id")
    @Expose
    private String authorId;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("color_id")
    @Expose
    private String colorId;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("size")
    @Expose
    private String size;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("comment")
    @Expose
    @JsonProperty("comment")
    private String comment;
    @SerializedName("tci_note")
    @Expose
    private String tciNote;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("note2")
    @Expose
    private String note2;
    @SerializedName("note3")
    @Expose
    private String note3;
    @SerializedName("image")
    @Expose
    private List<String> image = null;
    @SerializedName("accepted")
    @Expose
    private String accepted;
    @SerializedName("date_accepted")
    @Expose
    private String dateAccepted;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("hide")
    @Expose
    private String hide;
    @SerializedName("satisfied")
    @Expose
    private List<String> satisfied = null;
    @SerializedName("condition")
    @Expose
    private String condition;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("color_name")
    @Expose
    @JsonProperty("color_name")
    private String colorName;
    @SerializedName("size_name")
    @Expose
    @JsonProperty("size_name")
    private String sizeName;
    @SerializedName("username")
    @Expose
    @JsonProperty("username")
    private String username;
    @SerializedName("status")
    @Expose
    @JsonProperty("status")
    private String status;
    @SerializedName("images")
    @Expose
    @JsonProperty("images")
    private List<String> images = null;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
