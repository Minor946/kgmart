package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class SizeModel {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    @JsonProperty("name")
    private String name;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("name2")
    @Expose
    private Object name2;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("onesize")
    @Expose
    private String onesize;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Object getName2() {
        return name2;
    }

    public void setName2(Object name2) {
        this.name2 = name2;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getOnesize() {
        return onesize;
    }

    public void setOnesize(String onesize) {
        this.onesize = onesize;
    }
}
