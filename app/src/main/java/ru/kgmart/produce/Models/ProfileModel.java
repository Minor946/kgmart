package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileModel {

    @JsonProperty("Profile")
    private UserDetails profile;

    @JsonProperty("Shop")
    private Shop shop;

    private String key;

    public UserDetails getProfile() {
        return profile;
    }

    public void setProfile(UserDetails profile) {
        this.profile = profile;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class UserDetails {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("create_at")
        @Expose
        @JsonProperty("create_at")
        private String createAt;
        @SerializedName("lastvisit_at")
        @Expose
        private String lastvisitAt;
        @SerializedName("balance")
        @Expose
        private Integer balance;
        @SerializedName("bonus")
        @Expose
        private Integer bonus;
        @SerializedName("bonus_sum")
        @Expose
        private Integer bonusSum;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("patronymic")
        @Expose
        private String patronymic;
        @SerializedName("sex")
        @Expose
        private String sex;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("passport")
        @Expose
        private String passport;
        @SerializedName("city_id")
        @Expose
        private String cityId;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("inn")
        @Expose
        private String inn;
        @SerializedName("kpp")
        @Expose
        private String kpp;
        @SerializedName("orgnip")
        @Expose
        private String orgnip;
        @SerializedName("legal_address")
        @Expose
        private String legalAddress;
        @SerializedName("company")
        @Expose
        private String company;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("other_contact")
        @Expose
        @JsonProperty("other_contact")
        private String otherContact;
        @SerializedName("orgn")
        @Expose
        private String orgn;
        @SerializedName("timezone")
        @Expose
        private String timezone;
        @SerializedName("consignee")
        @Expose
        private String consignee;
        @SerializedName("cert_address")
        @Expose
        private String certAddress;
        @SerializedName("inn_type")
        @Expose
        private String innType;
        @SerializedName("mailing")
        @Expose
        private String mailing;
        @SerializedName("type_list")
        @Expose
        private List<String> typeList = null;
        @SerializedName("city")
        @Expose
        private String city;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCreateAt() {
            return createAt;
        }

        public void setCreateAt(String createAt) {
            this.createAt = createAt;
        }

        public String getLastvisitAt() {
            return lastvisitAt;
        }

        public void setLastvisitAt(String lastvisitAt) {
            this.lastvisitAt = lastvisitAt;
        }

        public Integer getBalance() {
            return balance;
        }

        public void setBalance(Integer balance) {
            this.balance = balance;
        }

        public Integer getBonus() {
            return bonus;
        }

        public void setBonus(Integer bonus) {
            this.bonus = bonus;
        }

        public Integer getBonusSum() {
            return bonusSum;
        }

        public void setBonusSum(Integer bonusSum) {
            this.bonusSum = bonusSum;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getPatronymic() {
            return patronymic;
        }

        public void setPatronymic(String patronymic) {
            this.patronymic = patronymic;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getPassport() {
            return passport;
        }

        public void setPassport(String passport) {
            this.passport = passport;
        }

        public String getCityId() {
            return cityId;
        }

        public void setCityId(String cityId) {
            this.cityId = cityId;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getInn() {
            return inn;
        }

        public void setInn(String inn) {
            this.inn = inn;
        }

        public String getKpp() {
            return kpp;
        }

        public void setKpp(String kpp) {
            this.kpp = kpp;
        }

        public String getOrgnip() {
            return orgnip;
        }

        public void setOrgnip(String orgnip) {
            this.orgnip = orgnip;
        }

        public String getLegalAddress() {
            return legalAddress;
        }

        public void setLegalAddress(String legalAddress) {
            this.legalAddress = legalAddress;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getOtherContact() {
            return otherContact;
        }

        public void setOtherContact(String otherContact) {
            this.otherContact = otherContact;
        }

        public String getOrgn() {
            return orgn;
        }

        public void setOrgn(String orgn) {
            this.orgn = orgn;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

        public String getConsignee() {
            return consignee;
        }

        public void setConsignee(String consignee) {
            this.consignee = consignee;
        }

        public String getCertAddress() {
            return certAddress;
        }

        public void setCertAddress(String certAddress) {
            this.certAddress = certAddress;
        }

        public String getInnType() {
            return innType;
        }

        public void setInnType(String innType) {
            this.innType = innType;
        }

        public String getMailing() {
            return mailing;
        }

        public void setMailing(String mailing) {
            this.mailing = mailing;
        }

        public List<String> getTypeList() {
            return typeList;
        }

        public void setTypeList(List<String> typeList) {
            this.typeList = typeList;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Shop {

        @SerializedName("login")
        @Expose
        private String login;

        @SerializedName("condit")
        @Expose
        private String condit;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getCondit() {
            return condit;
        }

        public void setCondit(String condit) {
            this.condit = condit;
        }
    }
}
