package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 8/3/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TciModel {

    @SerializedName("color_id")
    @Expose
    @JsonProperty("color_id")
    private String colorId;
    @SerializedName("color_name")
    @Expose
    @JsonProperty("color_name")
    private String colorName;
    @SerializedName("size_id")
    @Expose
    @JsonProperty("size_id")
    private String sizeId;
    @SerializedName("size_name")
    @Expose
    @JsonProperty("size_name")
    private String sizeName;
    @SerializedName("price")
    @Expose
    @JsonProperty("price")
    private String price;
    @SerializedName("check_date")
    @Expose
    @JsonProperty("check_date")
    private String checkDate;
    @SerializedName("number_in_line")
    @Expose
    @JsonProperty("number_in_line")
    private String numberInLine;
    @SerializedName("number_in_line_old")
    @Expose
    @JsonProperty("number_in_line_old")
    private String numberInLineOld;
    @SerializedName("status")
    @Expose
    @JsonProperty("status")
    private String status;

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public String getNumberInLine() {
        return numberInLine;
    }

    public void setNumberInLine(String numberInLine) {
        this.numberInLine = numberInLine;
    }

    public String getNumberInLineOld() {
        return numberInLineOld;
    }

    public void setNumberInLineOld(String numberInLineOld) {
        this.numberInLineOld = numberInLineOld;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}