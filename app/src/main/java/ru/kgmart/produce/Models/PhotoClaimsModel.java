package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created on 5/4/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PhotoClaimsModel {

    @JsonProperty("PhotoClaim")
    private List<Details> photoClaim = null;

    @JsonProperty("Bookkeeping")
    private List<DetailsBookkeeping> bookkeeping = null;

    public List<Details> getPhotoClaim() {
        return photoClaim;
    }

    public void setPhotoClaim(List<Details> photoClaim) {
        this.photoClaim = photoClaim;
    }

    public List<DetailsBookkeeping> getBookkeeping() {
        return bookkeeping;
    }

    public void setBookkeeping(List<DetailsBookkeeping> bookkeeping) {
        this.bookkeeping = bookkeeping;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DetailsBookkeeping {

        @JsonProperty("type")
        private String type;
        @JsonProperty("amount")
        private String amount;
        @JsonProperty("booking_date")
        private String booking_date;
        @JsonProperty("comment")
        private String comment;
        @JsonProperty("status")
        private String status;
        @JsonProperty("created_at")
        private String createdAt;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getBooking_date() {
            return booking_date;
        }

        public void setBooking_date(String booking_date) {
            this.booking_date = booking_date;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Details {

        @JsonProperty("id")
        private String id;
        @JsonProperty("user_id")
        private String userId;
        @JsonProperty("shop_id")
        private String shopId;
        @JsonProperty("quantity")
        private String quantity;
        @JsonProperty("status")
        private String status;
        @JsonProperty("place")
        private String place;
        @JsonProperty("date")
        private String date;
        @JsonProperty("created_date")
        private String createdDate;
        @JsonProperty("note")
        private String note;
        @JsonProperty("note_shop")
        private String noteShop;
        @JsonProperty("note_admin")
        private String noteAdmin;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getShopId() {
            return shopId;
        }

        public void setShopId(String shopId) {
            this.shopId = shopId;
        }

        public String getQuantity() {
            return quantity;
        }

        public void setQuantity(String quantity) {
            this.quantity = quantity;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPlace() {
            return place;
        }

        public void setPlace(String place) {
            this.place = place;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getCreatedDate() {
            return createdDate;
        }

        public void setCreatedDate(String createdDate) {
            this.createdDate = createdDate;
        }

        public String getNote() {
            return note;
        }

        public void setNote(String note) {
            this.note = note;
        }

        public String getNoteShop() {
            return noteShop;
        }

        public void setNoteShop(String noteShop) {
            this.noteShop = noteShop;
        }

        public String getNoteAdmin() {
            return noteAdmin;
        }

        public void setNoteAdmin(String noteAdmin) {
            this.noteAdmin = noteAdmin;
        }
    }

}
