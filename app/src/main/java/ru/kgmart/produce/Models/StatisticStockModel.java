package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticStockModel {

    @SerializedName("quantity")
    @Expose
    @JsonProperty("quantity")
    private Integer quantity;
    @SerializedName("sum")
    @Expose
    @JsonProperty("sum")
    private Integer sum;
    @SerializedName("sold")
    @Expose
    @JsonProperty("sold")
    private Integer sold;
    @SerializedName("sold_sum")
    @Expose
    @JsonProperty("sold_sum")
    private Integer soldSum;
    @SerializedName("return")
    @Expose
    @JsonProperty("return")
    private Integer returnVal;
    @SerializedName("return_sum")
    @Expose
    @JsonProperty("return_sum")
    private Integer returnSum;
    @SerializedName("ost_quantity")
    @Expose
    @JsonProperty("ost_quantity")
    private Integer ostQuantity;
    @SerializedName("ost_sum")
    @Expose
    @JsonProperty("ost_sum")
    private Integer ostSum;
    @SerializedName("full_sum_get")
    @Expose
    @JsonProperty("full_sum_get")
    private Integer fullSumGet;

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getSoldSum() {
        return soldSum;
    }

    public void setSoldSum(Integer soldSum) {
        this.soldSum = soldSum;
    }

    public Integer getReturnVal() {
        return returnVal;
    }

    public void setReturnVal(Integer return_val) {
        this.returnVal = return_val;
    }

    public Integer getReturnSum() {
        return returnSum;
    }

    public void setReturnSum(Integer returnSum) {
        this.returnSum = returnSum;
    }

    public Integer getOstQuantity() {
        return ostQuantity;
    }

    public void setOstQuantity(Integer ostQuantity) {
        this.ostQuantity = ostQuantity;
    }

    public Integer getOstSum() {
        return ostSum;
    }

    public void setOstSum(Integer ostSum) {
        this.ostSum = ostSum;
    }

    public Integer getFullSumGet() {
        return fullSumGet;
    }

    public void setFullSumGet(Integer fullSumGet) {
        this.fullSumGet = fullSumGet;
    }
}
