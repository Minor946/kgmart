package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class RestResponse<T> {
    //@JsonProperty("meta")
    private Meta meta;
    //@JsonProperty("data")
    private T data;

    private static final String STATUS_ERROR = "ERROR";
    private static final String STATUS_SUCCESS = "SUCCESS";

    public RestResponse() {
        meta = new Meta();
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public enum Status {
        ERROR,
        SUCCESS
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Meta {
        private Status status;
        //@JsonProperty("message")
        private String message;

        private Meta() {
        }

        private Meta(Status status, String message) {
            this.status = status;
            this.message = message;
        }

        public Status getStatus() {
            return status;
        }

        public void setStatus(Status status) {
            this.status = status;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}