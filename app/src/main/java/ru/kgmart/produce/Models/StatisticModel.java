package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticModel {

    @SerializedName("access_balance")
    @Expose
    @JsonProperty("access_balance")
    private Integer accessBalance;
    @SerializedName("wait_balance")
    @Expose
    @JsonProperty("wait_balance")
    private Integer waitBalance;
    @SerializedName("sum_status_consign")
    @Expose
    @JsonProperty("sum_status_consign")
    private Integer sumStatusConsign;
    @SerializedName("sum_status_income")
    @Expose
    @JsonProperty("sum_status_income")
    private Integer sumStatusIncome;
    @SerializedName("sum_status_return")
    @Expose
    @JsonProperty("sum_status_return")
    private Integer sumStatusReturn;
    @SerializedName("sum_status_no_pay")
    @Expose
    @JsonProperty("sum_status_no_pay")
    private Integer sumStatusNoPay;
    @SerializedName("statistic_details")
    @Expose
    @JsonProperty("statistic_details")
    private StatisticDetailsModel statisticDetails;
    @SerializedName("statistic_stock")
    @Expose
    @JsonProperty("statistic_stock")
    private StatisticStockModel statisticStock;
    @SerializedName("diagram_income")
    @Expose
    @JsonProperty("diagram_income")
    private List<GraphicModel> diagramIncome = null;

    @SerializedName("diagram_stock")
    @Expose
    @JsonProperty("diagram_stock")
    private List<GraphicModel> diagramStock = null;

    public Integer getAccessBalance() {
        return accessBalance;
    }

    public void setAccessBalance(Integer accessBalance) {
        this.accessBalance = accessBalance;
    }

    public Integer getWaitBalance() {
        return waitBalance;
    }

    public void setWaitBalance(Integer waitBalance) {
        this.waitBalance = waitBalance;
    }

    public Integer getSumStatusConsign() {
        return sumStatusConsign;
    }

    public void setSumStatusConsign(Integer sumStatusConsign) {
        this.sumStatusConsign = sumStatusConsign;
    }

    public Integer getSumStatusIncome() {
        return sumStatusIncome;
    }

    public void setSumStatusIncome(Integer sumStatusIncome) {
        this.sumStatusIncome = sumStatusIncome;
    }

    public Integer getSumStatusReturn() {
        return sumStatusReturn;
    }

    public void setSumStatusReturn(Integer sumStatusReturn) {
        this.sumStatusReturn = sumStatusReturn;
    }

    public Integer getSumStatusNoPay() {
        return sumStatusNoPay;
    }

    public void setSumStatusNoPay(Integer sumStatusNoPay) {
        this.sumStatusNoPay = sumStatusNoPay;
    }

    public StatisticDetailsModel getStatisticDetails() {
        return statisticDetails;
    }

    public void setStatisticDetails(StatisticDetailsModel statisticDetails) {
        this.statisticDetails = statisticDetails;
    }

    public List<GraphicModel> getDiagramIncome() {
        return diagramIncome;
    }

    public void setDiagramIncome(List<GraphicModel> diagramIncome) {
        this.diagramIncome = diagramIncome;
    }

    public List<GraphicModel> getDiagramStock() {
        return diagramStock;
    }

    public void setDiagramStock(List<GraphicModel> diagramStock) {
        this.diagramStock = diagramStock;
    }

    public StatisticStockModel getStatisticStock() {
        return statisticStock;
    }

    public void setStatisticStock(StatisticStockModel statisticStock) {
        this.statisticStock = statisticStock;
    }
}
