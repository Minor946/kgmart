package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoodsModel {

    @JsonProperty("Products")
    private List<ProductsModels> products;

    @JsonProperty("Count")
    private Integer count;

    @JsonProperty("OrderItemShop")
    private ItemAvailableModel itemAvailableModel;

    @JsonProperty("OrderItemReceptionShop")
    private ReceptionModel receptionModel;

    @JsonProperty("DiscountShop")
    private DiscountItemShopModel discountItemShopModel;

    public List<ProductsModels> getProducts() {
        return products;
    }

    public void setProducts(List<ProductsModels> products) {
        this.products = products;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public ItemAvailableModel getItemAvailableModel() {
        return itemAvailableModel;
    }

    public void setItemAvailableModel(ItemAvailableModel itemAvailableModel) {
        this.itemAvailableModel = itemAvailableModel;
    }

    public DiscountItemShopModel getDiscountItemShopModel() {
        return discountItemShopModel;
    }

    public void setDiscountItemShopModel(DiscountItemShopModel discountItemShopModel) {
        this.discountItemShopModel = discountItemShopModel;
    }

    public ReceptionModel getReceptionModel() {
        return receptionModel;
    }

    public void setReceptionModel(ReceptionModel receptionModel) {
        this.receptionModel = receptionModel;
    }
}
