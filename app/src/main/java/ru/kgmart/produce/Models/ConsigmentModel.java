package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 5/8/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsigmentModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("color_id")
    @Expose
    @JsonProperty("color_id")
    private String colorId;
    @SerializedName("size_id")
    @Expose
    private String sizeId;
    @SerializedName("order_id")
    @Expose
    private Object orderId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("tci_user_id")
    @Expose
    private String tciUserId;
    @SerializedName("number_in_line")
    @Expose
    @JsonProperty("number_in_line")
    private String numberInLine;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("confirm")
    @Expose
    private String confirm;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("invoice")
    @Expose
    private String invoice;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("updated")
    @Expose
    private String updated;
    @SerializedName("finished")
    @Expose
    private String finished;
    @SerializedName("until")
    @Expose
    private String until;
    @SerializedName("receipt")
    @Expose
    private String receipt;
    @SerializedName("color_name")
    @Expose
    @JsonProperty("color_name")
    private String colorName;
    @SerializedName("size_name")
    @Expose
    @JsonProperty("size_name")
    private String sizeName;
    @SerializedName("storage_name")
    @Expose
    @JsonProperty("storage_name")
    private String storageName;


    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public String getNumberInLine() {
        return numberInLine;
    }

    public void setNumberInLine(String numberInLine) {
        this.numberInLine = numberInLine;
    }

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getStorageName() {
        return storageName;
    }

    public void setStorageName(String storageName) {
        this.storageName = storageName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
