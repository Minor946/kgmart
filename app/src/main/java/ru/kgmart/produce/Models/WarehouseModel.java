package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WarehouseModel {

    @SerializedName("color_id")
    @Expose
    @JsonProperty("color_id")
    private String colorId;
    @SerializedName("color_name")
    @Expose
    @JsonProperty("color_name")
    private String colorName;
    @SerializedName("size_id")
    @Expose
    @JsonProperty("size_id")
    private String sizeId;
    @SerializedName("size_name")
    @Expose
    @JsonProperty("size_name")
    private String sizeName;
    @SerializedName("product_count")
    @Expose
    @JsonProperty("product_count")
    private Integer productCount;
    @SerializedName("order_item_count")
    @Expose
    @JsonProperty("order_item_count")
    private Integer orderItemCount;
    @SerializedName("need_to_bring")
    @Expose
    @JsonProperty("need_to_bring")
    private Integer needToBring;

    public String getColorId() {
        return colorId;
    }

    public void setColorId(String colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getSizeId() {
        return sizeId;
    }

    public void setSizeId(String sizeId) {
        this.sizeId = sizeId;
    }

    public String getSizeName() {
        return sizeName;
    }

    public void setSizeName(String sizeName) {
        this.sizeName = sizeName;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    public Integer getOrderItemCount() {
        return orderItemCount;
    }

    public void setOrderItemCount(Integer orderItemCount) {
        this.orderItemCount = orderItemCount;
    }

    public Integer getNeedToBring() {
        return needToBring;
    }

    public void setNeedToBring(Integer needToBring) {
        this.needToBring = needToBring;
    }
}
