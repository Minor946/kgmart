package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationModel {

    @SerializedName("product")
    @Expose
    private Integer product;
    @SerializedName("warehouse")
    @Expose
    private Integer warehouse;
    @SerializedName("reception")
    @Expose
    private Integer reception;
    @SerializedName("item")
    @Expose
    private Integer item;

    @SerializedName("defect")
    @Expose
    private Integer defect;

    public Integer getProduct() {
        return product;
    }

    public void setProduct(Integer product) {
        this.product = product;
    }

    public Integer getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Integer warehouse) {
        this.warehouse = warehouse;
    }

    public Integer getReception() {
        return reception;
    }

    public void setReception(Integer reception) {
        this.reception = reception;
    }

    public Integer getItem() {
        return item;
    }

    public void setItem(Integer item) {
        this.item = item;
    }

    public Integer getDefect() {
        return defect;
    }

    public void setDefect(Integer defect) {
        this.defect = defect;
    }
}
