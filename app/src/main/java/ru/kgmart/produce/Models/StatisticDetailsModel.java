package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticDetailsModel {

    @SerializedName("income")
    @Expose
    @JsonProperty("income")
    private StatisticDiffModel income;
    @SerializedName("consign")
    @Expose
    @JsonProperty("consign")
    private StatisticDiffModel consign;
    @SerializedName("full_sum")
    @Expose
    @JsonProperty("full_sum")
    private String fullSum;

    public StatisticDiffModel getIncome() {
        return income;
    }

    public void setIncome(StatisticDiffModel income) {
        this.income = income;
    }

    public StatisticDiffModel getConsign() {
        return consign;
    }

    public void setConsign(StatisticDiffModel consign) {
        this.consign = consign;
    }

    public String getFullSum() {
        return fullSum;
    }

    public void setFullSum(String fullSum) {
        this.fullSum = fullSum;
    }
}
