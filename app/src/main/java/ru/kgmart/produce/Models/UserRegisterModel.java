package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 7/3/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class UserRegisterModel {


    @JsonProperty("UserRegister")
    private UserRegisterModel.Details UserRegisterForm;

    @JsonIgnore
    private String message;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Details getUserRegisterForm() {
        return UserRegisterForm;
    }

    public void setUserRegisterForm(Details userRegisterForm) {
        UserRegisterForm = userRegisterForm;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Details {

        @JsonProperty("last_name")
        private String last_name;

        @JsonProperty("first_name")
        private String first_name;

        @JsonProperty("phone")
        private String phone;

        @JsonProperty("address")
        private String address;

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }
    }
}
