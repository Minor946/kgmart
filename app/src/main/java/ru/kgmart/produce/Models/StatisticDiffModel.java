package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StatisticDiffModel {

    @SerializedName("receive_quantity")
    @Expose
    @JsonProperty("receive_quantity")
    private Integer receiveQuantity;
    @SerializedName("receive_nl")
    @Expose
    @JsonProperty("receive_nl")
    private Integer receiveNl;
    @SerializedName("revert_nl")
    @Expose
    @JsonProperty("revert_nl")
    private Integer revertNl;
    @SerializedName("sum_revert")
    @Expose
    @JsonProperty("sum_revert")
    private Integer sumRevert;
    @SerializedName("sum")
    @Expose
    @JsonProperty("sum")
    private Integer sum;

    public Integer getReceiveQuantity() {
        return receiveQuantity;
    }

    public void setReceiveQuantity(Integer receiveQuantity) {
        this.receiveQuantity = receiveQuantity;
    }

    public Integer getReceiveNl() {
        return receiveNl;
    }

    public void setReceiveNl(Integer receiveNl) {
        this.receiveNl = receiveNl;
    }

    public Integer getRevertNl() {
        return revertNl;
    }

    public void setRevertNl(Integer revertNl) {
        this.revertNl = revertNl;
    }

    public Integer getSumRevert() {
        return sumRevert;
    }

    public void setSumRevert(Integer sumRevert) {
        this.sumRevert = sumRevert;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }
}
