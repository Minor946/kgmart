package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created on 4/23/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProductsModels {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("category_id")
    @Expose
    @JsonProperty("category_id")
    private String categoryId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("sub_age_type")
    @Expose
    private String subAgeType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("margin_price")
    @Expose
    private Double marginPrice;
    @SerializedName("total_price")
    @Expose
    private Double totalPrice;
    @SerializedName("sellout_total_price")
    @Expose
    @JsonProperty("sellout_total_price")
    private String selloutTotalPrice;
    @SerializedName("sellout_price")
    @Expose
    @JsonProperty("sellout_price")
    private String selloutPrice;
    @SerializedName("trade_margin_price")
    @Expose
    private Integer tradeMarginPrice;
    @SerializedName("trade_total_price")
    @Expose
    @JsonProperty("trade_total_price")
    private Integer tradeTotalPrice;
    @SerializedName("tradePrice")
    @Expose
    @JsonProperty("trade_price")
    private String tradePrice;
    @SerializedName("wholesale")
    @Expose
    @JsonProperty("wholesale")
    private String wholesale;
    @SerializedName("orign_id")
    @Expose
    private String orignId;
    @SerializedName("number_in_line")
    @Expose
    @JsonProperty("number_in_line")
    private String numberInLine;
    @SerializedName("weight")
    @Expose
    @JsonProperty("weight")
    private String weight;
    @SerializedName("in_stock")
    @Expose
    @JsonProperty("in_stock")
    private String inStock;
    @SerializedName("main_image")
    @Expose
    @JsonProperty("main_image")
    private String mainImage;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("sell_out")
    @Expose
    @JsonProperty("sell_out")
    private String sellOut;
    @SerializedName("visible")
    @Expose
    @JsonProperty("visible")
    private String visible;
    @SerializedName("to_top")
    @Expose
    private String toTop;
    @SerializedName("onesize")
    @Expose
    private String onesize;
    @SerializedName("_update_date")
    @Expose
    private String updateDate;
    @SerializedName("_created_date")
    @Expose
    private String createdDate;
    @SerializedName("hits")
    @Expose
    private String hits;
    @SerializedName("old_price")
    @Expose
    private Double oldPrice;
    @SerializedName("line_weight")
    @Expose
    private Integer lineWeight;
    @SerializedName("discount")
    @Expose
    private Integer discount;
    @SerializedName("countdown")
    @Expose
    private String countdown;
    @SerializedName("images")
    @Expose
    @JsonProperty("images")
    private List<ProductImageModel> images = null;
    @SerializedName("colors")
    @Expose
    private List<ColorModel> colors = null;
    @SerializedName("sizes")
    @Expose
    private List<SizeModel> sizes = null;
    @SerializedName("accessories")
    @Expose
    private List<AccessoryModel> accessories = null;
    @SerializedName("options")
    @Expose
    private List<ProductsModels.Option> options = null;
    @SerializedName("front_name")
    @Expose
    private String frontName;
    @SerializedName("bonus")
    @Expose
    private Double bonus;
    @SerializedName("bonus_url")
    @Expose
    private String bonusUrl;
    @SerializedName("bonus_article_id")
    @Expose
    private Integer bonusArticleId;
    @SerializedName("certificate")
    @Expose
    private String certificate;
    @SerializedName("free_delivery")
    @Expose
    private String freeDelivery;
    @SerializedName("free_delivery_url")
    @Expose
    private String freeDeliveryUrl;
    @SerializedName("guarantees")
    @Expose
    private String guarantees;
    @SerializedName("guarantees_url")
    @Expose
    private String guaranteesUrl;
    @SerializedName("return_guarantees")
    @Expose
    private String returnGuarantees;
    @SerializedName("category_name")
    @Expose
    @JsonProperty("category_name")
    private String categoryName;

    @SerializedName("status_visible")
    @Expose
    @JsonProperty("status_visible")
    private String statusVisible;

    @SerializedName("total_price_in_currency")
    @Expose
    @JsonProperty("total_price_in_currency")
    private String totalPriceInCurrency;

    @SerializedName("new_good")
    @Expose
    private Integer newGood;

    @SerializedName("warehouse")
    @Expose
    private List<WarehouseModel> warehouse = null;

    @SerializedName("reception")
    @Expose
    private List<ReceptionModel> reception = null;

    @SerializedName("item")
    @Expose
    @JsonProperty("item")
    private List<ItemAvailableModel> item = null;

    @SerializedName("tci")
    @Expose
    @JsonProperty("tci")
    private List<TciModel> tci = null;

    @SerializedName("stock")
    @Expose
    @JsonProperty("stock")
    private StockModel stock = null;

    public List<ProductImageModel> getImages() {
        return images;
    }

    @JsonProperty("receptiondefect")
    private List<ReceptionDefectModel> receptiondefect = null;

    public void setImages(List<ProductImageModel> images) {
        this.images = images;
    }

    @SerializedName("itemreturn")
    @Expose
    @JsonProperty("itemreturn")
    private List<ReturnDefectModel> itemreturn = null;

    @SerializedName("consigment")
    @Expose
    @JsonProperty("consigment")
    private List<ConsigmentModel> consigment = null;

    public String getVisible() {
        return visible;
    }

    public void setVisible(String visible) {
        this.visible = visible;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Integer getTradeTotalPrice() {
        return tradeTotalPrice;
    }

    public void setTradeTotalPrice(Integer tradeTotalPrice) {
        this.tradeTotalPrice = tradeTotalPrice;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<TciModel> getTci() {
        return tci;
    }

    public void setTci(List<TciModel> tci) {
        this.tci = tci;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Option {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("product_id")
        @Expose
        private String productId;
        @SerializedName("attr_id")
        @Expose
        private String attrId;
        @SerializedName("item_id")
        @Expose
        private String itemId;
        @SerializedName("label")
        @Expose
        private String label;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("value")
        @Expose
        private String value;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public String getAttrId() {
            return attrId;
        }

        public void setAttrId(String attrId) {
            this.attrId = attrId;
        }

        public String getItemId() {
            return itemId;
        }

        public void setItemId(String itemId) {
            this.itemId = itemId;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<ColorModel> getColors() {
        return colors;
    }

    public void setColors(List<ColorModel> colors) {
        this.colors = colors;
    }

    public List<SizeModel> getSizes() {
        return sizes;
    }

    public void setSizes(List<SizeModel> sizes) {
        this.sizes = sizes;
    }

    public String getMainImage() {
        return mainImage;
    }

    public void setMainImage(String mainImage) {
        this.mainImage = mainImage;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getInStock() {
        return inStock;
    }

    public void setInStock(String inStock) {
        this.inStock = inStock;
    }

    public String getNumberInLine() {
        return numberInLine;
    }

    public void setNumberInLine(String numberInLine) {
        this.numberInLine = numberInLine;
    }

    public List<WarehouseModel> getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(List<WarehouseModel> warehouse) {
        this.warehouse = warehouse;
    }

    public String getSellOut() {
        return sellOut;
    }

    public void setSellOut(String sellOut) {
        this.sellOut = sellOut;
    }

    public List<ReceptionModel> getReception() {
        return reception;
    }

    public void setReception(List<ReceptionModel> reception) {
        this.reception = reception;
    }

    public List<ItemAvailableModel> getItem() {
        return item;
    }

    public void setItem(List<ItemAvailableModel> item) {
        this.item = item;
    }

    public List<ReceptionDefectModel> getReceptiondefect() {
        return receptiondefect;
    }

    public void setReceptiondefect(List<ReceptionDefectModel> receptiondefect) {
        this.receptiondefect = receptiondefect;
    }

    public List<ReturnDefectModel> getItemreturn() {
        return itemreturn;
    }

    public void setItemreturn(List<ReturnDefectModel> itemreturn) {
        this.itemreturn = itemreturn;
    }

    public List<ConsigmentModel> getConsigment() {
        return consigment;
    }

    public void setConsigment(List<ConsigmentModel> consigment) {
        this.consigment = consigment;
    }

    public String getSelloutTotalPrice() {
        return selloutTotalPrice;
    }

    public void setSelloutTotalPrice(String selloutTotalPrice) {
        this.selloutTotalPrice = selloutTotalPrice;
    }

    public String getTradePrice() {
        return tradePrice;
    }

    public void setTradePrice(String tradePrice) {
        this.tradePrice = tradePrice;
    }

    public String getWholesale() {
        return wholesale;
    }

    public void setWholesale(String wholesale) {
        this.wholesale = wholesale;
    }

    public String getSelloutPrice() {
        return selloutPrice;
    }

    public void setSelloutPrice(String selloutPrice) {
        this.selloutPrice = selloutPrice;
    }

    public String getStatusVisible() {
        return statusVisible;
    }

    public void setStatusVisible(String statusVisible) {
        this.statusVisible = statusVisible;
    }

    public StockModel getStock() {
        return stock;
    }

    public void setStock(StockModel stock) {
        this.stock = stock;
    }

    public String getTotalPriceInCurrency() {
        return totalPriceInCurrency;
    }

    public void setTotalPriceInCurrency(String totalPriceInCurrency) {
        this.totalPriceInCurrency = totalPriceInCurrency;
    }
}