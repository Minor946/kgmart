package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StockModel {

    @SerializedName("count_all")
    @Expose
    @JsonProperty("count_all")
    private Integer countAll;
    @SerializedName("sold")
    @Expose
    @JsonProperty("sold")
    private Integer sold;
    @SerializedName("return")
    @Expose
    @JsonProperty("return")
    private Integer countReturn;
    @SerializedName("price")
    @Expose
    @JsonProperty("price")
    private Integer price;

    public Integer getCountAll() {
        return countAll;
    }

    public void setCountAll(Integer countAll) {
        this.countAll = countAll;
    }

    public Integer getSold() {
        return sold;
    }

    public void setSold(Integer sold) {
        this.sold = sold;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCountReturn() {
        return countReturn;
    }

    public void setCountReturn(Integer countReturn) {
        this.countReturn = countReturn;
    }
}
