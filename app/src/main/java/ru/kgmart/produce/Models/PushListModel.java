package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created on 5/7/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class PushListModel {

    @SerializedName("Push")
    @Expose
    @JsonProperty("Push")
    private PushTokenModel push;

    public PushTokenModel getPush() {
        return push;
    }

    public void setPush(PushTokenModel push) {
        this.push = push;
    }

}
