package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserLoginModel {

    private UserModel user;
    private String key;

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }


}
