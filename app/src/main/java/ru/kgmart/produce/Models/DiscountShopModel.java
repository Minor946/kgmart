package ru.kgmart.produce.Models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created on 5/15/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DiscountShopModel {

    @JsonProperty("DiscountShop")
    private ArrayList<DiscountItemShopModel> discountItemShopModel;

    public ArrayList<DiscountItemShopModel> getDiscountItemShopModel() {
        return discountItemShopModel;
    }

    public void setDiscountItemShopModel(ArrayList<DiscountItemShopModel> discountItemShopModel) {
        this.discountItemShopModel = discountItemShopModel;
    }
}
