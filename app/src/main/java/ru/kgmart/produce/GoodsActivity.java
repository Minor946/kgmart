package ru.kgmart.produce;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.perf.metrics.AddTrace;

import java.util.Objects;

import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.Goods.GoodSalesFragment;
import ru.kgmart.produce.Fragments.Goods.GoodToCarryFragment;
import ru.kgmart.produce.Fragments.GoodsFormFragment;
import ru.kgmart.produce.Fragments.GoodsFragment;
import ru.kgmart.produce.Fragments.GoodsMyFragment;

public class GoodsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "GoodsActivity";
    FloatingActionButton fab;
    Fragment fragment;

    @SuppressLint("RestrictedApi")
    @Override
    @AddTrace(name = "onCreateTraceGood", enabled = true)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goods);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.label_my_goods));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                CommonComponents.updateNotificationCount(getApplicationContext(), navigationView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        CommonComponents.initNavigation(getApplicationContext(), navigationView);

        Intent intent = getIntent();
        String type2 = null;
        if (intent.getAction() != null) {
            switch (intent.getAction()) {
                case "TARGET_TO_CARRY":
                    type2 = String.valueOf(GoodEnum.TYPE_TO_CARRY);
                    break;
                case "TARGET_TO_CONSIGN":
                    type2 = String.valueOf(GoodEnum.TYPE_CONSIGN);
                    break;
                case "TARGET_TO_AVAILABLE":
                    type2 = String.valueOf(GoodEnum.TYPE_AVAILABLE);
                    break;
                default:
                    type2 = String.valueOf(GoodEnum.TYPE_ALL);
                    break;
            }
        }

        Bundle extras = getIntent().getExtras();
        String type = null;
        String sub_type = null;
        String search = null;

        if (extras != null) {
            type = extras.getString("type");
            sub_type = extras.getString("sub_type");
            search = extras.getString("search");
            // and get whatever type user account id is
        }
        if (TextUtils.isEmpty(type) && !TextUtils.isEmpty(type2)) {
            type = type2;
        }

        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        assert type != null;


        try {
            if (type.equals(String.valueOf(GoodEnum.TYPE_ALL)) && TextUtils.isEmpty(sub_type)) {
                fab = (FloatingActionButton) findViewById(R.id.fab);
                fab.setVisibility(View.VISIBLE);
                fab.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        fab.setVisibility(View.GONE);
                        openFragment(new GoodsFormFragment(), "goodForm");
                    }
                });
            }
            bundle.putString("sub_type", sub_type);
            if ((type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY)))) {
                fragment = new GoodToCarryFragment();
            } else if ((type.equals(String.valueOf(GoodEnum.TYPE_SALES)))) {
                fragment = new GoodSalesFragment();
            } else



                if ((type.equals(String.valueOf(GoodEnum.TYPE_ALL))) && !TextUtils.isEmpty(sub_type)) {
                fragment = new GoodsFragment();
            } else if ((type.equals(String.valueOf(GoodEnum.TYPE_ALL))) && !TextUtils.isEmpty(sub_type)) {
                fragment = new GoodsFragment();
            } else if (type.equals(String.valueOf(GoodEnum.TYPE_ALL)) || type.equals(String.valueOf(GoodEnum.TYPE_ALL_UPDATE))) {
                fragment = new GoodsMyFragment();
            } else if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
                fragment = new GoodsFragment();
                bundle.putString("sub_type", String.valueOf(GoodEnum.SUB_TYPE_DEFECT_RETURN));
            } else {
                fragment = new GoodsFragment();
            }

            fragment.setArguments(bundle);
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.mainFrame, fragment, "Goods")
                    .commit();

        } catch (Exception e) {
            Crashlytics.logException(e);
            Log.e(TAG, "onCreate: ", e);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            String fragment = getSupportFragmentManager().findFragmentById(R.id.mainFrame).getClass().getName();
            if (fragment.equals(GoodsFormFragment.class.getName())) {
                fab.setVisibility(View.VISIBLE);
            }
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_search, menu);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;
        Log.d(TAG, "onNavigationItemSelected: " + String.valueOf(id));
        switch (id) {
            case R.id.action_main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
            case R.id.action_faq:
                startActivity(new Intent(getApplicationContext(), FaqActivity.class));
                break;
            case R.id.action_my_photo_session:
                startActivity(new Intent(getApplicationContext(), SchedulePhotoActivity.class));
                break;
            case R.id.action_my_profile:
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.action_statistic:
                startActivity(new Intent(getApplicationContext(), StatisticActivity.class));
                break;
            case R.id.action_my_goods:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_ALL));
                startActivity(intent);
                break;
            case R.id.action_need_to_carry:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_TO_CARRY));
                startActivity(intent);
                break;
            case R.id.action_check_available:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_AVAILABLE));
                startActivity(intent);
                break;
            case R.id.action_my_defects:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_DEFECT));
                intent.putExtra("sub_type", String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK));
                startActivity(intent);
                break;
            case R.id.action_sales:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_SALES));
                startActivity(intent);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);

        transaction.add(R.id.mainFrame, fragment, tag);
        transaction.addToBackStack("Goods");
        transaction.commit();
    }

}
