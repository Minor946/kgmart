package ru.kgmart.produce;

import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import ru.kgmart.produce.Fragments.LoginFragment;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivityLog";
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginFragment loginFragment = new LoginFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainFrame, loginFragment, "Login")
                .commit();
    }

    @Override
    public void onBackPressed() {
        String fragment = getSupportFragmentManager().findFragmentById(R.id.mainFrame).getClass().getName();
        if (fragment.equals(LoginFragment.class.getName())) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                this.finishAffinity();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Нажмите еще раз для выхода из приложения", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            getSupportFragmentManager().popBackStack();
        }
    }
}

