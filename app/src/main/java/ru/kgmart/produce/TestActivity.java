package ru.kgmart.produce;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class TestActivity extends AppCompatActivity {

    private static final String TAG = "TestLog";
    String type = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Bundle bundle = getIntent().getExtras();
        type = bundle.getString("type");

        switch (type) {
            case "schedule_form":
                setContentView(R.layout.fragment_photo_schedule_form);
                break;
            case "promo":
                setContentView(R.layout.fragment_stock_list);
                break;
            case "carry":
                setContentView(R.layout.fragment_goods_carry);
                break;
            case "goods_form":
                setContentView(R.layout.fragment_goods_form);
                break;
            case "profile_edit":
                setContentView(R.layout.fragment_user_profile_edit);
                break;
            case "discounts":
                setContentView(R.layout.fragment_discount_form);
                break;
        }

    }
}
