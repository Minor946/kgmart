package ru.kgmart.produce.Adapters;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Objects;

import ru.kgmart.produce.Fragments.GoodsFullscreenImageFragment;
import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.R;

/**
 * Created on 5/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class ImageHorizontalAdapter extends RecyclerView.Adapter<ImageHorizontalAdapter.ImageViewHolder> {

    private ArrayList<String> imageList;
    private ArrayList<String> imageThumbList;
    private FragmentActivity activity;
    private View v;

    public ImageHorizontalAdapter(FragmentActivity activity, ArrayList<String> imageThumbList, ArrayList<String> imageList) {
        this.imageThumbList = imageThumbList;
        this.imageList = imageList;
        this.activity = activity;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_image, parent, false);
        return new ImageHorizontalAdapter.ImageViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, @SuppressLint("RecyclerView") int position) {

        Glide.with(Objects.requireNonNull(activity))
                .load(imageThumbList.get(position))
                .override(200, 300)
                .fitCenter()
                .into(holder.goods_image);

        holder.goods_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodsFullscreenImageFragment imagesDialog = null;
                imagesDialog = GoodsFullscreenImageFragment.newInstance(imageList, position);
                imagesDialog.show(activity.getSupportFragmentManager(), GoodsPageFragment.class.getSimpleName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return imageThumbList.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
        }
    }
}
