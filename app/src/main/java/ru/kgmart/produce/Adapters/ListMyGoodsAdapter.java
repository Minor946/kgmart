package ru.kgmart.produce.Adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.GoodsFragment;

/**
 * Created on 4/25/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class ListMyGoodsAdapter extends FragmentPagerAdapter {

    private final int NumOfTabs;
    private final String type;

    public ListMyGoodsAdapter(FragmentManager fm, int NumOfTabs, String type) {
        super(fm);
        this.NumOfTabs = NumOfTabs;
        this.type = type;
    }

    @Override
    public Fragment getItem(int position) {

        GoodsFragment fragment = new GoodsFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", String.valueOf(type));

        switch (position) {
            case 0:
                bundle.putString("sub_type", String.valueOf(GoodEnum.SUB_TYPE_AVAILABLE));
                break;
            case 2:
                bundle.putString("sub_type", String.valueOf(GoodEnum.SUB_TYPE_NO_AVAILABLE));
                break;
            case 1:
                bundle.putString("type", String.valueOf(GoodEnum.TYPE_STOCK_SALES));
                break;
            default:
                bundle.putString("sub_type", String.valueOf(GoodEnum.SUB_TYPE_ALL));
                break;
        }

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        return this.NumOfTabs;
    }

}