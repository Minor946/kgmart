package ru.kgmart.produce.Adapters.GoodAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Components.RoundedImageView;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Fragments.GoodsFullscreenImageFragment;
import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.ReceptionModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodToCarryRecyclerViewAdapter extends RecyclerView.Adapter<GoodToCarryRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "GoodRecyclerViewAdapter";

    private Map<Integer, GoodsAdapterModel> map;

    private Context context;
    private String type;
    private RelativeLayout rl_empty;
    private FragmentActivity activity;
    private MaterialDialog.Builder dialog;


    public GoodToCarryRecyclerViewAdapter(Map<Integer, GoodsAdapterModel> map, Context context,
                                          String type,
                                          FragmentActivity activity, RelativeLayout rl_empty) {
        this.map = map;
        this.context = context;
        this.type = type;
        this.activity = activity;
        this.rl_empty = rl_empty;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public GoodToCarryRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_v2, parent, false);
        return new GoodToCarryRecyclerViewAdapter.ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull GoodToCarryRecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (Objects.requireNonNull(map).size() > 0 && Objects.requireNonNull(map).get(position) != null && Objects.requireNonNull(Objects.requireNonNull(map).get(position)).getProductsModels() != null) {

            ProductsModels model = Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(map).get(position)).getProductsModels());
            String image_url = null;
            ArrayList<String> imageList = new ArrayList<>();
            Crashlytics.setString("adapter_product_id", model.getId());
            Crashlytics.setString("type", type);


            if (Objects.requireNonNull(map.get(position)).getReceptionModel() != null) {
                holder.bindColor(String.valueOf(Objects.requireNonNull(map.get(position)).getReceptionModel().getColorName()));
                holder.bindSize(String.valueOf(Objects.requireNonNull(map.get(position)).getReceptionModel().getSizeName()));
                image_url = GoodsComponents.getImageByColor(model.getColors(), Objects.requireNonNull(map.get(position)).getReceptionModel().getColorId());
                holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), Objects.requireNonNull(map.get(position)).getReceptionModel().getColorId()),
                        String.valueOf(Objects.requireNonNull(map.get(position)).getReceptionModel().getSizeName()));
                holder.goods_planned_date.setText(Objects.requireNonNull(map.get(position)).getReceptionModel().getPlanedDate());
                holder.ll_planned_date.setVisibility(View.VISIBLE);
                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), Objects.requireNonNull(map.get(position)).getReceptionModel().getColorId()));
                holder.btn_carry.setOnClickListener(checkAvailable(Objects.requireNonNull(map.get(position)).getReceptionModel().getId(), 0, "carry"));
                holder.btn_no_carry.setOnClickListener(checkAvailable(Objects.requireNonNull(map.get(position)).getReceptionModel().getId(), 1, "carry"));
                holder.ll_quantity.setVisibility(View.VISIBLE);
                holder.goods_count.setText(Objects.requireNonNull(map.get(position)).getReceptionModel().getQuantity() + " ЛР");
                holder.goods_price.setText(String.valueOf(Objects.requireNonNull(map.get(position)).getProductsModels().getPrice()));

                if (TextUtils.isEmpty(Objects.requireNonNull(map.get(position)).getReceptionModel().getStatus())) {
                    holder.ll_actions_carry.setVisibility(View.VISIBLE);
                    holder.ll_cancel.setVisibility(View.GONE);
                    holder.ll_status.setVisibility(View.GONE);
                } else {
                    if (Objects.requireNonNull(map.get(position)).getReceptionModel().getStatus().equals("Принесу")) {
                        holder.goods_status.setTextColor(Color.parseColor("#2e7d32"));
                    }
                    holder.ll_cancel.setVisibility(View.VISIBLE);
                    holder.ll_status.setVisibility(View.VISIBLE);
                    holder.ll_actions_carry.setVisibility(View.GONE);
                    holder.goods_status.setText(String.valueOf(Objects.requireNonNull(map.get(position)).getReceptionModel().getStatus()));
                    holder.btn_cancel.setOnClickListener(checkAvailable(Objects.requireNonNull(map.get(position)).getReceptionModel().getId(), 5, "carry"));
                }
            }


            try {
                if (model.getSizes() != null) {
                    if (model.getSizes().get(0) != null) {
                        holder.goods_size.setText(model.getSizes().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            try {
                if (model.getColors() != null) {
                    if (model.getColors().get(0) != null) {
                        holder.goods_color.setText(model.getColors().get(0).getName());
                        holder.bindColorImage(model.getColors().get(0).getCode(), model.getColors().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            if (image_url == null) {
                image_url = model.getMainImage();
            }

            if (image_url.contains("http://kgmart.ru")) {
                Glide.with(context)
                        .load(image_url)
                        .override(396, 600)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.goods_image);

            } else {
                Glide.with(context)
                        .load("http://kgmart.ru" + image_url)
                        .override(396, 600)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.goods_image);
            }


            if (imageList.size() == 0) {
                imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
            }

            if (imageList.size() > 0) {
                holder.goods_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GoodsFullscreenImageFragment imagesDialog = null;
                        Log.d(TAG, "onClick: " + String.valueOf(imageList));
                        imagesDialog = GoodsFullscreenImageFragment.newInstance(imageList, 0);
                        imagesDialog.show(activity.getSupportFragmentManager(), GoodsPageFragment.class.getSimpleName());
                    }
                });
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (map != null) {
            return map.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;

        TextView goods_name;
        TextView goods_color;
        TextView goods_size;
        TextView goods_id;
        TextView goods_number_in_line;
        TextView goods_price;
        TextView goods_price_currency;
        TextView goods_position;
        TextView goods_status;
        TextView goods_count;
        TextView goods_planned_date;
        TextView btn_cancel;

        LinearLayout ll_actions_carry;
        LinearLayout ll_quantity;
        LinearLayout ll_cancel;
        LinearLayout ll_planned_date;
        LinearLayout ll_status;

        Button btn_carry;
        Button btn_no_carry;

        RoundedImageView color_picker_image_view;

        ViewHolder(View itemView) {
            super(itemView);
            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
            goods_name = (TextView) itemView.findViewById(R.id.goods_name);
            goods_color = (TextView) itemView.findViewById(R.id.goods_color);
            goods_id = (TextView) itemView.findViewById(R.id.goods_id);
            goods_position = (TextView) itemView.findViewById(R.id.goods_position);
            goods_status = (TextView) itemView.findViewById(R.id.goods_status);
            goods_price_currency = (TextView) itemView.findViewById(R.id.goods_price_currency);
            color_picker_image_view = (RoundedImageView) itemView.findViewById(R.id.color_picker_image_view);
            goods_price = (TextView) itemView.findViewById(R.id.goods_price);
            goods_number_in_line = (TextView) itemView.findViewById(R.id.goods_number_in_line);
            goods_size = (TextView) itemView.findViewById(R.id.goods_size);

            ll_actions_carry = (LinearLayout) itemView.findViewById(R.id.ll_actions_carry);
            ll_quantity = (LinearLayout) itemView.findViewById(R.id.ll_quantity);
            ll_planned_date = (LinearLayout) itemView.findViewById(R.id.ll_planned_date);
            ll_status = (LinearLayout) itemView.findViewById(R.id.ll_status);
            ll_cancel = (LinearLayout) itemView.findViewById(R.id.ll_cancel);


            btn_carry = (Button) itemView.findViewById(R.id.btn_carry);
            btn_no_carry = (Button) itemView.findViewById(R.id.btn_no_carry);

            goods_count = (TextView) itemView.findViewById(R.id.goods_count);
            goods_planned_date = (TextView) itemView.findViewById(R.id.goods_planned_date);
            btn_cancel = (TextView) itemView.findViewById(R.id.btn_cancel);
        }

        void bindColor(String color) {
            goods_color.setText(color);
        }

        void bindColorImage(String colorCode, String colorName) {
            try {
                GradientDrawable gradDrawable = (GradientDrawable) color_picker_image_view.getBackground();
                int resultColor = 0xffffffff;
                gradDrawable.setColor(resultColor);
                resultColor = Color.parseColor(colorCode);
                Log.d("WRONG COLOR", "CustomSpinnerColors parse color exception");
                gradDrawable.setColor(resultColor);
            } catch (Exception ignore) {
            }
        }

        void bindSize(String size) {
            goods_size.setText(size);
        }
    }

    private View.OnClickListener checkAvailable(int position, int type, String action) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 0 || type == 5) {
                    sendCarry(position, type);
                } else {
                    dialog = DialogManager.goodAvailableDialog(context, "Вы уверены что товара не будет в наличии");
                    dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            sendCarry(position, type);
                            dialog.dismiss();
                        }
                    });
                    dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            sendCarry(position, 3);
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        };
    }

    private void sendCarry(Integer position, Integer type) {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        GoodsModel goodsModel = new GoodsModel();
        @SuppressLint("UseSparseArrays")
        Map<Integer, GoodsAdapterModel> mapNew = new HashMap<>();
        ReceptionModel receptionModel = new ReceptionModel();
        List<Integer> keys = new ArrayList<Integer>(map.keySet());
        Log.d(TAG, "sendCarry: item_id " + position);
        int i = 0;
        for (Map.Entry<Integer, GoodsAdapterModel> entry : map.entrySet()) {
            if (entry.getValue().getReceptionModel().getId().equals(position)) {
                Log.d(TAG, "sendCarry: item_id_equal " + position);
                Log.d(TAG, "sendCarry: item_id_equal " + i);
                GoodsAdapterModel full_model = map.get(i);
                assert full_model != null;
                if (full_model.getReceptionModel() != null) {
                    if (full_model.getReceptionModel().getColorId() != null) {
                        receptionModel.setColorId(full_model.getReceptionModel().getColorId());
                    }
                    receptionModel.setItemId(full_model.getReceptionModel().getItemId());
                    receptionModel.setReceptionId(full_model.getReceptionModel().getId());
                    receptionModel.setSizeId(full_model.getReceptionModel().getSizeId());
                    receptionModel.setProductId(Integer.valueOf(full_model.getProductsModels().getId()));
                    receptionModel.setStatus(type.toString());
                    goodsModel.setReceptionModel(receptionModel);

                    Call<RestResponse<GoodsModel>> call = apiGoods.setCarry(goodsModel, CommonComponents.getUserKey(context));
                    int finalI = i;
                    call.enqueue(new Callback<RestResponse<GoodsModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                            Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
                            Toast.makeText(context, "Запись успешно обновлена", Toast.LENGTH_SHORT).show();
                            switch (type) {
                                case 0:
                                    Objects.requireNonNull(map.get(keys.get(finalI))).getReceptionModel().setStatus("Принесу");
                                    break;
                                case 1:
                                    Objects.requireNonNull(map.get(keys.get(finalI))).getReceptionModel().setStatus("Скоро будет");
                                    break;
                                case 3:
                                    Objects.requireNonNull(map.get(keys.get(finalI))).getReceptionModel().setStatus("Нет в наличии");
                                    break;
                                case 5:
                                    Objects.requireNonNull(map.get(keys.get(finalI))).getReceptionModel().setStatus("");
                                    break;
                                default:
                                    map.remove(keys.get(finalI));
                                    break;
                            }
                            @SuppressLint("UseSparseArrays")
                            Map<Integer, GoodsAdapterModel> defaultMap = new HashMap<Integer, GoodsAdapterModel>(map);
                            map.clear();
                            int j = 0;
                            for (Map.Entry<Integer, GoodsAdapterModel> entry : defaultMap.entrySet()) {
                                map.put(j, entry.getValue());
                                j++;
                            }
                            Log.d(TAG, "sendCarry: defaultMap " + defaultMap.keySet().toString());
                            notifyItemRemoved(finalI);
                            notifyDataSetChanged();
                            checkList();
                        }

                        @Override
                        public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                        }
                    });
                }
                break;
            }
            i++;
        }
    }

    private void checkList() {
        if (map.size() <= 0) {
            rl_empty.setVisibility(View.VISIBLE);
        } else {
            rl_empty.setVisibility(View.GONE);
        }
    }
}
