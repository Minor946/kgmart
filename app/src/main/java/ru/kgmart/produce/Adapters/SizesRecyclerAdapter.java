package ru.kgmart.produce.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.kgmart.produce.Models.SizeModel;
import ru.kgmart.produce.R;

/**
 * Created on 7/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class SizesRecyclerAdapter extends RecyclerView.Adapter<SizesRecyclerAdapter.ViewHolder> {

    private static final String TAG = "SizesRecyclerAdapter";

    private View v;
    private Context context;
    private List<SizeModel> sizeList = new ArrayList<>();

    public SizesRecyclerAdapter(Context context, List<SizeModel> sizeList) {
        this.sizeList = sizeList;
        this.context = context;
    }

    @NonNull
    @Override
    public SizesRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_good_size, parent, false);
        return new SizesRecyclerAdapter.ViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull SizesRecyclerAdapter.ViewHolder holder, int position) {
        SizeModel model = sizeList.get(position);

        if (model != null) {
            holder.good_color_name.setText(model.getName());
        }
    }

    @Override
    public int getItemCount() {
        return sizeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView good_color_name;

        public ViewHolder(View itemView) {
            super(itemView);
            good_color_name = itemView.findViewById(R.id.good_color_name);
        }
    }
}
