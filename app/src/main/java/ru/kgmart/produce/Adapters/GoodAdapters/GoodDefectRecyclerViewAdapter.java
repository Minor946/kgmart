package ru.kgmart.produce.Adapters.GoodAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;

import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Components.RoundedImageView;
import ru.kgmart.produce.Fragments.GoodsFullscreenImageFragment;
import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.R;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodDefectRecyclerViewAdapter extends RecyclerView.Adapter<GoodDefectRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "GoodRecyclerViewAdapter";

    private Map<Integer, GoodsAdapterModel> map;

    private Context context;
    private String type;
    private FragmentActivity activity;

    public GoodDefectRecyclerViewAdapter(Map<Integer, GoodsAdapterModel> map, Context context, String type,
                                         FragmentActivity activity) {
        this.map = map;
        this.context = context;
        this.type = type;
        this.activity = activity;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public GoodDefectRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_v2, parent, false);
        return new GoodDefectRecyclerViewAdapter.ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull GoodDefectRecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (Objects.requireNonNull(map).size() > 0 && Objects.requireNonNull(map).get(position) != null && Objects.requireNonNull(Objects.requireNonNull(map).get(position)).getProductsModels() != null) {

            ProductsModels model = Objects.requireNonNull(Objects.requireNonNull(Objects.requireNonNull(map).get(position)).getProductsModels());
            Crashlytics.setString("adapter_product_id", model.getId());
            Crashlytics.setString("type", type);

            holder.goods_position.setText(String.valueOf(position));
            holder.goods_id.setText(String.valueOf(model.getId()));
            holder.goods_name.setText(model.getName());
            holder.goods_price_currency.setText(model.getTotalPriceInCurrency());

            try {
                if (model.getSizes() != null) {
                    if (model.getSizes().get(0) != null) {
                        holder.goods_size.setText(model.getSizes().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            try {
                if (model.getColors() != null) {
                    if (model.getColors().get(0) != null) {
                        holder.goods_color.setText(model.getColors().get(0).getName());
                        holder.bindColorImage(model.getColors().get(0).getCode(), model.getColors().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
            holder.goods_price.setText(String.valueOf(Objects.requireNonNull(map.get(position)).getTciModel().getPrice()));

            holder.goods_defect.setText(map.get(position).getReceptionDefectModel().getDefectNote());

            holder.bindColor(String.valueOf(Objects.requireNonNull(map.get(position)).getTciModel().getColorName()));
            holder.bindSize(String.valueOf(Objects.requireNonNull(map.get(position)).getTciModel().getSizeName()));
            String image_url = GoodsComponents.getImageByColor(model.getColors(), Objects.requireNonNull(map.get(position)).getTciModel().getColorId());
            holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), Objects.requireNonNull(map.get(position)).getTciModel().getColorId()),
                    String.valueOf(Objects.requireNonNull(map.get(position)).getTciModel().getSizeName()));
            ArrayList<String> imageList =
                    new ArrayList<>(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), Objects.requireNonNull(map.get(position)).getTciModel().getColorId()));



            if (image_url == null) {
                image_url = model.getMainImage();
            }

            if (image_url.contains("http://kgmart.ru")) {
                Glide.with(context)
                        .load(image_url)
                        .override(396, 600)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.goods_image);

            } else {
                Glide.with(context)
                        .load("http://kgmart.ru" + image_url)
                        .override(396, 600)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.goods_image);
            }


            if (imageList.size() == 0) {
                imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
            }

            if (imageList.size() > 0) {
                holder.goods_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GoodsFullscreenImageFragment imagesDialog = null;
                        Log.d(TAG, "onClick: " + String.valueOf(imageList));
                        imagesDialog = GoodsFullscreenImageFragment.newInstance(imageList, 0);
                        imagesDialog.show(activity.getSupportFragmentManager(), GoodsPageFragment.class.getSimpleName());
                    }
                });
            }
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (map != null) {
            return map.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;

        TextView goods_name;
        TextView goods_color;
        TextView goods_size;
        TextView goods_id;
        TextView goods_number_in_line;
        TextView goods_price;
        TextView goods_price_currency;
        TextView goods_position;
        TextView goods_status;
        TextView goods_defect;
        TextView goods_username;
        TextView countPhoto;
        TextView goods_return_number;


        RoundedImageView color_picker_image_view;

        ViewHolder(View itemView) {
            super(itemView);
            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
            goods_name = (TextView) itemView.findViewById(R.id.goods_name);
            goods_color = (TextView) itemView.findViewById(R.id.goods_color);
            goods_id = (TextView) itemView.findViewById(R.id.goods_id);
            goods_position = (TextView) itemView.findViewById(R.id.goods_position);
            goods_status = (TextView) itemView.findViewById(R.id.goods_status);
            goods_price_currency = (TextView) itemView.findViewById(R.id.goods_price_currency);
            color_picker_image_view = (RoundedImageView) itemView.findViewById(R.id.color_picker_image_view);
            goods_price = (TextView) itemView.findViewById(R.id.goods_price);
            goods_number_in_line = (TextView) itemView.findViewById(R.id.goods_number_in_line);
            goods_size = (TextView) itemView.findViewById(R.id.goods_size);
            goods_defect = (TextView) itemView.findViewById(R.id.goods_defect);

            goods_defect = (TextView) itemView.findViewById(R.id.goods_defect);
            goods_username = (TextView) itemView.findViewById(R.id.goods_username);
            countPhoto = (TextView) itemView.findViewById(R.id.countPhoto);
            goods_return_number.setVisibility(View.VISIBLE);
        }

        void bindColor(String color) {
            goods_color.setText(color);
        }

        void bindColorImage(String colorCode, String colorName) {
            try {
                GradientDrawable gradDrawable = (GradientDrawable) color_picker_image_view.getBackground();
                int resultColor = 0xffffffff;
                gradDrawable.setColor(resultColor);
                resultColor = Color.parseColor(colorCode);
                Log.d("WRONG COLOR", "CustomSpinnerColors parse color exception");
                gradDrawable.setColor(resultColor);
            } catch (Exception ignore) {
            }
        }

        void bindSize(String size) {
            goods_size.setText(size);
        }
    }

}
