package ru.kgmart.produce.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.github.lguipeng.library.animcheckbox.AnimCheckBox;

import java.util.ArrayList;
import java.util.Objects;

import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.R;

/**
 * Created on 5/7/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodSelectRecyclerViewAdapter extends RecyclerView.Adapter<GoodSelectRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "GoodSelectAdapter";
    ArrayList<ProductsModels> goodsList = new ArrayList<>();
    Context context;
    private SparseBooleanArray itemStateArray = new SparseBooleanArray();
    private ArrayList<String> itemProductId = new ArrayList<>();

    public GoodSelectRecyclerViewAdapter(ArrayList<ProductsModels> goodsList, Context context) {
        this.goodsList = goodsList;
        this.context = context;
    }

    @NonNull
    @Override
    public GoodSelectRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_select, parent, false);
        return new GoodSelectRecyclerViewAdapter.ViewHolder(v);
    }

    public SparseBooleanArray getItemStateArray() {
        return itemStateArray;
    }

    public ArrayList<String> getItemProductId() {
        return itemProductId;
    }

    @Override
    public void onBindViewHolder(@NonNull GoodSelectRecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        ProductsModels model = Objects.requireNonNull(goodsList).get(position);

        holder.goods_name.setText(model.getName());

        if (model.getSizes().get(0) != null) {
            holder.goods_size.setText(model.getSizes().get(0).getName());
        }
        if (model.getColors().get(0) != null) {
            holder.goods_color.setText(model.getColors().get(0).getName());
        }
        holder.goods_id.setText(model.getId());
        holder.goods_price.setText(String.valueOf(model.getPrice()));

        String image_url = model.getMainImage();
        Glide.with(context)
                .load("http://kgmart.ru" + image_url)
                .override(396, 600)
                .fitCenter()
                .into(holder.goods_image);

        holder.bind(position);

        try {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!itemStateArray.get(position, false)) {
                        holder.animCheckBox.setChecked(true);
                        itemStateArray.put(position, true);
                        itemProductId.add(model.getId());
                    } else {
                        holder.animCheckBox.setChecked(false);
                        itemStateArray.put(position, false);
                        for (int i = 0; i < itemProductId.size(); i++) {
                            if (itemProductId.get(i).equals(model.getId())) {
                                itemProductId.remove(i);
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "onBindViewHolder: ", e);
            Crashlytics.logException(e);
        }
    }

    @Override
    public int getItemCount() {
        return goodsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;
        TextView goods_name;
        TextView goods_id;
        TextView goods_color;
        TextView goods_size;
        TextView goods_price;
        AnimCheckBox animCheckBox;

        public ViewHolder(View itemView) {
            super(itemView);

            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
            goods_name = (TextView) itemView.findViewById(R.id.goods_name);
            goods_color = (TextView) itemView.findViewById(R.id.goods_color);
            goods_id = (TextView) itemView.findViewById(R.id.goods_id);
            goods_price = (TextView) itemView.findViewById(R.id.goods_price);
            goods_size = (TextView) itemView.findViewById(R.id.goods_size);
            animCheckBox = (AnimCheckBox) itemView.findViewById(R.id.btn_select_good);

        }

        void bind(int position) {
            // use the sparse boolean array to check
            if (!itemStateArray.get(position, false)) {
                animCheckBox.setChecked(false);
            } else {
                animCheckBox.setChecked(true);
            }
        }
    }
}
