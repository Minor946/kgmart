package ru.kgmart.produce.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.kgmart.produce.Components.RoundedImageView;
import ru.kgmart.produce.Models.ColorModel;
import ru.kgmart.produce.R;

/**
 * Created on 7/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class ColorsRecyclerAdapter extends RecyclerView.Adapter<ColorsRecyclerAdapter.ViewHolder> {

    private static final String TAG = "ColorsRecyclerAdapter";

    private View v;
    private Context context;
    private List<ColorModel> colorsList = new ArrayList<>();

    public ColorsRecyclerAdapter(Context context, List<ColorModel> colorsList) {
        this.colorsList = colorsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ColorsRecyclerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_good_color, parent, false);
        return new ColorsRecyclerAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ColorsRecyclerAdapter.ViewHolder holder, int position) {
        ColorModel model = colorsList.get(position);

        if (model != null) {
            holder.color_picker_text.setText(model.getName());
            GradientDrawable gradDrawable = (GradientDrawable) holder.color_picker_image_view.getBackground();
            int resultColor = 0xffffffff;
            try {
                resultColor = Color.parseColor(model.getCode());
            } catch (Exception e) {
                Log.d("WRONG COLOR", "CustomSpinnerColors parse color exception");
            }
            gradDrawable.setColor(resultColor);
            holder.color_picker_text.setText(model.getName());

            if (model.getName().equals("Разноцветный")) {
                gradDrawable = new GradientDrawable(
                        GradientDrawable.Orientation.TL_BR,
                        new int[]{ContextCompat.getColor(context, R.color.white),
                                ContextCompat.getColor(context, R.color.blue),
                                ContextCompat.getColor(context, R.color.yellow_warning),
                                ContextCompat.getColor(context, R.color.green)});
                gradDrawable.setShape(GradientDrawable.OVAL);
                holder.color_picker_image_view.setBackground(gradDrawable);
            }

        }
    }

    @Override
    public int getItemCount() {
        return colorsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RoundedImageView color_picker_image_view;
        TextView color_picker_text;

        public ViewHolder(View itemView) {
            super(itemView);
            color_picker_image_view = itemView.findViewById(R.id.color_picker_image_view);
            color_picker_text = itemView.findViewById(R.id.color_picker_text);
        }
    }
}
