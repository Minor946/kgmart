package ru.kgmart.produce.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Fragments.GoodsFullscreenImageFragment;
import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.R;

public class GoodRecyclerNewAdapter extends RecyclerView.Adapter<GoodRecyclerNewAdapter.ViewHolder> {

    private static final String TAG = "GoodRecyclerNewAdapter";

    private Map<Integer, GoodsAdapterModel> map = new HashMap<Integer, GoodsAdapterModel>();

    private int all_size;
    private int page;

    private Context context;
    private String type;
    private String sub_type;
    private String product_id;
    private RelativeLayout rl_empty;
    private View v;
    private FragmentActivity activity;
    MaterialDialog.Builder dialog;

    public GoodRecyclerNewAdapter(Map<Integer, GoodsAdapterModel> map, Context context,
                                  String type, String sub_type,
                                  FragmentActivity activity, String product_id, RelativeLayout rl_empty) {
        this.map = map;
        this.context = context;
        this.type = type;
        this.activity = activity;
        this.sub_type = sub_type;
        this.product_id = product_id;
        this.rl_empty = rl_empty;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public GoodRecyclerNewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_stock, parent, false);
        return new GoodRecyclerNewAdapter.ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull GoodRecyclerNewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
//        try {
            ProductsModels model = Objects.requireNonNull(Objects.requireNonNull(map).get(position).getProductsModels());
            String image_url = null;
            ArrayList<String> imageList = new ArrayList<>();

            Crashlytics.setString("adapter_product_id", model.getId());
            Crashlytics.setString("type", type);

            holder.goods_position.setText(String.valueOf(position));
            holder.goods_id.setText(String.valueOf(model.getId()));
            holder.goods_name.setText(model.getName());

            holder.goods_income.setText(String.valueOf(map.get(position).getStockModel().getCountAll()) + " ЛР");
            holder.goods_return.setText(String.valueOf(map.get(position).getStockModel().getCountReturn()) + " ЛР");
            holder.goods_sold.setText(String.valueOf(map.get(position).getStockModel().getSold()) + " ЛР");
            holder.goods_price.setText(String.valueOf(map.get(position).getStockModel().getPrice()));
            holder.goods_full_price.setText(String.valueOf(map.get(position).getStockModel().getSold() * map.get(position).getStockModel().getPrice()));
            holder.goods_count_on_consig.setText(String.valueOf((map.get(position).getStockModel().getCountAll() - map.get(position).getStockModel().getCountReturn() - map.get(position).getStockModel().getSold())) + " ЛР");

            if (image_url == null) {
                image_url = model.getMainImage();
            }

            Glide.with(context)
                    .load("http://kgmart.ru" + image_url)
                    .override(396, 600)
                    .fitCenter()
                    .into(holder.goods_image);

            if (imageList.size() == 0) {
                imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
            }

            if (imageList.size() > 0) {
                holder.goods_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
                        GoodsFullscreenImageFragment imagesDialog = null;
                        imagesDialog = GoodsFullscreenImageFragment.newInstance(imageList, 0);
                        imagesDialog.show(activity.getSupportFragmentManager(), GoodsPageFragment.class.getSimpleName());
                    }
                });
            }

//        } catch (Exception e) {
//            Crashlytics.logException(e);
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        if (map != null) {
            return map.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;
        LinearLayout ll_count_on_consig;

        TextView goods_name;
        TextView goods_id;
        TextView goods_income;
        TextView goods_return;
        TextView goods_sold;
        TextView goods_price;
        TextView goods_full_price;
        TextView goods_count_on_consig;
        TextView goods_position;

        public ViewHolder(View itemView) {
            super(itemView);
            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
            goods_name = (TextView) itemView.findViewById(R.id.goods_name);
            goods_id = (TextView) itemView.findViewById(R.id.goods_id);
            goods_position = (TextView) itemView.findViewById(R.id.goods_position);


            goods_income = (TextView) itemView.findViewById(R.id.goods_income);
            goods_return = (TextView) itemView.findViewById(R.id.goods_return);
            goods_sold = (TextView) itemView.findViewById(R.id.goods_sold);
            goods_price = (TextView) itemView.findViewById(R.id.goods_price);
            goods_full_price = (TextView) itemView.findViewById(R.id.goods_full_price);
            goods_count_on_consig = (TextView) itemView.findViewById(R.id.goods_count_on_consig);
        }
    }
}
