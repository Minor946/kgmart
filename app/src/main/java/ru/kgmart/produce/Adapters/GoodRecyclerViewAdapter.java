package ru.kgmart.produce.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Components.RoundedImageView;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Fragments.GoodsFragment;
import ru.kgmart.produce.Fragments.GoodsFullscreenImageFragment;
import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.GoodsDetailsActivity;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ItemAvailableModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.ReceptionModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodRecyclerViewAdapter extends RecyclerView.Adapter<GoodRecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "GoodRecyclerViewAdapter";

    private Map<Integer, GoodsAdapterModel> map = new HashMap<Integer, GoodsAdapterModel>();

    private int all_size;
    private int page;

    private Context context;
    private String type;
    private String sub_type;
    private String product_id;
    private RelativeLayout rl_empty;
    private View v;
    private FragmentActivity activity;
    MaterialDialog.Builder dialog;

    public GoodRecyclerViewAdapter(Map<Integer, GoodsAdapterModel> map, Context context,
                                   String type, String sub_type,
                                   FragmentActivity activity, String product_id, RelativeLayout rl_empty) {
        this.map = map;
        this.context = context;
        this.type = type;
        this.activity = activity;
        this.sub_type = sub_type;
        this.product_id = product_id;
        this.rl_empty = rl_empty;
        setHasStableIds(true);
    }


    @NonNull
    @Override
    public GoodRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (type.equals(String.valueOf(GoodEnum.TYPE_ALL))
                || type.equals(String.valueOf(GoodEnum.TYPE_ALL_UPDATE))
                || type.equals(String.valueOf(GoodEnum.TYPE_DEFECT)) || type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))
                || type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_v2, parent, false);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_carry, parent, false);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_carry, parent, false);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES)) || type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_goods_sales, parent, false);
        }
        return new GoodRecyclerViewAdapter.ViewHolder(v);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull GoodRecyclerViewAdapter.ViewHolder holder, @SuppressLint("RecyclerView") int position) {
        if (Objects.requireNonNull(map).size() > 0 && Objects.requireNonNull(map).get(position) != null && Objects.requireNonNull(map).get(position).getProductsModels() != null) {

            ProductsModels model = Objects.requireNonNull(Objects.requireNonNull(map).get(position).getProductsModels());
            String image_url = null;
            ArrayList<String> imageList = new ArrayList<>();
            Crashlytics.setString("adapter_product_id", model.getId());
            Crashlytics.setString("type", type);

            holder.goods_position.setText(String.valueOf(position));
            holder.goods_id.setText(String.valueOf(model.getId()));
            holder.goods_name.setText(model.getName());
            holder.goods_price_currency.setText(model.getTotalPriceInCurrency());
            try {
                if (model.getSizes() != null) {
                    if (model.getSizes().get(0) != null) {
                        holder.goods_size.setText(model.getSizes().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }

            try {
                if (model.getColors() != null) {
                    if (model.getColors().get(0) != null) {
                        holder.goods_color.setText(model.getColors().get(0).getName());
                        holder.bindColorImage(model.getColors().get(0).getCode(), model.getColors().get(0).getName());
                    }
                }
            } catch (Exception e) {
                Crashlytics.logException(e);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_ALL))) {
                holder.goods_price.setText(String.valueOf(map.get(position).getProductsModels().getPrice()));
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE)) && map.get(position).getItemAvailableModel() != null) {
                holder.bindCount(String.valueOf(map.get(position).getItemAvailableModel().getQuantity()) + " ЛР");
                holder.bindColor(String.valueOf(map.get(position).getItemAvailableModel().getColorName()));
                holder.bindSize(String.valueOf(map.get(position).getItemAvailableModel().getSizeName()));
                image_url = GoodsComponents.getImageByColor(model.getColors(), map.get(position).getItemAvailableModel().getColorId());
                holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), map.get(position).getItemAvailableModel().getColorId()), String.valueOf(map.get(position).getItemAvailableModel().getColorName()));
                holder.ll_actions.setVisibility(View.VISIBLE);
                holder.ll_in_stock.setVisibility(View.GONE);
                holder.ll_in_line.setVisibility(View.GONE);
                holder.ll_price.setVisibility(View.GONE);
                holder.btn_available.setOnClickListener(checkAvailable(Integer.valueOf(map.get(position).getItemAvailableModel().getItemId()), 0, "available"));
                holder.btn_no_available.setOnClickListener(checkAvailable(Integer.valueOf(map.get(position).getItemAvailableModel().getItemId()), 1, "available"));
                holder.ll_quantity.setVisibility(View.VISIBLE);
                holder.goods_count.setText(map.get(position).getItemAvailableModel().getQuantity() + " ЛР");
                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), map.get(position).getItemAvailableModel().getColorId()));
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY)) && map.get(position).getReceptionModel() != null) {
                holder.bindColor(String.valueOf(map.get(position).getReceptionModel().getColorName()));
                holder.bindSize(String.valueOf(map.get(position).getReceptionModel().getSizeName()));
                image_url = GoodsComponents.getImageByColor(model.getColors(), map.get(position).getReceptionModel().getColorId());
                holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), map.get(position).getReceptionModel().getColorId()), String.valueOf(map.get(position).getReceptionModel().getSizeName()));
                holder.goods_planned_date.setText(map.get(position).getReceptionModel().getPlanedDate());
                holder.ll_planned_date.setVisibility(View.VISIBLE);
                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), map.get(position).getReceptionModel().getColorId()));
                holder.btn_carry.setOnClickListener(checkAvailable(map.get(position).getReceptionModel().getId(), 0, "carry"));
                holder.btn_no_carry.setOnClickListener(checkAvailable(map.get(position).getReceptionModel().getId(), 1, "carry"));
                holder.ll_in_line.setVisibility(View.GONE);
                holder.ll_quantity.setVisibility(View.VISIBLE);
                holder.goods_count.setText(map.get(position).getReceptionModel().getQuantity() + " ЛР");
                holder.ll_in_stock.setVisibility(View.GONE);
                holder.goods_price.setText(String.valueOf(map.get(position).getProductsModels().getPrice()));

                if (TextUtils.isEmpty(map.get(position).getReceptionModel().getStatus())) {
                    holder.ll_actions_carry.setVisibility(View.VISIBLE);
                    holder.ll_cancel.setVisibility(View.GONE);
                    holder.ll_status.setVisibility(View.GONE);
                } else {
                    if (map.get(position).getReceptionModel().getStatus().equals("Принесу")) {
                        holder.goods_status.setTextColor(Color.parseColor("#2e7d32"));
                    }
                    holder.ll_cancel.setVisibility(View.VISIBLE);
                    holder.ll_status.setVisibility(View.VISIBLE);
                    holder.ll_actions_carry.setVisibility(View.GONE);
                    holder.goods_status.setText(String.valueOf(map.get(position).getReceptionModel().getStatus()));
                    holder.btn_cancel.setOnClickListener(checkAvailable(map.get(position).getReceptionModel().getId(), 5, "carry"));
                }
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES))) {
                holder.bindColor(String.valueOf(map.get(position).getTciModel().getColorName()));
                holder.bindSize(String.valueOf(map.get(position).getTciModel().getSizeName()));
                image_url = GoodsComponents.getImageByColor(model.getColors(), map.get(position).getTciModel().getColorId());
                holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), map.get(position).getTciModel().getColorId()), String.valueOf(map.get(position).getTciModel().getSizeName()));
                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), map.get(position).getTciModel().getColorId()));
                holder.goods_status.setText(map.get(position).getTciModel().getStatus());
                holder.goods_number_in_line.setText(String.valueOf(map.get(position).getTciModel().getNumberInLine()) + "/" + String.valueOf(map.get(position).getTciModel().getNumberInLineOld()) + " ед.");
                holder.goods_check_date.setText(String.valueOf(map.get(position).getTciModel().getCheckDate()));
                holder.goods_price.setText(String.valueOf(map.get(position).getTciModel().getPrice()));
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
                holder.goods_price.setText(String.valueOf(map.get(position).getTciModel().getPrice()));
                holder.bindColor(String.valueOf(map.get(position).getTciModel().getColorName()));
                holder.bindSize(String.valueOf(map.get(position).getTciModel().getSizeName()));
                image_url = GoodsComponents.getImageByColor(model.getColors(), map.get(position).getTciModel().getColorId());
                holder.bindColorImage(GoodsComponents.getCodeByColor(model.getColors(), map.get(position).getTciModel().getColorId()), String.valueOf(map.get(position).getTciModel().getSizeName()));
                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), map.get(position).getTciModel().getColorId()));
                holder.goods_status.setText(map.get(position).getTciModel().getStatus());
                if (map.get(position).getTciModel().getStatus().equals("ожидает отправки")) {
                    holder.goods_status.setTextColor(Color.parseColor("FF43A047"));
                }
            }

            if (model.getSellOut() != null && Integer.valueOf(model.getSellOut()) == 1) {
                holder.goods_discount.setVisibility(View.VISIBLE);
            } else {
                holder.goods_discount.setVisibility(View.GONE);
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
                holder.ll_quantity.setVisibility(View.GONE);
            }

            if (image_url == null) {
                image_url = model.getMainImage();
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
                if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK))) {
                    holder.goods_defect.setText(map.get(position).getReceptionDefectModel().getDefectNote());
                }
                if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_DEFECT_RETURN))) {
                    holder.goods_defect.setText(map.get(position).getReturnDefectModel().getComment());
                    if (map.get(position).getReturnDefectModel().getImages().size() > 0) {
                        imageList.addAll(GoodsComponents.getImageUrl(map.get(position).getReturnDefectModel().getImages()));
                        image_url = imageList.get(0);
                    }
                    holder.goods_size.setText(map.get(position).getReturnDefectModel().getSizeName());
                    holder.goods_color.setText(map.get(position).getReturnDefectModel().getColorName());
                    holder.goods_username.setText(map.get(position).getReturnDefectModel().getUsername());
                    holder.goods_return_number.setText("Заявка №" + map.get(position).getReturnDefectModel().getId());
                    holder.goods_name.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                    holder.goods_status.setText(map.get(position).getReturnDefectModel().getStatus());
                    holder.countPhoto.setText("Фотографий (" + map.get(position).getReturnDefectModel().getImages().size() + ")");
                }
            }

            Log.d(TAG, "onBindViewHolder: " + String.valueOf(image_url));

            if (image_url.contains("http://kgmart.ru")) {
                Glide.with(context)
                        .load(image_url)
                        .override(396, 600)
                        .fitCenter()
                        .into(holder.goods_image);
            } else {
                Glide.with(context)
                        .load("http://kgmart.ru" + image_url)
                        .override(396, 600)
                        .fitCenter()
                        .into(holder.goods_image);
            }


            if ((type.equals(String.valueOf(GoodEnum.TYPE_ALL)) || type.equals(String.valueOf(GoodEnum.TYPE_ALL_UPDATE))) && TextUtils.isEmpty(product_id)) {
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, GoodsDetailsActivity.class);
                        intent.putExtra("product_id", holder.goods_id.getText().toString());
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);
                    }
                });
            } else {
                if (imageList.size() == 0) {
                    imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
                }

                if (imageList.size() > 0) {
                    holder.goods_image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GoodsFullscreenImageFragment imagesDialog = null;
                            Log.d(TAG, "onClick: " + String.valueOf(imageList));
                            imagesDialog = GoodsFullscreenImageFragment.newInstance(imageList, 0);
                            imagesDialog.show(activity.getSupportFragmentManager(), GoodsPageFragment.class.getSimpleName());
                        }
                    });
                }
            }
        }
//        } catch (Exception e) {
//            Crashlytics.logException(e);
//        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private View.OnClickListener checkAvailable(int position, int type, String action) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type == 0) {
                    if (action.equals("available")) {
                        sendAvailable(position, type);
                    }
                    if (action.equals("carry")) {
                        sendCarry(position, type);
                    }
                } else if (type == 5) {
                    if (action.equals("carry")) {
                        sendCarry(position, type);
                    }
                } else {
                    dialog = DialogManager.goodAvailableDialog(context, "Вы уверены что товара не будет в наличии");
                    dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (action.equals("available")) {
                                sendAvailable(position, type);
                            }
                            if (action.equals("carry")) {
                                sendCarry(position, type);
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if (action.equals("available")) {
                                sendAvailable(position, 3);
                            }
                            if (action.equals("carry")) {
                                sendCarry(position, 3);
                            }
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                }
            }
        };
    }

    @Override
    public int getItemCount() {
        if (map != null) {
            return map.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView goods_image;
        TextView goods_name;
        TextView goods_color;
        TextView goods_count;
        TextView goods_size;
        TextView goods_discount;
        TextView goods_id;
        TextView goods_number_in_line;
        TextView goods_stock;
        TextView goods_price;
        TextView goods_defect;
        TextView goods_count_on_consig;
        TextView goods_position;
        TextView goods_status;
        TextView goods_check_date;
        TextView goods_name_consig;
        TextView goods_return_number;
        TextView goods_username;
        TextView countPhoto;
        TextView goods_price_currency;
        LinearLayout ll_quantity;
        LinearLayout ll_in_line;
        LinearLayout ll_in_stock;
        LinearLayout ll_defect;
        LinearLayout ll_price;
        LinearLayout ll_count_on_consig;
        LinearLayout ll_actions;
        LinearLayout ll_actions_carry;
        LinearLayout ll_status;
        LinearLayout ll_planned_date;
        LinearLayout ll_check_date;
        LinearLayout ll_name_consig;
        LinearLayout ll_username;
        LinearLayout ll_cancel;
        TextView goods_planned_date;
        TextView btn_cancel;
        Button btn_no_available;
        Button btn_available;
        Button btn_no_carry;
        Button btn_carry;
        RoundedImageView color_picker_image_view;

        ViewHolder(View itemView) {
            super(itemView);
            goods_image = (ImageView) itemView.findViewById(R.id.goods_image);
            goods_name = (TextView) itemView.findViewById(R.id.goods_name);
            goods_return_number = (TextView) itemView.findViewById(R.id.goods_return_number);
            goods_color = (TextView) itemView.findViewById(R.id.goods_color);
            ll_in_line = (LinearLayout) itemView.findViewById(R.id.ll_in_line);
            ll_in_stock = (LinearLayout) itemView.findViewById(R.id.ll_in_stock);
            ll_price = (LinearLayout) itemView.findViewById(R.id.ll_price);
            ll_count_on_consig = (LinearLayout) itemView.findViewById(R.id.ll_count_on_consig);
            goods_count_on_consig = (TextView) itemView.findViewById(R.id.goods_count_on_consig);
            goods_id = (TextView) itemView.findViewById(R.id.goods_id);
            goods_position = (TextView) itemView.findViewById(R.id.goods_position);
            if (!type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN)) && !type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
                goods_price_currency = (TextView) itemView.findViewById(R.id.goods_price_currency);
            }
            color_picker_image_view = (RoundedImageView) itemView.findViewById(R.id.color_picker_image_view);
            try {
                ll_defect = (LinearLayout) itemView.findViewById(R.id.ll_defect);
                ll_username = (LinearLayout) itemView.findViewById(R.id.ll_username);
                ll_defect.setVisibility(View.GONE);
                ll_username.setVisibility(View.GONE);
            } catch (Exception ignored) {
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN)) ||
                    type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
                goods_count = (TextView) itemView.findViewById(R.id.goods_count);
                ll_quantity = (LinearLayout) itemView.findViewById(R.id.ll_quantity);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
                ll_actions_carry = (LinearLayout) itemView.findViewById(R.id.ll_actions_carry);
                btn_carry = (Button) itemView.findViewById(R.id.btn_carry);
                btn_no_carry = (Button) itemView.findViewById(R.id.btn_no_carry);
                goods_count = (TextView) itemView.findViewById(R.id.goods_count);
                ll_quantity = (LinearLayout) itemView.findViewById(R.id.ll_quantity);
                goods_planned_date = (TextView) itemView.findViewById(R.id.goods_planned_date);
                ll_planned_date = (LinearLayout) itemView.findViewById(R.id.ll_planned_date);
                goods_price = (TextView) itemView.findViewById(R.id.goods_price);
                ll_status = (LinearLayout) itemView.findViewById(R.id.ll_status);
                goods_status = (TextView) itemView.findViewById(R.id.goods_status);
                ll_cancel = (LinearLayout) itemView.findViewById(R.id.ll_cancel);
                btn_cancel = (TextView) itemView.findViewById(R.id.btn_cancel);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
                ll_actions = (LinearLayout) itemView.findViewById(R.id.ll_actions);
                btn_no_available = (Button) itemView.findViewById(R.id.btn_no_available);
                btn_available = (Button) itemView.findViewById(R.id.btn_available);

            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
                goods_status = (TextView) itemView.findViewById(R.id.goods_status);
                goods_status.setVisibility(View.VISIBLE);
                ll_check_date = (LinearLayout) itemView.findViewById(R.id.ll_check_date);
                ll_check_date.setVisibility(View.GONE);
                goods_price = (TextView) itemView.findViewById(R.id.goods_price);
                goods_number_in_line = (TextView) itemView.findViewById(R.id.goods_number_in_line);
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_ALL)) || type.equals(String.valueOf(GoodEnum.TYPE_ALL_UPDATE))) {
                goods_number_in_line = (TextView) itemView.findViewById(R.id.goods_number_in_line);
                goods_stock = (TextView) itemView.findViewById(R.id.goods_stock);
                goods_price = (TextView) itemView.findViewById(R.id.goods_price);

                ll_name_consig = (LinearLayout) itemView.findViewById(R.id.ll_name_consig);
                goods_name_consig = (TextView) itemView.findViewById(R.id.goods_name_consig);
                if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_ON_CONSIGN))) {
                    ll_count_on_consig.setVisibility(View.VISIBLE);
                    ll_name_consig.setVisibility(View.VISIBLE);
                    ll_in_stock.setVisibility(View.GONE);
                }
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
                ll_quantity = (LinearLayout) itemView.findViewById(R.id.ll_quantity);
            }

            if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES))) {
                ll_status = (LinearLayout) itemView.findViewById(R.id.ll_status);
                goods_status = (TextView) itemView.findViewById(R.id.goods_status);
                goods_number_in_line = (TextView) itemView.findViewById(R.id.goods_number_in_line);
                goods_check_date = (TextView) itemView.findViewById(R.id.goods_check_date);
                goods_price = (TextView) itemView.findViewById(R.id.goods_price);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
                ll_in_line.setVisibility(View.GONE);
                ll_in_stock.setVisibility(View.GONE);
                ll_price.setVisibility(View.GONE);
                ll_defect = (LinearLayout) itemView.findViewById(R.id.ll_defect);
                ll_username = (LinearLayout) itemView.findViewById(R.id.ll_username);
                ll_defect.setVisibility(View.VISIBLE);
                ll_username.setVisibility(View.VISIBLE);
                goods_defect = (TextView) itemView.findViewById(R.id.goods_defect);
                goods_username = (TextView) itemView.findViewById(R.id.goods_username);
                countPhoto = (TextView) itemView.findViewById(R.id.countPhoto);
                goods_return_number.setVisibility(View.VISIBLE);
                countPhoto.setVisibility(View.VISIBLE);
                ll_status = (LinearLayout) itemView.findViewById(R.id.ll_status);
                goods_status = (TextView) itemView.findViewById(R.id.goods_status);
            }
            goods_discount = (TextView) itemView.findViewById(R.id.goods_discount);
            goods_size = (TextView) itemView.findViewById(R.id.goods_size);
        }

        void bindColor(String color) {
            goods_color.setText(color);
        }

        void bindColorImage(String colorCode, String colorName) {
            try {
                GradientDrawable gradDrawable = (GradientDrawable) color_picker_image_view.getBackground();
                int resultColor = 0xffffffff;
                gradDrawable.setColor(resultColor);
                resultColor = Color.parseColor(colorCode);
                Log.d("WRONG COLOR", "CustomSpinnerColors parse color exception");
                gradDrawable.setColor(resultColor);
            } catch (Exception ignore) {
            }
        }

        void bindSize(String size) {
            goods_size.setText(size);
        }

        void bindCount(String count) {
            goods_count.setText(count);
        }

    }

    private void sendAvailable(Integer position, Integer type) {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        GoodsModel goodsModel = new GoodsModel();
        @SuppressLint("UseSparseArrays")
        Map<Integer, GoodsAdapterModel> mapNew = new HashMap<>();
        ItemAvailableModel modelAvailable = new ItemAvailableModel();
        List<Integer> keys = new ArrayList<Integer>(map.keySet());
        Log.d(TAG, "sendAvailable: item_id " + position);
        int i = 0;
        for (Map.Entry<Integer, GoodsAdapterModel> entry : map.entrySet()) {
            if (entry.getValue().getItemAvailableModel().getItemId().equals(String.valueOf(position))) {
                Log.d(TAG, "sendAvailable: item_id_equal " + position);
                Log.d(TAG, "sendAvailable: item_id_equal " + i);
                GoodsAdapterModel full_model = map.get(i);
                if (full_model.getItemAvailableModel() != null) {
                    if (full_model.getItemAvailableModel().getColorId() != null) {
                        modelAvailable.setColorId(full_model.getItemAvailableModel().getColorId());
                    }
                    modelAvailable.setItemId(full_model.getItemAvailableModel().getItemId());
                    modelAvailable.setProductId(full_model.getProductsModels().getId());
                    modelAvailable.setSizeId(full_model.getItemAvailableModel().getSizeId());
                    modelAvailable.setStatus(type.toString());
                    goodsModel.setItemAvailableModel(modelAvailable);

                    Call<RestResponse<GoodsModel>> call = apiGoods.setAvailable(goodsModel, CommonComponents.getUserKey(context));
                    int finalI = i;
                    call.enqueue(new Callback<RestResponse<GoodsModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                            Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
                            Toast.makeText(context, "Запись успешно обновлена", Toast.LENGTH_SHORT).show();
                            map.remove(keys.get(finalI));
                            @SuppressLint("UseSparseArrays")
                            Map<Integer, GoodsAdapterModel> defaultMap = new HashMap<Integer, GoodsAdapterModel>();
                            defaultMap.putAll(map);
                            map.clear();
                            int j = 0;
                            for (Map.Entry<Integer, GoodsAdapterModel> entry : defaultMap.entrySet()) {
                                map.put(j, entry.getValue());
                                j++;
                            }
                            Log.d(TAG, "sendAvailable: AfterMap " + map.keySet().toString());
                            Log.d(TAG, "sendAvailable: defaultMap " + defaultMap.keySet().toString());
                            notifyItemRemoved(finalI);
                            notifyDataSetChanged();
                            checkList();
                        }

                        @Override
                        public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                        }
                    });
                }
                break;
            }
            i++;
        }
    }

    private void sendCarry(Integer position, Integer type) {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        GoodsModel goodsModel = new GoodsModel();
        @SuppressLint("UseSparseArrays")
        Map<Integer, GoodsAdapterModel> mapNew = new HashMap<>();
        ReceptionModel receptionModel = new ReceptionModel();
        List<Integer> keys = new ArrayList<Integer>(map.keySet());
        Log.d(TAG, "sendCarry: item_id " + position);
        int i = 0;
        for (Map.Entry<Integer, GoodsAdapterModel> entry : map.entrySet()) {
            if (entry.getValue().getReceptionModel().getId().equals(position)) {
                Log.d(TAG, "sendCarry: item_id_equal " + position);
                Log.d(TAG, "sendCarry: item_id_equal " + i);
                GoodsAdapterModel full_model = map.get(i);
                if (full_model.getReceptionModel() != null) {
                    if (full_model.getReceptionModel().getColorId() != null) {
                        receptionModel.setColorId(full_model.getReceptionModel().getColorId());
                    }
                    receptionModel.setItemId(full_model.getReceptionModel().getItemId());
                    receptionModel.setReceptionId(full_model.getReceptionModel().getId());
                    receptionModel.setSizeId(full_model.getReceptionModel().getSizeId());
                    receptionModel.setProductId(Integer.valueOf(full_model.getProductsModels().getId()));
                    receptionModel.setStatus(type.toString());
                    goodsModel.setReceptionModel(receptionModel);

                    Call<RestResponse<GoodsModel>> call = apiGoods.setCarry(goodsModel, CommonComponents.getUserKey(context));
                    int finalI = i;
                    call.enqueue(new Callback<RestResponse<GoodsModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                            Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
                            Toast.makeText(context, "Запись успешно обновлена", Toast.LENGTH_SHORT).show();
                            switch (type) {
                                case 0:
                                    map.get(keys.get(finalI)).getReceptionModel().setStatus("Принесу");
                                    break;
                                case 1:
                                    map.get(keys.get(finalI)).getReceptionModel().setStatus("Скоро будет");
                                    break;
                                case 3:
                                    map.get(keys.get(finalI)).getReceptionModel().setStatus("Нет в наличии");
                                    break;
                                case 5:
                                    map.get(keys.get(finalI)).getReceptionModel().setStatus("");
                                    break;
                                default:
                                    map.remove(keys.get(finalI));
                                    break;
                            }
                            @SuppressLint("UseSparseArrays")
                            Map<Integer, GoodsAdapterModel> defaultMap = new HashMap<Integer, GoodsAdapterModel>();
                            defaultMap.putAll(map);
                            map.clear();
                            int j = 0;
                            for (Map.Entry<Integer, GoodsAdapterModel> entry : defaultMap.entrySet()) {
                                map.put(j, entry.getValue());
                                j++;
                            }
                            Log.d(TAG, "sendCarry: defaultMap " + defaultMap.keySet().toString());
                            notifyItemRemoved(finalI);
                            notifyDataSetChanged();
                            checkList();
                        }

                        @Override
                        public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                        }
                    });
                }
                break;
            }
            i++;
        }
    }

    private void checkList() {
        if (map.size() <= 0) {
            rl_empty.setVisibility(View.VISIBLE);
        } else {
            rl_empty.setVisibility(View.GONE);
        }
    }
}
