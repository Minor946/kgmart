package ru.kgmart.produce.Adapters;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.google.gson.Gson;

import java.util.Map;

import ru.kgmart.produce.Fragments.GoodsPageFragment;
import ru.kgmart.produce.Models.GoodsAdapterModel;

/**
 * Created on 4/23/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class ListGoodAdapter extends FragmentStatePagerAdapter {


    private String type;
    private Map<Integer, GoodsAdapterModel> map;

    public ListGoodAdapter(FragmentManager fm, Map<Integer, GoodsAdapterModel> map, String type) {
        super(fm);
        this.map = map;
        this.type = type;
    }

    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt("position", position);
        bundle.putString("type", type);
        bundle.putSerializable("model", new Gson().toJson(map.get(position)));
        Fragment fragment = new GoodsPageFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public int getCount() {
        if (map == null)
            return 0;
        else
            return map.size();
    }

}