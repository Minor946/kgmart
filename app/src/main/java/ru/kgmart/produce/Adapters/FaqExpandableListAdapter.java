package ru.kgmart.produce.Adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.util.List;

import ru.kgmart.produce.Models.FaqModel;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class FaqExpandableListAdapter extends BaseExpandableListAdapter {

  private LayoutInflater inflater;
  private List<FaqModel> parent;
  private ExpandableListView expandableListView;
  private int lastExpandedGroupPosition;

  /**
   * @param context            Context app
   * @param parent             List of faq list
   * @param expandableListView Expandable List View
   */
  public FaqExpandableListAdapter(Context context, List<FaqModel> parent, ExpandableListView expandableListView) {
    this.parent = parent;
    try {
      inflater = LayoutInflater.from(context);
    } catch (NullPointerException e) {
      Crashlytics.logException(e);
    }
    this.expandableListView = expandableListView;
  }

  @Override
  public int getGroupCount() {
    return parent.size();
  }

  @Override
  public int getChildrenCount(int i) {
    return 1;
  }

  @Override
  public Object getGroup(int i) {
    return parent.get(i).getQ();
  }

  @Override
  public Object getChild(int i, int i1) {
    return parent.get(i).getA();
  }

  @Override
  public long getGroupId(int i) {
    return i;
  }

  @Override
  public long getChildId(int i, int i1) {
    return i1;
  }

  @Override
  public boolean hasStableIds() {
    return true;
  }

  @Override
  public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {

    if (view == null) {
      view = inflater.inflate(R.layout.item_faq_parent, viewGroup, false);
    }
    // set category name as tag so view can be found view later
    view.setTag(getGroup(i).toString());

    TextView textView = (TextView) view.findViewById(R.id.tv_item_faq_parent);

    //"i" is the position of the parent/group in the list
    textView.setText(getGroup(i).toString());

    //return the entire view
    return view;
  }


  @Override
  //in this method you must set the text to see the children on the list
  public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
    if (view == null) {
      view = inflater.inflate(R.layout.item_faq_child, viewGroup, false);
    }

    TextView textView = (TextView) view.findViewById(R.id.list_item_text_child);

    //"i" is the position of the parent/group in the list and
    //"i1" is the position of the child
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
      textView.setText(Html.fromHtml(parent.get(i).getA(), Html.FROM_HTML_MODE_LEGACY));
    } else {
      textView.setText(Html.fromHtml(parent.get(i).getA()));
    }

    //return the entire view
    return view;
  }

  @Override
  public boolean isChildSelectable(int i, int i1) {
    return true;
  }


  @Override
  public void onGroupExpanded(int groupPosition) {
    if (groupPosition != lastExpandedGroupPosition) {
      expandableListView.collapseGroup(lastExpandedGroupPosition);
    }
    super.onGroupExpanded(groupPosition);
    lastExpandedGroupPosition = groupPosition;
  }
}
