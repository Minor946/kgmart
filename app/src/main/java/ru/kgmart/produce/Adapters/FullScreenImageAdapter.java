package ru.kgmart.produce.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.Objects;

import ru.kgmart.produce.R;

/**
 * Created on 5/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

public class FullScreenImageAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<String> images;
    private ProgressBar loading;

    /**
     * Creates an adapter for viewing product images.
     *
     * @param context activity context.
     * @param images  list of product images.
     */
    public FullScreenImageAdapter(Context context, ArrayList<String> images) {
        this.context = context;
        this.images = images;
    }

    @Override
    public int getCount() {
        if (this.images != null) {
            return this.images.size();
        }
        return  0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        PhotoView imgDisplay;
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = Objects.requireNonNull(inflater).inflate(R.layout.popup_photo_full, container, false);
        imgDisplay = (PhotoView) viewLayout.findViewById(R.id.full_image_good);
        loading = (ProgressBar) viewLayout.findViewById(R.id.loading);

        imgDisplay.setMaximumScale(6);

        Glide.with(context).asBitmap()
                .load(images.get(position))
                .fitCenter()
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        loading.setIndeterminate(false);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                        imgDisplay.setImageBitmap(resource);
                        loading.setVisibility(View.GONE);
                        return false;
                    }
                })

                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgDisplay);

        container.addView(viewLayout);
        return viewLayout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((RelativeLayout) object);
    }
}