package ru.kgmart.produce.Components.Event;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.tibolte.agendacalendarview.render.EventRenderer;

import ru.kgmart.produce.R;

/**
 * Created on 4/26/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class EventItemRenderer extends EventRenderer<CustomCalendarEvent> {

  // region Class - EventRenderer
  private String type = null;

  @Override
  public void render(View view, CustomCalendarEvent event) {

    TextView txtTitle = (TextView) view.findViewById(R.id.view_agenda_event_title);

    TextView txtTime = (TextView) view.findViewById(R.id.view_agenda_event_time);

    TextView txtDesc = (TextView) view.findViewById(R.id.view_agenda_event_desc);

    TextView txtLocation = (TextView) view.findViewById(R.id.view_agenda_event_location);

    TextView txtStatus = (TextView) view.findViewById(R.id.view_agenda_event_status);

    LinearLayout descriptionContainer = (LinearLayout) view.findViewById(R.id.view_agenda_event_description_container);

    txtTitle.setText(event.getTitle());
    txtLocation.setVisibility(View.GONE);
    txtStatus.setText(event.getLocation());
    txtDesc.setText(event.getDescription());

    descriptionContainer.setBackgroundColor(event.getColor());
  }

  @Override
  public int getEventLayout() {
    return R.layout.item_event;
  }

  @Override
  public Class<CustomCalendarEvent> getRenderType() {
    return CustomCalendarEvent.class;
  }

  // endregion
}
