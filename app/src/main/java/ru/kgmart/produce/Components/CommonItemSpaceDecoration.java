package ru.kgmart.produce.Components;

import android.graphics.Rect;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

/**
 * Created on 5/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class CommonItemSpaceDecoration extends RecyclerView.ItemDecoration {
    private int mSpace = 0;

    public CommonItemSpaceDecoration(int space) {
        this.mSpace = space;
    }


    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(mSpace, mSpace, mSpace, mSpace);
    }

}

