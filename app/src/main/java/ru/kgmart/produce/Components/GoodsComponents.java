package ru.kgmart.produce.Components;

import android.util.Log;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import ru.kgmart.produce.Models.ColorModel;
import ru.kgmart.produce.Models.ProductImageModel;

/**
 * Created on 5/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsComponents {


  private static final String TAG = "GoodsComponents";

  /**
   * @param colors  List ColorModel selected goods
   * @param colorId needed color id
   * @return String url image
   */
  public static String getImageByColor(List<ColorModel> colors, String colorId) {
    if (colors != null && colors.size() > 0) {
      for (int i = 0; i < colors.size(); i++) {
        ColorModel colorModel = colors.get(i);
        if (colorModel != null && colorModel.getImages() != null) {
          if (colorModel.getId().equals(colorId)) {
            return colorModel.getImages().get(0);
          }
        }
      }
    }
    return null;
  }

  public static String getCodeByColor(List<ColorModel> colors, String colorId) {
    if (colors != null && colors.size() > 0) {
      for (int i = 0; i < colors.size(); i++) {
        ColorModel colorModel = colors.get(i);
        if (colorModel != null && colorModel.getImages() != null) {
          if (colorModel.getId().equals(colorId)) {
            return colorModel.getCode();
          }
        }
      }
    }
    return null;
  }


  /**
   * @param isMain   boolean is image main
   * @param imageUrl String url image thumb
   * @param goodId   String id goods
   * @param colorId  String selected color id
   * @return String url full image
   */
  public static String getFullImage(Boolean isMain, String imageUrl, String goodId, String colorId) {
    String[] separated = imageUrl.split("x0_");
    try {
      if (separated.length > 1) {
        if (isMain) {
          return "http://kgmart.ru/images/uploads/product/" + goodId + "/" + separated[1];
        } else {
          return "http://kgmart.ru/images/uploads/variants/" + colorId + "/" + goodId + "/" + separated[1];
        }
      }
    } catch (Exception e) {
      Log.e(TAG, "getFullImage:  ", e);
      Crashlytics.logException(e);
    }
    return null;
  }


  public static ArrayList<String> getImageUrl(List<String> imagesUrl) {
    ArrayList<String> imagesList = new ArrayList<>();
    if (imagesUrl != null && imagesUrl.size() > 0) {
      for (int i = 0; i < imagesUrl.size(); i++) {
        Log.d(TAG, "getImageUrl: " + String.valueOf("http://kgmart.ru" + imagesUrl.get(i)));
        imagesList.add("http://kgmart.ru" + imagesUrl.get(i));
      }
    }
    return imagesList;
  }

  public static ArrayList<String> getMainFullImageList(String mainImage, List<ProductImageModel> images, String id) {
    ArrayList<String> imagesList = new ArrayList<>();
    imagesList.add(GoodsComponents.getFullImage(true, mainImage, id, null));
    if (images != null && images.size() > 0) {
      for (int i = 0; i < images.size(); i++) {
        imagesList.add(GoodsComponents.getFullImage(true, images.get(i).getImage(), id, null));
      }
    }
    return imagesList;
  }

  public static ArrayList<String> getMainThumbImageList(String mainImage, List<ProductImageModel> images) {
    ArrayList<String> imagesList = new ArrayList<>();
    imagesList.add("http://kgmart.ru" + mainImage);
    if (images != null && images.size() > 0) {
      for (int i = 0; i < images.size(); i++) {
        imagesList.add("http://kgmart.ru" + images.get(i).getImage());
      }
    }
    return imagesList;
  }

  public static ArrayList<String> getColorFullImageList(List<ColorModel> colors, String id, String colorId) {
    ArrayList<String> imagesList = new ArrayList<>();
    if (colors != null && colors.size() > 0) {
      for (int i = 0; i < colors.size(); i++) {
        ColorModel colorModel = colors.get(i);
        if (colorModel != null && colorModel.getImages() != null) {
          if (colorModel.getId().equals(colorId)) {
            for (int j = 0; j < colorModel.getImages().size(); j++) {
              imagesList.add(GoodsComponents.getFullImage(false, colorModel.getImages().get(j), id, colorModel.getId()));
            }
            break;
          }
        }
      }
    }
    return imagesList;
  }

  public static ArrayList<String> getColorThumbImageList(List<ColorModel> colors, String id, String colorId) {
    ArrayList<String> imagesList = new ArrayList<>();
    if (colors != null && colors.size() > 0) {
      for (int i = 0; i < colors.size(); i++) {
        ColorModel colorModel = colors.get(i);
        if (colorModel != null && colorModel.getImages() != null) {
          if (colorModel.getId().equals(colorId)) {
            for (int j = 0; j < colorModel.getImages().size(); j++) {
              imagesList.add("http://kgmart.ru" + colorModel.getImages().get(j));
            }
            break;
          }
        }
      }
    }
    return imagesList;
  }

  public static String getStockLabel(String visible) {
    if (visible.equals("2")) {
      return "Нет в наличии";
    } else if (visible.equals("1")) {
      return "Скоро будет";
    }
    {
      return "В наличии";
    }
  }
}
