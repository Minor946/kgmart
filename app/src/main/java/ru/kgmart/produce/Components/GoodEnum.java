package ru.kgmart.produce.Components;

public class GoodEnum {

    public static final int TYPE_ALL_UPDATE = -2;
    public static final int TYPE_ALL = -1;
    public static final int TYPE_AVAILABLE = 0;
    public static final int TYPE_CONSIGN = 1;
    public static final int TYPE_TO_CARRY = 2;
    public static final int TYPE_DISCOUNT = 3;
    public static final int TYPE_DEFECT = 4;
    public static final int TYPE_STOCK_SALES = 5;
    public static final int TYPE_SALES = 6;

    public static final int SUB_TYPE_ALL = -1;
    public static final int SUB_TYPE_AVAILABLE = 0;
    public static final int SUB_TYPE_NO_AVAILABLE = 2;
    public static final int SUB_TYPE_ON_CONSIGN = 3;


    public static final int SUB_TYPE_DEFECT_RETURN = 4;
    public static final int SUB_TYPE_DEFECT_OTK = 5;

}
