package ru.kgmart.produce.Components.Event;

import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;

import java.util.Calendar;

/**
 * Created on 4/26/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class CustomCalendarEvent extends BaseCalendarEvent {

  private int mDrawableId;

  // region Constructors

  public CustomCalendarEvent(long id, int color, String title, String description, String location, long dateStart, long dateEnd, int allDay, String duration, int drawableId) {
    super(id, color, title, description, location, dateStart, dateEnd, allDay, duration);
    this.mDrawableId = drawableId;
  }

  public CustomCalendarEvent(String title, String description, String location, int color, Calendar startTime, Calendar endTime, boolean allDay, int drawableId) {
    super(title, description, location, color, startTime, endTime, allDay);
    this.mDrawableId = drawableId;
  }

  public CustomCalendarEvent(CustomCalendarEvent calendarEvent) {
    super(calendarEvent);
    this.mDrawableId = calendarEvent.getDrawableId();
  }

  // endregion

  // region Public methods

  public int getDrawableId() {
    return mDrawableId;
  }

  public void setDrawableId(int drawableId) {
    this.mDrawableId = drawableId;
  }

  // endregion

  // region Class - BaseCalendarEvent

  @Override
  public CustomCalendarEvent copy() {
    return new CustomCalendarEvent(this);
  }

}
