package ru.kgmart.produce.Components;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.preference.PreferenceManager;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.GoodsDetailsActivity;
import ru.kgmart.produce.Models.NotificationModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.ProfileActivity;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class CommonComponents {

    private static final String TAG = "CommonComponents";

    public static String md5(final String source) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(source.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getUserKey(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_ACCESS_TOKEN", "");
        }
        return null;
    }

    public static String getUserEmail(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_LOGIN", "");
        }
        return null;
    }

    public static String getUserPassword(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_PASSWORD", "");
        }
        return null;
    }

    public static String getUserName(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_USERNAME", "");
        }
        return null;
    }

    public static String getUserBalance(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_BALANCE", "");
        }
        return null;
    }

    public static String getUserID(Context context) {
        if (context != null) {
            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(context);
            return getPrefs.getString("APP_PREFERENCES_ID", "");
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    public static Boolean initNavigation(Context context, NavigationView navigationView) {
        String username = CommonComponents.getUserName(context);
        View navHeaderView = navigationView.getHeaderView(0);
        TextView textViewUsername = navHeaderView.findViewById(R.id.nav_header_username);
        TextView textViewEmail = navHeaderView.findViewById(R.id.nav_header_email);
        TextView btnTicketDirector = navHeaderView.findViewById(R.id.btn_ticket_director);
        textViewUsername.setText(username);
        View.OnClickListener profileClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        };
        textViewUsername.setOnClickListener(profileClickListener);
        textViewEmail.setOnClickListener(profileClickListener);
        btnTicketDirector.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openWhatsApp(v, context);
            }
        });

        updateNotificationCount(context, navigationView);
        return true;
    }

    public static void openWhatsApp(View v, Context context) {
        String contact = "996702000070";
        String url = "https://api.whatsapp.com/send?phone=" + contact;
        try {
            PackageManager pm = context.getPackageManager();
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException ignored) {}
    }

    public static void updateNotificationCount(Context context, NavigationView navigationView) {
        APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);
        Call<RestResponse<NotificationModel>> call = apiOther.getNotification(CommonComponents.getUserKey(context));
        call.enqueue(new Callback<RestResponse<NotificationModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<NotificationModel>> call, @NonNull Response<RestResponse<NotificationModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<NotificationModel> notificationModel = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(notificationModel).getMeta();
                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        try {
                            if (notificationModel.getData().getReception() != null)
                                setMenuCounter(R.id.action_need_to_carry, notificationModel.getData().getReception(), navigationView, context);
                            if (notificationModel.getData().getItem() != null)
                                setMenuCounter(R.id.action_check_available, notificationModel.getData().getItem(), navigationView, context);
                            if (notificationModel.getData().getDefect() != null)
                                setMenuCounter(R.id.action_my_defects, notificationModel.getData().getDefect(), navigationView, context);
                        } catch (NullPointerException ignored) {
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<NotificationModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Crashlytics.logException(t);
            }
        });
    }

    private static void setMenuCounter(@IdRes int itemId, int count, NavigationView navigationView, Context context) {
        TextView view = (TextView) navigationView.getMenu().findItem(itemId).getActionView();
        if (count == 0) {
            view.setVisibility(View.GONE);
        } else {
            view.setVisibility(View.VISIBLE);
            view.setGravity(Gravity.CENTER);
            view.setTypeface(null, Typeface.BOLD);
            view.setTextColor(context.getResources().getColor(R.color.red));
            view.setText(count > 0 ? String.valueOf(count) : null);
        }
    }


    /**
     * @param year        int Year
     * @param monthOfYear int number of month in year
     * @param dayOfMonth  int
     * @return date String new format
     */

    public static String getPublicDate(int year, int monthOfYear, int dayOfMonth, String separate) {

        String dateNewFormat = "";

        if (dayOfMonth < 10) {
            dateNewFormat += "0" + dayOfMonth;

        } else {
            dateNewFormat += dayOfMonth;
        }

        dateNewFormat += separate;

        if ((monthOfYear + 1) < 10) {
            dateNewFormat += "0" + (monthOfYear + 1);
        } else {
            dateNewFormat += (monthOfYear + 1);
        }

        dateNewFormat += separate + String.valueOf(year).trim();
        return dateNewFormat;
    }

    /**
     * @param year        int Year
     * @param monthOfYear int number of month in year
     * @param dayOfMonth  int
     * @return date String new format
     */

    public static String getMysqlDate(int year, int monthOfYear, int dayOfMonth, String separate) {
        String dateNewFormat = "";

        dateNewFormat += String.valueOf(year).trim() + separate;


        if ((monthOfYear + 1) < 10) {
            dateNewFormat += "0" + (monthOfYear + 1);
        } else {
            dateNewFormat += (monthOfYear + 1);
        }

        dateNewFormat += separate;

        if (dayOfMonth < 10) {
            dateNewFormat += "0" + dayOfMonth;

        } else {
            dateNewFormat += dayOfMonth;
        }

        return dateNewFormat;
    }

    public static String getMount(Context context, String month) {
        String result = "";
        if (context != null) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            try {
                calendar.setTime(sdf.parse(month));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                result = context.getResources().getStringArray(R.array.month_names)[calendar.get(Calendar.MONTH)];
            } catch (ArrayIndexOutOfBoundsException e) {
                result = Integer.toString(calendar.get(Calendar.MONTH));
            }
            result = Integer.toString(calendar.get(Calendar.DAY_OF_MONTH)) + " - " + result;
        }
        return result;
    }

}
