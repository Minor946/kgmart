package ru.kgmart.produce;

import androidx.annotation.NonNull;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import ru.kgmart.produce.Models.BookkeepingModel;
import ru.kgmart.produce.Models.BookkeepingsModel;
import ru.kgmart.produce.Models.CategoryModel;
import ru.kgmart.produce.Models.ColorModel;
import ru.kgmart.produce.Models.DashboardModel;
import ru.kgmart.produce.Models.DiscountShopModel;
import ru.kgmart.produce.Models.FaqListModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.NotificationModel;
import ru.kgmart.produce.Models.PasswordChangeModel;
import ru.kgmart.produce.Models.PasswordRecoveryModel;
import ru.kgmart.produce.Models.PhotoClaimModel;
import ru.kgmart.produce.Models.PhotoClaimsModel;
import ru.kgmart.produce.Models.ProductShopModel;
import ru.kgmart.produce.Models.ProfileModel;
import ru.kgmart.produce.Models.PushListModel;
import ru.kgmart.produce.Models.PushTokenModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.SizeModel;
import ru.kgmart.produce.Models.StatisticModel;
import ru.kgmart.produce.Models.UserLoginModel;
import ru.kgmart.produce.Models.UserRegisterModel;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class APi {


    private static String serverUrl = "http://mobapi.kgmart.ru/";

    public static Retrofit getRetrofitBuild() {

        if (BuildConfig.DEBUG) {
            serverUrl = "http://mobapi1.kgmart.ru/";
        }
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.readTimeout(40, TimeUnit.SECONDS);
        okHttpBuilder.writeTimeout(40, TimeUnit.SECONDS);
        okHttpBuilder.addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(@NonNull Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "api=1.0&lang=ru&platform=android")
                        .build();
                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = okHttpBuilder.addInterceptor(logging).build();

        return new Retrofit.Builder()
                .baseUrl(serverUrl)
                .addConverterFactory(JacksonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public interface ApiGoods {
        @GET("api2/product")
        Call<RestResponse<GoodsModel>> getGoodInfo(@Query("key_id") String key_id, @Query("id") String id);

        @GET("api2/product")
        Call<RestResponse<GoodsModel>> getGoods(@Query("key_id") String key_id, @Query("visible") String visible, @Query("limit") Integer limit, @Query("page") int page, @Query("q") String q);

        @GET("api2/product")
        Call<RestResponse<GoodsModel>> getSellOut(@Query("key_id") String key_id, @Query("visible") String visible, @Query("limit") Integer limit, @Query("page") int page, @Query("sell_out") int sell_out);

        @GET("api2/warehouse")
        Call<RestResponse<GoodsModel>> getGoodsConsign(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/consigment")
        Call<RestResponse<GoodsModel>> getAllGoodsConsign(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/reception")
        Call<RestResponse<GoodsModel>> getGoodsToCarry(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/productSales")
        Call<RestResponse<GoodsModel>> getGoodsSale(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/item")
        Call<RestResponse<GoodsModel>> getGoodsToAvailable(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/itemreturn")
        Call<RestResponse<GoodsModel>> getDefectGoodsReturn(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/receptiondefect")
        Call<RestResponse<GoodsModel>> getDefectGoodsReception(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @GET("api2/productStock")
        Call<RestResponse<GoodsModel>> getGoodsStock(@Query("key_id") String key_id, @Query("limit") Integer limit, @Query("page") int page);

        @POST("api2/item")
        Call<RestResponse<GoodsModel>> setAvailable(@Body GoodsModel goodsModel, @Query("key_id") String key_id);

        @POST("api2/reception")
        Call<RestResponse<GoodsModel>> setCarry(@Body GoodsModel goodsModel, @Query("key_id") String key_id);

        @POST("api2/itemdiscount")
        Call<RestResponse<DiscountShopModel>> addDiscount(@Body GoodsModel goodsModel, @Query("key_id") String key_id);

        @Multipart
        @POST("api2/oneProduct")
        Call<RestResponse<ProductShopModel>> createGoods(
                @Query("key_id") String key_id,
                @PartMap Map<String, RequestBody> product,
                @Part List<MultipartBody.Part> files);
    }

    public interface ApiOther {
        @GET("api2/faq")
        Call<RestResponse<FaqListModel>> getFaq(@Query("key_id") String key_id);

        @GET("api/sizes")
        Call<RestResponse<List<SizeModel>>> getSizes();

        @GET("api/colors")
        Call<RestResponse<List<ColorModel>>> getColors();

        @GET("api/productCategory2")
        Call<RestResponse<List<CategoryModel>>> getCategory();

        @GET("api2/notification")
        Call<RestResponse<NotificationModel>> getNotification(@Query("key_id") String key_id);

        @POST("api2/push")
        Call<RestResponse<PushListModel>> saveToken(
                @Body PushTokenModel pushTokenModel,
                @Query("key_id") String key_id);

        @POST("api2/claimBooking")
        Call<RestResponse<BookkeepingsModel>> claimBooking(@Body BookkeepingsModel bookkeepingsModel, @Query("key_id") String key_id);

        @GET("api2/claimBooking")
        Call<RestResponse<BookkeepingModel>> getClaimBooking(@Query("key_id") String key_id);

        @GET("api2/disabledBooking")
        Call<RestResponse<BookkeepingsModel>> getDisabledDate(@Query("key_id") String key_id);

    }

    public interface ApiProfile {
        @GET("api-profile")
        Call<RestResponse<ProfileModel>> getProfile(@Query("key_id") String key_id);

        @POST("api-profile")
        Call<RestResponse<ProfileModel>> updateProfile(
                @Body ProfileModel user,
                @Query("key_id") String token);

        @POST("api-changepassword")
        Call<RestResponse<String>> changePassword(
                @Body PasswordChangeModel passwordChangeModel,
                @Query("key_id") String token);

        @GET("api2/dashboard")
        Call<RestResponse<DashboardModel>> getDashboard(@Query("key_id") String key_id);

        @GET("api2/statistic")
        Call<RestResponse<StatisticModel>> getStatistic(@Query("key_id") String userKey,
                                                        @Query("select_date") String position,
                                                        @Query("start_date") String first_date,
                                                        @Query("finish_date") String second_date);

    }

    public interface ApiPromo {
    }

    public interface ApiPhotoSession {

        @GET("api2/claim")
        Call<RestResponse<PhotoClaimsModel>> getPhotoSession(@Query("key_id") String key_id);

        @POST("api2/claim")
        Call<RestResponse<PhotoClaimsModel>> addPhotoSession(@Body PhotoClaimModel photoClaimModel, @Query("key_id") String key_id);

    }

    public interface Auth {
        @GET("api-login")
        Call<RestResponse<UserLoginModel>> login(@Query("login") String login, @Query("pas") String pas, @Query("type") String type);

        @POST("api-recovery")
        Call<RestResponse<PasswordRecoveryModel>> resetPassword(@Body PasswordRecoveryModel passwordRecoveryDto);

        @POST("api2/signUp")
        Call<RestResponse<UserRegisterModel>> signUp(@Body UserRegisterModel userRegisterModel);
    }
}
