package ru.kgmart.produce;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;

import com.google.android.material.navigation.NavigationView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.perf.metrics.AddTrace;

import java.util.Objects;

import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.StatisticFragment;

public class StatisticActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "TestLog";
    Toolbar toolbar;

    @Override
    @AddTrace(name = "onCreateTraceMain", enabled = true)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistic);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.action_statistic));

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        if (FirebaseInstanceId.getInstance().getToken() != null) {
            MyFirebaseInstanceIDService.sendRegistrationToServer(Objects.requireNonNull(FirebaseInstanceId.getInstance().getToken()), getApplicationContext());
        } else {
            Log.d(TAG, "onCreate: token_push" + FirebaseInstanceId.getInstance().getToken());
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                Log.d(TAG, "onDrawerClosed: close");
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                Log.d(TAG, "onDrawerClosed: open");
                CommonComponents.updateNotificationCount(getApplicationContext(), navigationView);
            }
        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);

        CommonComponents.initNavigation(getApplicationContext(), navigationView);

        StatisticFragment statisticFragment = new StatisticFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainFrame, statisticFragment, "Statistic")
                .commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_statistic, menu);

        return true;
    }



    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        switch (id) {
            case R.id.action_main:
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                break;
            case R.id.action_faq:
                startActivity(new Intent(getApplicationContext(), FaqActivity.class));
                break;
            case R.id.action_my_photo_session:
                startActivity(new Intent(getApplicationContext(), SchedulePhotoActivity.class));
                break;
            case R.id.action_my_profile:
                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                break;
            case R.id.action_statistic:
                startActivity(new Intent(getApplicationContext(), StatisticActivity.class));
                break;
            case R.id.action_my_goods:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_ALL));
                startActivity(intent);
                break;
            case R.id.action_need_to_carry:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_TO_CARRY));
                startActivity(intent);
                break;
            case R.id.action_check_available:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_AVAILABLE));
                startActivity(intent);
                break;
            case R.id.action_my_defects:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_DEFECT));
                intent.putExtra("sub_type", String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK));
                startActivity(intent);
                break;
            case R.id.action_sales:
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_SALES));
                startActivity(intent);
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(Fragment fragment, String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);

        transaction.replace(R.id.mainFrame, fragment, tag);
        transaction.addToBackStack("Main");
        transaction.commit();
    }

}