package ru.kgmart.produce;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Objects;

import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Fragments.GoodsFragment;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    private static final int NOTIFY_ID = 101;
    String clickAction = "TARGET_MAIN";
    String title = "";
    String body = "";
    PendingIntent pendingIntent;

    String type = String.valueOf(GoodEnum.TYPE_TO_CARRY);

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
//        if (remoteMessage.getData().size() > 0) {
        Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        if (Objects.requireNonNull(remoteMessage.getNotification()).getClickAction() != null) {
            clickAction = Objects.requireNonNull(remoteMessage.getNotification()).getClickAction();

            switch (clickAction) {
                case "TARGET_TO_CARRY":
                    type = String.valueOf(GoodEnum.TYPE_TO_CARRY);
                    break;
                case "TARGET_TO_CONSIGN":
                    type = String.valueOf(GoodEnum.TYPE_CONSIGN);
                    break;
                case "TARGET_TO_AVAILABLE":
                    type = String.valueOf(GoodEnum.TYPE_AVAILABLE);
                    break;
                default:
                    type = String.valueOf(GoodEnum.TYPE_ALL);
                    break;
            }

        }
        title = remoteMessage.getNotification().getTitle();
        body = remoteMessage.getNotification().getBody();

        if (/* Check if data needs to be processed by long running job */ true) {
            // For long-running tasks (10 seconds or more) use Firebase Job Dispatcher.
            scheduleJob();
        } else {
            // Handle message within 10 seconds
            handleNow();
        }

        Log.d(TAG, "Message data action: " + clickAction);
//        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            sendNotification(title, body, clickAction);
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]


    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow() {
        Log.d(TAG, "Short lived task is done.");
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param body FCM message body received.
     */
    private void sendNotification(String title, String body, String clickAction) {
        Intent intent;
        switch (clickAction) {
            case "TARGET_TO_CARRY":
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", type);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.d(TAG, type);
                break;
            case "TARGET_TO_CONSIGN":
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", type);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case "TARGET_TO_AVAILABLE":
                intent = new Intent(getApplicationContext(), GoodsActivity.class);
                intent.putExtra("type", type);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case "TARGET_TO_SCHEDULE":
                intent = new Intent(getApplicationContext(), SchedulePhotoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case "TARGET_TO_PHOTOSHOOT":
                intent = new Intent(getApplicationContext(), SchedulePhotoActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            default:
                intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
        }
        pendingIntent = PendingIntent.getService(
                getApplicationContext(),
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(getApplicationContext())
                        .setContentTitle(title)
                        .setSmallIcon(R.mipmap.ic_launcher_statusbar)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher_new))
                        .setWhen(System.currentTimeMillis())
                        .setContentIntent(pendingIntent)
                        .setSound(defaultSoundUri)
                        .setContentText(body);

        Notification notification = mBuilder.build();
        NotificationManager notificationManager = (NotificationManager) getApplicationContext()
                .getSystemService(Context.NOTIFICATION_SERVICE);
        Objects.requireNonNull(notificationManager).notify(NOTIFY_ID, notification);
    }

    private void scheduleJob() {
        // [START dispatch_job]
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(this));
        Job myJob = dispatcher.newJobBuilder()
                .setService(MyJobService.class)
                .setTag("my-job-tag")
                .build();
        dispatcher.schedule(myJob);
        // [END dispatch_job]
    }
}