package ru.kgmart.produce.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.UserRegisterModel;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class SignUpFragment extends Fragment {

    private static final String TAG = "SignUpFragment";
    Button email_sign_in_button;

    TextView last_name;
    TextView first_name;
    TextView phone;
    TextView address;

    String last_name_text;
    String first_name_text;
    String phone_text;
    String address_text;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_sign_up, container, false);

        Crashlytics.setString("fragment", "SignUpFragment");

        TextView to_reset_pass = (TextView) v.findViewById(R.id.to_reset_pass);
        TextView to_login = (TextView) v.findViewById(R.id.to_login);

        last_name = (TextView) v.findViewById(R.id.last_name);
        first_name = (TextView) v.findViewById(R.id.first_name);
        phone = (TextView) v.findViewById(R.id.phone);
        address = (TextView) v.findViewById(R.id.address);

        email_sign_in_button = (Button) v.findViewById(R.id.email_sign_in_button);

        to_reset_pass.setOnClickListener(fragListen);
        to_login.setOnClickListener(fragListen);

        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFields();
            }
        });

        return v;
    }

    private void checkFields() {
        last_name.setError(null);
        first_name.setError(null);
        phone.setError(null);
        address.setError(null);

        last_name_text = last_name.getText().toString();
        first_name_text = first_name.getText().toString();
        phone_text = phone.getText().toString();
        address_text = address.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(last_name_text)) {
            last_name.setError("Это поле обязательно");
            focusView = last_name;
            cancel = true;
        } else if (TextUtils.isEmpty(first_name_text)) {
            first_name.setError("Это поле обязательно");
            focusView = first_name;
            cancel = true;
        } else if (TextUtils.isEmpty(phone_text)) {
            phone.setError("Это поле обязательно");
            focusView = phone;
            cancel = true;
        } else if (TextUtils.isEmpty(address_text)) {
            address.setError("Это поле обязательно");
            focusView = address;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            signUp();
        }
    }

    private void signUp() {
        UserRegisterModel model = new UserRegisterModel();
        UserRegisterModel.Details details = new UserRegisterModel().new Details();

        details.setLast_name(last_name_text.trim());
        details.setFirst_name(first_name_text.trim());
        details.setPhone(phone_text.trim());
        details.setAddress(address_text.trim());

        model.setUserRegisterForm(details);

        APi.Auth apiProfile = APi.getRetrofitBuild().create(APi.Auth.class);
        Call<RestResponse<UserRegisterModel>> call = apiProfile.signUp(model);
        call.enqueue(new Callback<RestResponse<UserRegisterModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<UserRegisterModel>> call, @NonNull Response<RestResponse<UserRegisterModel>> response) {
                Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
                Toast.makeText(getContext(), "Заявка на регистрацию принята", Toast.LENGTH_LONG).show();
                SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext()).getApplicationContext());
                SharedPreferences.Editor e = getPrefs.edit();
                e.putBoolean("APP_SIGN_UP", true);
                e.apply();
                Objects.requireNonNull(getFragmentManager()).popBackStack();
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<UserRegisterModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                Toast.makeText(getContext(), "Заявка на регистрацию принята", Toast.LENGTH_LONG).show();
            }
        });

    }


    View.OnClickListener fragListen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switchFragment((String) v.getTag());
        }
    };

    private void switchFragment(String type) {
        Fragment fragment = null;
        String fragment_tag = "";
        if (type.equals("SignUp")) {
            fragment = new SignUpFragment();
            fragment_tag = "SignUp";
        } else if (type.equals("ResetPass")) {
            fragment = new ResetPassFragment();
            fragment_tag = "ResetPass";
        } else if (type.equals("Login")) {
            fragment = new LoginFragment();
            fragment_tag = "Login";
        }

        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainFrame, fragment, fragment_tag)
                .commit();
    }
}
