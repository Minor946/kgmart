package ru.kgmart.produce.Fragments;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.BuildConfig;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.MainActivity;
import ru.kgmart.produce.Models.DashboardModel;
import ru.kgmart.produce.Models.GraphicModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class MainFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {


    private static final String TAG = "MainFragment";

    private TextView value_accept_balance;
    private TextView label_wait_balance;

    private TextView label_count_goods;
    private TextView count_consignment;
    private TextView count_no_availability;
    private TextView count_in_stock;

    private TextView count_sold_goods;
    private TextView count_sold_currency;

    private TextView count_not_sold_goods;
    private TextView count_not_sold_currency;

    private TextView count_dream_sold_goods;
    private TextView count_dream_sold_currency;

    private TextView count_defect;
    private TextView count_no_carry;
    private TextView count_not_available;

    LineChartView chart;
    ScrollView scrollView;
    ProgressBar progress_efficiency;
    TextView percent_efficiency;
    private int oldScrollYPostion = 0;
    FloatingActionButton fab;
    SwipeRefreshLayout swipe_container;
    Call<RestResponse<DashboardModel>> call;
    APi.ApiProfile apiProfile;

    MaterialDialog dialogAlert;
    boolean dialogIsShow;
    SharedPreferences getPrefs;

    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.content_main, container, false);

        Crashlytics.setString("fragment", "MainFragment");

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.app_name));
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setDisplayHomeAsUpEnabled(false);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setDisplayShowHomeEnabled(false);

        scrollView = (ScrollView) v.findViewById(R.id.sv_content_main);
        progress_efficiency = (ProgressBar) v.findViewById(R.id.progress_efficiency);
        swipe_container = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

        value_accept_balance = (TextView) v.findViewById(R.id.value_accept_balance);
        label_wait_balance = (TextView) v.findViewById(R.id.value_wait_balance);
        label_count_goods = (TextView) v.findViewById(R.id.label_count_goods);
        count_consignment = (TextView) v.findViewById(R.id.count_consignment);
        count_no_availability = (TextView) v.findViewById(R.id.count_no_availability);
        count_in_stock = (TextView) v.findViewById(R.id.count_in_stock);
        count_sold_goods = (TextView) v.findViewById(R.id.count_sold_goods);
        count_sold_currency = (TextView) v.findViewById(R.id.count_sold_currency);
        count_not_sold_goods = (TextView) v.findViewById(R.id.count_not_sold_goods);
        count_not_sold_currency = (TextView) v.findViewById(R.id.count_not_sold_currency);

        count_dream_sold_goods = (TextView) v.findViewById(R.id.count_dream_sold_goods);
        count_dream_sold_currency = (TextView) v.findViewById(R.id.count_dream_sold_currency);
        count_defect = (TextView) v.findViewById(R.id.count_defect);
        count_no_carry = (TextView) v.findViewById(R.id.count_no_carry);
        count_not_available = (TextView) v.findViewById(R.id.count_not_available);
        percent_efficiency = (TextView) v.findViewById(R.id.percent_efficiency);

        chart = (LineChartView) v.findViewById(R.id.chart_efficiency);
        chart.setInteractive(true);
        chart.setZoomEnabled(false);
        chart.setPadding(5, 25, 25, 15);

        fab = ((MainActivity) this.getActivity()).getFab();

        getPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());

        initDashboard();
        return v;
    }

    private void initDashboard() {
        try{
            apiProfile = APi.getRetrofitBuild().create(APi.ApiProfile.class);
            call = apiProfile.getDashboard(CommonComponents.getUserKey(getContext()));
            call.enqueue(new Callback<RestResponse<DashboardModel>>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<RestResponse<DashboardModel>> call, @NonNull Response<RestResponse<DashboardModel>> response) {
                    if (response.isSuccessful()) {
                        RestResponse<DashboardModel> dashboardModel = response.body();
                        RestResponse.Meta meta = Objects.requireNonNull(dashboardModel).getMeta();
                        if (meta.getStatus().equals(RestResponse.Status.SUCCESS) && dashboardModel != null) {
                            if (dashboardModel.getData().getAccessBalance() != 0) {
                                value_accept_balance.setText(String.valueOf(dashboardModel.getData().getAccessBalance()) + " сом");
                            } else {
                                value_accept_balance.setText("--");
                            }
                            if (dashboardModel.getData().getWaitBalance() != 0) {
                                label_wait_balance.setText(String.valueOf(dashboardModel.getData().getWaitBalance()) + " сом");
                            } else {
                                label_wait_balance.setText("--");
                            }

                            label_count_goods.setText(String.valueOf(dashboardModel.getData().getAllGoods()));
                            count_consignment.setText(String.valueOf(dashboardModel.getData().getOnConsigment()));
                            count_no_availability.setText(String.valueOf(dashboardModel.getData().getOnNotAvailable()));
                            count_in_stock.setText(String.valueOf(dashboardModel.getData().getOnAvailable()));

                            NumberFormat formatter = NumberFormat.getNumberInstance();

                            count_sold_goods.setText(String.valueOf(dashboardModel.getData().getCountSold()));
                            count_sold_currency.setText(String.valueOf(formatter.format(dashboardModel.getData().getSummarySold())));

                            count_dream_sold_goods.setText(String.valueOf(dashboardModel.getData().getCountNotSold()));
                            count_dream_sold_currency.setText(String.valueOf(formatter.format(dashboardModel.getData().getSummaryNotSold())));

                            count_not_sold_goods.setText(String.valueOf(dashboardModel.getData().getCountNotSold() - dashboardModel.getData().getCountSold()));
                            count_not_sold_currency.setText(String.valueOf(formatter.format(dashboardModel.getData().getSummaryNotSold() - dashboardModel.getData().getSummarySold())));

                            count_defect.setText(String.valueOf(dashboardModel.getData().getCountDefects()));
                            count_no_carry.setText(String.valueOf(dashboardModel.getData().getCountNoCarry()));
                            count_not_available.setText(String.valueOf(dashboardModel.getData().getCountNoAvailable()));
                            colorProgressBar(dashboardModel.getData().getReceptionRate());
                            progress_efficiency.setProgress(dashboardModel.getData().getReceptionRate());
                            ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progress_efficiency, "progress", 0, dashboardModel.getData().getReceptionRate());
                            progressAnimator.start();
                            percent_efficiency.setText(String.valueOf(dashboardModel.getData().getReceptionRate()) + "%");

                            try {
                                initGraphic(dashboardModel.getData().getGraphics());
                            } catch (Exception e) {
                                Log.e(TAG, "onResponse: charts ", e);
                                Crashlytics.logException(e);
                            }
                        }
                    } else {
                        Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
                    }
                    closeRefresh();
                }

                @Override
                public void onFailure(@NonNull Call<RestResponse<DashboardModel>> call, @NonNull Throwable t) {
                    closeRefresh();
                    Log.e(TAG, "onFailure: ", t);
                }
            });
        }catch (Exception e){
            initDashboard();
        }

    }

    private void closeRefresh() {
        if (swipe_container.isRefreshing()) {
            swipe_container.setRefreshing(false);
        }
    }

    private void colorProgressBar(Integer receptionRate) {
        try {
            if (receptionRate <= 20) {
                progress_efficiency.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_danger));
            } else if (receptionRate < 70) {
                progress_efficiency.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar_warning));
            } else {
                progress_efficiency.setProgressDrawable(getResources().getDrawable(R.drawable.custom_progressbar));
            }
        } catch (Exception e) {
            Crashlytics.logException(e);
        }
    }

    private void initGraphic(List<GraphicModel> graphicsList) {
        if (graphicsList != null && graphicsList.size() > 0) {
            List<PointValue> values_sold = new ArrayList<PointValue>();
            List<PointValue> values_all = new ArrayList<PointValue>();
            List<PointValue> values_not_sold = new ArrayList<PointValue>();
            ArrayList<AxisValue> labelAxisX = new ArrayList<AxisValue>();
            ArrayList<AxisValue> labelAxisY = new ArrayList<AxisValue>();

            for (int i = 0; i < graphicsList.size(); i++) {
                GraphicModel graphics = graphicsList.get(i);
                try {
                    values_sold.add(new PointValue(i, graphics.getCountSold()));
                    values_all.add(new PointValue(i, (graphics.getCountNotSold())));
                    values_not_sold.add(new PointValue(i, (graphics.getCountNotSold() - graphics.getCountSold())));

                    labelAxisX.add(new AxisValue((float) i).setLabel(CommonComponents.getMount(getContext(), graphics.getMonth())));

                    labelAxisY.add(new AxisValue((float) (graphics.getCountSold())).setLabel(String.valueOf((graphics.getCountSold()))));

                    labelAxisY.add(new AxisValue((float) (graphics.getCountNotSold())).setLabel(String.valueOf((graphics.getCountNotSold()))));
                } catch (Exception e) {
                    Log.e(TAG, "initGraphic: ", e);
                }
            }

            Line line = new Line(values_sold).setCubic(true);
            line.setStrokeWidth(1).setHasPoints(true);
            line.setColor(Color.parseColor("#FF43A047"));//green
            line.setFilled(true);
            line.setAreaTransparency(60);

            line.setHasLabels(false);
            line.setHasLabelsOnlyForSelected(true);

            Line line2 = new Line(values_not_sold).setCubic(true);
            line2.setColor(Color.parseColor("#757575"));//gray
            line2.setStrokeWidth(1).setHasPoints(false);
            line2.setHasLabels(false);
            line2.setHasLabelsOnlyForSelected(true);

            Line line3 = new Line(values_all).setCubic(true);
            line3.setColor(Color.parseColor("#1e88e5"));//blue
            line3.setStrokeWidth(1).setHasPoints(true);
            line3.setHasLabels(false);
            line3.setHasLabelsOnlyForSelected(true);

            List<Line> lines = new ArrayList<Line>();
            lines.add(line);
            lines.add(line2);
            lines.add(line3);

            LineChartData data = new LineChartData();
            data.setLines(lines);
            data.setAxisXBottom(new Axis(labelAxisX));
            data.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(false));

            chart.setZoomEnabled(false);
            try {
                chart.setLineChartData(data);
            } catch (OutOfMemoryError e) {
                Log.e(TAG, "initGraphic", e);
                Crashlytics.logException(e);
            }
        }
    }

    @Override
    public void onResume() {
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.app_name));
        fab.show();
        super.onResume();
    }

    @Override
    public void onRefresh() {
        initDashboard();
    }
}
