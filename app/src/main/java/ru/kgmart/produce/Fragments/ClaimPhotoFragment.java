package ru.kgmart.produce.Fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.datetimepicker.date.DatePickerDialog;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.PhotoClaimModel;
import ru.kgmart.produce.Models.PhotoClaimsModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;
import ru.kgmart.produce.SchedulePhotoActivity;

/**
 * Created on 5/4/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class ClaimPhotoFragment extends Fragment {

    private static final String TAG = "ClaimPhotoFragment";

    private static final int REQUEST_CODE = 1;

    String[] data = {"1", "5", "10", "20", "50", "100", "200"};

    private TextView btn_in_studio;
    private TextView btn_in_place;
    private TextView claim_date;
    private TextView claim_count_model;
    private TextView claim_desc;

    MaterialDialog dialog;

    private Button btn_create_claim;

    Spinner count_spinner_select;

    int count_model = 1;

    private ImageView btn_add_count_model;
    private ImageView btn_remove_count_model;

    int place = 0;

    Boolean dateChange = false;
    String date;
    String dateShort;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_photo_schedule_form, container, false);

        btn_in_place = (TextView) v.findViewById(R.id.btn_in_place);
        btn_in_studio = (TextView) v.findViewById(R.id.btn_in_studio);
        claim_date = (TextView) v.findViewById(R.id.claim_date);
        claim_count_model = (TextView) v.findViewById(R.id.claim_count_model);
        claim_desc = (TextView) v.findViewById(R.id.claim_desc);

        claim_count_model = (TextView) v.findViewById(R.id.claim_count_model);

        claim_count_model.setText(String.valueOf(count_model));

        btn_add_count_model = (ImageView) v.findViewById(R.id.btn_add_count_model);
        btn_remove_count_model = (ImageView) v.findViewById(R.id.btn_remove_count_model);

        btn_create_claim = (Button) v.findViewById(R.id.btn_create_claim);

        try {
            FloatingActionButton fab2 = ((SchedulePhotoActivity) Objects.requireNonNull(this.getActivity())).getFab();
            fab2.hide();
        } catch (Exception ignored) {
        }


        btn_in_place.setOnClickListener(onClickPlace);
        btn_in_studio.setOnClickListener(onClickPlace);

        btn_add_count_model.setOnClickListener(onClickCountModel);
        btn_remove_count_model.setOnClickListener(onClickCountModel);

        dialog = DialogManager.newDialogLoading(getContext());

        claim_count_model.setOnClickListener(onClickValueModel);

        count_spinner_select = (Spinner) v.findViewById(R.id.count_spinner_select);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        count_spinner_select.setAdapter(adapter);
        count_spinner_select.setPrompt(getActivity().getResources().getString(R.string.label_count_model));
        count_spinner_select.setSelection(0);

        count_spinner_select.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                claim_count_model.setText(parent.getItemAtPosition(position).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "onNothingSelected: ");
            }
        });

        claim_date.setFocusable(false);
        claim_date.setClickable(true);

        Calendar calendar = Calendar.getInstance();

        claim_date.setText(CommonComponents.getPublicDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), "."));

        claim_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(date)) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        calendar.setTime(sdf.parse(String.valueOf(date)));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                date = "";
                DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                 @Override
                                                 public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                                                     dateChange = true;
                                                     date = year + "-";
                                                     if ((monthOfYear + 1) < 10) {
                                                         date += "0" + (monthOfYear + 1);
                                                     } else {
                                                         date += (monthOfYear + 1);
                                                     }
                                                     date += "-";
                                                     if (dayOfMonth < 10) {
                                                         date += "0" + dayOfMonth;

                                                     } else {
                                                         date += dayOfMonth;
                                                     }
                                                     claim_date.setText(CommonComponents.getPublicDate(year, monthOfYear, dayOfMonth, "."));
                                                 }
                                             }, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show(Objects.requireNonNull(getActivity()).getFragmentManager(), "datePicker");
            }
        });

        btn_create_claim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                claim_desc.clearFocus();
                btn_create_claim.setClickable(false);
                if (!TextUtils.isEmpty(claim_desc.getText().toString())) {
                    APi.ApiPhotoSession apiPhotoSession = APi.getRetrofitBuild().create(APi.ApiPhotoSession.class);
                    PhotoClaimModel photoClaimModel = new PhotoClaimModel();

                    PhotoClaimModel.Details details = new PhotoClaimModel().new Details();

                    details.setQuantity(claim_count_model.getText().toString());
                    details.setDate(claim_date.getText().toString());
                    details.setPlace(String.valueOf(place));
                    details.setNoteShop(claim_desc.getText().toString());
                    details.setNoteAdmin(null);

                    photoClaimModel.setNewClaimDetail(details);

                    Call<RestResponse<PhotoClaimsModel>> call = apiPhotoSession.addPhotoSession(photoClaimModel, CommonComponents.getUserKey(getContext()));
                    call.enqueue(new Callback<RestResponse<PhotoClaimsModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<RestResponse<PhotoClaimsModel>> call, @NonNull Response<RestResponse<PhotoClaimsModel>> response) {
                            dialog.dismiss();
                            if (response.isSuccessful()) {
                                Log.d(TAG, "onResponse: success");
                                Toast.makeText(getContext(), "Заявка создана", Toast.LENGTH_LONG).show();

                                Intent scheduleActivity = new Intent(getActivity(), SchedulePhotoActivity.class);
                                scheduleActivity.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getActivity().startActivity(scheduleActivity);
                            } else {
                                try {
                                    JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                                    String msgError = jObjError.getJSONObject("meta").getString("message");
                                    if (msgError != null) {
                                        Toast.makeText(getContext(), Html.fromHtml(msgError), Toast.LENGTH_LONG).show();
                                    }
                                } catch (Exception e) {
                                    Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                                }
                                btn_create_claim.setClickable(true);
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<RestResponse<PhotoClaimsModel>> call, @NonNull Throwable t) {
                            Log.e(TAG, "onFailure: ", t);
                            dialog.dismiss();
                            btn_create_claim.setClickable(true);
                            getActivity().getSupportFragmentManager().popBackStack();
                        }
                    });
                } else {
                    dialog.dismiss();
                    btn_create_claim.setClickable(true);
                    claim_desc.setError("Примечание не должно быть пустым");
                }
            }
        });

        return v;
    }

    View.OnClickListener onClickPlace = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v.getTag().equals("0")) {
                btn_in_studio.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.photo_label_backgound_otline));
                btn_in_place.setBackgroundColor(Objects.requireNonNull(getActivity()).getResources().getColor(R.color.colorAccent));
                place = 0;
            }
            if (v.getTag().equals("1")) {
                btn_in_place.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.photo_label_backgound_otline));
                btn_in_studio.setBackgroundColor(Objects.requireNonNull(getActivity()).getResources().getColor(R.color.colorAccent));
                place = 1;
            }
        }
    };

    View.OnClickListener onClickCountModel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            count_model = Integer.valueOf(claim_count_model.getText().toString());

            if (count_model > 1) {
                if (v.getTag().equals("remove")) {
                    count_model = count_model - 1;
                    claim_count_model.setText(String.valueOf(count_model));
                    return;
                }
            }

            if (count_model <= 1000) {
                if (v.getTag().equals("add")) {
                    count_model = count_model + 1;
                    claim_count_model.setText(String.valueOf(count_model));
                    return;
                }
            }
        }
    };

    View.OnClickListener onClickValueModel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            count_spinner_select.performClick();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Записаться на фотосессию");
    }


    @Override
    public void onPause() {

        super.onPause();
    }
}
