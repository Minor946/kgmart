package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;
import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.Event.CustomCalendarEvent;
import ru.kgmart.produce.Components.Event.EventItemRenderer;
import ru.kgmart.produce.Models.PhotoClaimsModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;
import ru.kgmart.produce.SchedulePhotoActivity;

/**
 * Created on 4/26/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class SchedulePhotoFragment extends Fragment {

    private static final String TAG = "SchedulePhotoFragment";
    AgendaCalendarView mAgendaCalendarView;
    FloatingActionButton fab;
    CustomCalendarEvent event;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_photo_schedule_v2, container, false);

        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        minDate.add(Calendar.MONTH, -2);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.YEAR, 2);

        try {
            fab = ((SchedulePhotoActivity) Objects.requireNonNull(this.getActivity())).getFab();
            fab.hide();
        } catch (Exception ignored) {
        }

        mAgendaCalendarView = (AgendaCalendarView) v.findViewById(R.id.agenda_calendar_view);

        List<CalendarEvent> eventList = new ArrayList<>();

        mAgendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), calendarPickerController);

        APi.ApiPhotoSession apiPhotoSession = APi.getRetrofitBuild().create(APi.ApiPhotoSession.class);
        Call<RestResponse<PhotoClaimsModel>> call = apiPhotoSession.getPhotoSession(CommonComponents.getUserKey(getContext()));

        call.enqueue(new Callback<RestResponse<PhotoClaimsModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<PhotoClaimsModel>> call, @NonNull Response<RestResponse<PhotoClaimsModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<PhotoClaimsModel> scheduleModelRestResponse = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(scheduleModelRestResponse).getMeta();
                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        Log.d(TAG, "onResponse: getBookkeeping" + String.valueOf(scheduleModelRestResponse.getData().getBookkeeping().size()));
                        if (scheduleModelRestResponse.getData().getBookkeeping() != null && scheduleModelRestResponse.getData().getBookkeeping().size() > 0) {
                            List<PhotoClaimsModel.DetailsBookkeeping> modelsBookkeeping = scheduleModelRestResponse.getData().getBookkeeping();
                            Log.d(TAG, "onResponse: " + String.valueOf(modelsBookkeeping.size()));
                            if (modelsBookkeeping.size() > 0) {
                                for (int i = 0; i < modelsBookkeeping.size(); i++) {
                                    PhotoClaimsModel.DetailsBookkeeping bookkeeping = modelsBookkeeping.get(i);
                                    Calendar calendar = Calendar.getInstance();
                                    String date = bookkeeping.getBooking_date();
                                    Log.d(TAG, "onResponse: date " + bookkeeping.getBooking_date());
//                                    if (!TextUtils.isEmpty(bookkeeping.getBooking_date())) {
//                                        date = bookkeeping.getBooking_date();
//                                    }
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
                                    try {
                                        calendar.setTime(sdf.parse(String.valueOf(date)));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    String note = bookkeeping.getAmount();
                                    if (bookkeeping.getComment() != null && !TextUtils.isEmpty(bookkeeping.getComment())) {
                                        note += " " + bookkeeping.getComment();
                                    }
                                    try {
                                        event = new CustomCalendarEvent(
                                                "Бухгалтерия",
                                                note,
                                                "",
                                                ContextCompat.getColor(getContext(), getColorByPlace("Бухгалтерия", "-1")),
                                                calendar, calendar, false, android.R.drawable.ic_dialog_info);
                                    } catch (NullPointerException e) {
                                        Crashlytics.logException(e);

                                    }
                                    if (event != null) {
                                        eventList.add(event);
                                    }
                                }
                            }
                        }
                        Log.d(TAG, "onResponse: getPhotoClaim" + String.valueOf(scheduleModelRestResponse.getData().getPhotoClaim().size()));
                        if (scheduleModelRestResponse.getData().getPhotoClaim() != null && scheduleModelRestResponse.getData().getPhotoClaim().size() > 0) {
                            List<PhotoClaimsModel.Details> models = scheduleModelRestResponse.getData().getPhotoClaim();
                            if (models.size() > 0) {
                                for (int i = 0; i < models.size(); i++) {
                                    PhotoClaimsModel.Details model = models.get(i);
                                    Log.d(TAG, "onResponse: status " + model.getStatus());
                                    Calendar calendar = Calendar.getInstance();
                                    SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
                                    try {
                                        calendar.setTime(sdf.parse(String.valueOf(model.getDate())));
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }
                                    String note = "";
                                    if (model.getNoteAdmin() != null && !TextUtils.isEmpty(model.getNoteAdmin())) {
                                        note = model.getNoteAdmin();
                                    }
                                    try {
                                        event = new CustomCalendarEvent(
                                                getPlaceLabel(model.getPlace()),
                                                note,
                                                getStatusLabel(model.getStatus()),
                                                ContextCompat.getColor(getContext(), getColorByPlace(model.getPlace(), model.getStatus())),
                                                calendar, calendar, false, android.R.drawable.ic_dialog_info);
                                    } catch (NullPointerException e) {
                                        Crashlytics.logException(e);

                                    }
                                    if (event != null) {
                                        eventList.add(event);
                                    }
                                }
                            }
                        }
                        if(eventList != null && eventList.size() > 0){
                            mAgendaCalendarView.init(eventList, minDate, maxDate, Locale.getDefault(), calendarPickerController);
                            mAgendaCalendarView.addEventRenderer(new EventItemRenderer());
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<PhotoClaimsModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

        return v;
    }

    CalendarPickerController calendarPickerController = new CalendarPickerController() {
        @Override
        public void onDaySelected(DayItem dayItem) {

        }

        @Override
        public void onEventSelected(CalendarEvent event) {

        }

        @Override
        public void onScrollToDate(Calendar calendar) {

        }
    };

    private int getColorByPlace(String place, String status) {
        if (status.equals("0") || status.equals("1")) {
            if (place.equals("1")) {
                return R.color.photo_in_studio;
            } else {
                return R.color.photo_in_place;
            }
        } else if (status.equals("-1")) {
            return R.color.success;
        } else {
            return R.color.photo_finished;
        }
    }

    private String getPlaceLabel(String place) {
        if (place.equals("1")) {
            return "Фотосессия в студии";
        } else {
            return "Фотосессия на месте";
        }
    }

    private String getStatusLabel(String place) {
        if (place.equals("0")) {
            return "новая";
        } else if (place.equals("1")) {
            return "запланирована";
        } else {
            return "отклонена";
        }
    }

    private String getTimeBookkeeping(String type) {
        if (type.equals("0")) {
            return " (до обеда)";
        } else {
            return " (после обеда)";
        }
    }

    @Override
    public void onResume() {
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_photo_session));
        super.onResume();
        fab.show();
    }

}
