package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.GoodRecyclerNewAdapter;
import ru.kgmart.produce.Adapters.GoodRecyclerViewAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Listeners.EndlessScrollListener;
import ru.kgmart.produce.Models.ConsigmentModel;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ItemAvailableModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.ReceptionDefectModel;
import ru.kgmart.produce.Models.ReceptionModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.ReturnDefectModel;
import ru.kgmart.produce.Models.StockModel;
import ru.kgmart.produce.Models.TciModel;
import ru.kgmart.produce.Models.WarehouseModel;
import ru.kgmart.produce.R;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "GoodsFragments";

    private static int page = 1;
    RecyclerView recyclerView;
    LinearLayoutManager llm;

    @SuppressLint("UseSparseArrays")
    Map<Integer, GoodsAdapterModel> map = new HashMap<>();
    Map<Integer, GoodsAdapterModel> treeMap = new TreeMap<Integer, GoodsAdapterModel>();
    @SuppressLint("UseSparseArrays")
    Map<Integer, GoodsAdapterModel> mapSubItem = new HashMap<>();
    Map<String, String> mapSales = new HashMap<>();
    ArrayList<String> mapSalesId = new ArrayList<>();

    public static final int TYPE_ALL_UPDATE = -2;
    public static final int TYPE_ALL = -1;
    public static final int TYPE_AVAILABLE = 0;
    public static final int TYPE_CONSIGN = 1;
    public static final int TYPE_TO_CARRY = 2;
    public static final int TYPE_DISCOUNT = 3;
    public static final int TYPE_DEFECT = 4;
    public static final int TYPE_STOCK_SALES = 5;
    public static final int TYPE_SALES = 6;

    public static final int TYPE_DISCOUNT_FORM = -10;


    public static final int SUB_TYPE_ALL = -1;
    public static final int SUB_TYPE_AVAILABLE = 0;
    public static final int SUB_TYPE_NO_AVAILABLE = 2;
    public static final int SUB_TYPE_ON_CONSIGN = 3;


    public static final int SUB_TYPE_DEFECT_RETURN = 4;
    public static final int SUB_TYPE_DEFECT_OTK = 5;

    public static final int SUB_TYPE_SEARCH = 6;


    private static final int limit = 20;
    String visible = null;
    String type = null;
    String sub_type = null;

    SwipeRefreshLayout swipe_container;
    RelativeLayout rl_empty;

    MaterialDialog dialog;
    private static int sizeList = 0;
    int items_size = 0;
    Integer count_all = 0;

    String product_id = null;
    String q = null;

    public static int getCurrentPage() {
        return page;
    }

    public static int getSizeList() {
        return sizeList;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_goods, container, false);

        Crashlytics.setString("fragment", "GoodsFragment");

        recyclerView = (RecyclerView) v.findViewById(R.id.rr_goods);
        swipe_container = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        rl_empty = (RelativeLayout) v.findViewById(R.id.rl_empty);
        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

        dialog = DialogManager.newDialogLoading(getContext());

        Bundle extras = getArguments();
        if (extras != null) {
            type = extras.getString("type");
            sub_type = extras.getString("sub_type");
            product_id = extras.getString("product_id");
            q = extras.getString("search");
            Crashlytics.setString("current_level", type);
            Crashlytics.setString("sub_type", sub_type);
            Crashlytics.setString("product_id", product_id);
            Crashlytics.setString("q", q);
        }
        if (type != null && !type.equals(String.valueOf(TYPE_ALL)) &&
                !type.equals(String.valueOf(TYPE_ALL_UPDATE)) && !type.equals(String.valueOf(TYPE_DEFECT)) &&
                !type.equals(String.valueOf(TYPE_AVAILABLE)) && !type.equals(String.valueOf(TYPE_STOCK_SALES))
                && !type.equals(String.valueOf(TYPE_TO_CARRY)) && !type.equals(String.valueOf(TYPE_SALES))) {
            visible = type;
            llm = new GridLayoutManager(getContext(), 2);
        } else {
            llm = new GridLayoutManager(getContext(), 1);
        }

        initTitle(type);

        recyclerView.setLayoutManager(llm);
        if (type.equals(String.valueOf(TYPE_STOCK_SALES))) {
            GoodRecyclerNewAdapter goodRecyclerViewAdapter = new GoodRecyclerNewAdapter(treeMap, getContext(), type, sub_type, getActivity(), product_id, rl_empty);
            recyclerView.setAdapter(goodRecyclerViewAdapter);
        } else {
            GoodRecyclerViewAdapter goodRecyclerViewAdapter = new GoodRecyclerViewAdapter(treeMap, getContext(), type, sub_type, getActivity(), product_id, rl_empty);
            recyclerView.setAdapter(goodRecyclerViewAdapter);
        }


        if (map.size() == 0) {
            addMoreItem(0);
        }

        recyclerView.addOnScrollListener(new EndlessScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (map.size() >= limit && map.size() < count_all) {
                    addMoreItem(totalItemsCount);
                }
            }
        });

        Objects.requireNonNull(getFragmentManager()).addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                try {
                    Fragment myFragment = (Fragment) getFragmentManager().findFragmentByTag("Goods");
                    if (myFragment != null && myFragment.isVisible()) {
                        // add your code here
                        initTitle(type);
                    }
                } catch (NullPointerException e) {
                    Log.e(TAG, "onBackStackChanged", e);
                    Crashlytics.logException(e);
                }
            }
        });
        return v;
    }

    private void initTitle(String type) {
        if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_check_available));
            Objects.requireNonNull(getFragmentManager()).addOnBackStackChangedListener(
                    new FragmentManager.OnBackStackChangedListener() {
                        @Override
                        public void onBackStackChanged() {
                            addMoreItem(0);
                        }
                    });
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_carry_to_consignment));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_need_to_carry));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_ALL_UPDATE))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_goods_on_upgrade));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_discounts));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_defects));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_trades));
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_sales));
        } else if (product_id != null) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_on_stock));
        } else {
            if (!TextUtils.isEmpty(sub_type) && sub_type.equals(String.valueOf(SUB_TYPE_SEARCH))) {
                Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Поиск");
            }
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_goods));
        }
    }


    public void addMoreItem(int offset) {
        Call<RestResponse<GoodsModel>> call;
        dialog.show();
        if (offset == 0) {
            page = 1;
        }
        if (offset > 0) {
            page = offset / limit + 1;
        }
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
            call = apiGoods.getGoodsToAvailable(CommonComponents.getUserKey(getContext()), limit, page);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
            call = apiGoods.getGoodsConsign(CommonComponents.getUserKey(getContext()), limit, page);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
            call = apiGoods.getGoodsToCarry(CommonComponents.getUserKey(getContext()), limit, page);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
            call = apiGoods.getGoodsSale(CommonComponents.getUserKey(getContext()), limit, page);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
            if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK))) {
                call = apiGoods.getDefectGoodsReception(CommonComponents.getUserKey(getContext()), limit, page);
            } else {
                call = apiGoods.getDefectGoodsReturn(CommonComponents.getUserKey(getContext()), limit, page);
            }
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DISCOUNT))) {
            call = apiGoods.getSellOut(CommonComponents.getUserKey(getContext()), null, limit, page, 1);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES))) {
            call = apiGoods.getGoodsStock(CommonComponents.getUserKey(getContext()), limit, page);
        } else {
            if (sub_type.equals(String.valueOf(SUB_TYPE_ALL))) {
                call = apiGoods.getGoods(CommonComponents.getUserKey(getContext()), null, limit, page, null);
            } else if (sub_type.equals(String.valueOf(SUB_TYPE_SEARCH))) {
                call = apiGoods.getGoods(CommonComponents.getUserKey(getContext()), null, limit, page, q);
            } else {
                if (product_id != null && !TextUtils.isEmpty(product_id)) {
                    call = apiGoods.getGoodInfo(CommonComponents.getUserKey(getContext()), product_id);
                } else if (sub_type.equals(String.valueOf(SUB_TYPE_ON_CONSIGN))) {
                    call = apiGoods.getAllGoodsConsign(CommonComponents.getUserKey(getContext()), limit, page);
                } else {
                    call = apiGoods.getGoods(CommonComponents.getUserKey(getContext()), sub_type, limit, page, null);
                }
            }
        }
        int finalPage = page;
        try {
            call.enqueue(new Callback<RestResponse<GoodsModel>>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                    if (response.isSuccessful()) {
                        RestResponse<GoodsModel> goodsModel = response.body();
                        RestResponse.Meta meta = Objects.requireNonNull(goodsModel).getMeta();
                        if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                            if (goodsModel.getData() != null && goodsModel.getData().getProducts() != null) {
                                rl_empty.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                count_all = goodsModel.getData().getCount();
                                if (finalPage == 1) {
                                    map.clear();
                                    mapSubItem.clear();
                                    items_size = 0;
                                }
                                try {
                                    final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) getActivity()
                                            .findViewById(android.R.id.content)).getChildAt(0);
                                    TabLayout tabLayout = (TabLayout)  viewGroup.findViewById(R.id.tab_layout);
                                    if (type.equals(String.valueOf(TYPE_ALL))) {
                                        if (sub_type.equals(String.valueOf(SUB_TYPE_AVAILABLE)))
                                            tabLayout.getTabAt(0).setText("В наличии (" + goodsModel.getData().getCount() + ")");
                                        if (sub_type.equals(String.valueOf(SUB_TYPE_NO_AVAILABLE)))
                                            tabLayout.getTabAt(2).setText("Нет в наличии(" + goodsModel.getData().getCount() + ")");
                                    }
                                    if (type.equals(String.valueOf(TYPE_STOCK_SALES)))
                                        tabLayout.getTabAt(1).setText("Склад (" + goodsModel.getData().getCount() + ")");
                                    for (final ProductsModels productsModel : goodsModel.getData().getProducts()) {
                                        if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
                                            if (productsModel.getWarehouse() != null) {
                                                List<WarehouseModel> list = productsModel.getWarehouse();
                                                for (int j = 0; j < list.size(); j++) {
                                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                                    model.setProductsModels(productsModel);
                                                    model.setWarehouseModel(list.get(j));
                                                    mapSubItem.put(items_size, model);
                                                    items_size++;
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
                                            if (productsModel.getItem() != null) {
                                                List<ItemAvailableModel> list = productsModel.getItem();
                                                for (int j = 0; j < list.size(); j++) {
                                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                                    model.setProductsModels(productsModel);
                                                    model.setItemAvailableModel(list.get(j));
                                                    mapSubItem.put(items_size, model);
                                                    items_size++;
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_SALES))) {
                                            if (productsModel.getTci() != null) {
                                                List<TciModel> list = productsModel.getTci();
                                                for (int j = 0; j < list.size(); j++) {
                                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                                    model.setProductsModels(productsModel);
                                                    model.setTciModel(list.get(j));
                                                    mapSubItem.put(items_size, model);
                                                    items_size++;
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
                                            if (productsModel.getReception() != null) {
                                                List<ReceptionModel> list = productsModel.getReception();
                                                for (int j = 0; j < list.size(); j++) {
                                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                                    model.setProductsModels(productsModel);
                                                    model.setReceptionModel(list.get(j));
                                                    mapSubItem.put(items_size, model);
                                                    items_size++;
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_ALL)) &&
                                                sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_ON_CONSIGN))) {
                                            if (productsModel.getConsigment() != null) {
                                                List<ConsigmentModel> list = productsModel.getConsigment();
                                                for (int j = 0; j < list.size(); j++) {
                                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                                    model.setProductsModels(productsModel);
                                                    model.setConsigmentModel(list.get(j));
                                                    mapSubItem.put(items_size, model);
                                                    items_size++;
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_DEFECT))) {
                                            if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_DEFECT_OTK))) {
                                                if (productsModel.getReceptiondefect() != null) {
                                                    List<ReceptionDefectModel> list = productsModel.getReceptiondefect();
                                                    for (int j = 0; j < list.size(); j++) {
                                                        GoodsAdapterModel model = new GoodsAdapterModel();
                                                        model.setProductsModels(productsModel);
                                                        model.setReceptionDefectModel(list.get(j));
                                                        mapSubItem.put(items_size, model);
                                                        items_size++;
                                                    }
                                                }
                                            }
                                            if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_DEFECT_RETURN))) {
                                                if (productsModel.getItemreturn() != null) {
                                                    List<ReturnDefectModel> list = productsModel.getItemreturn();
                                                    for (int j = 0; j < list.size(); j++) {
                                                        GoodsAdapterModel model = new GoodsAdapterModel();
                                                        model.setProductsModels(productsModel);
                                                        model.setReturnDefectModel(list.get(j));
                                                        mapSubItem.put(items_size, model);
                                                        items_size++;
                                                    }
                                                }
                                            }
                                        } else if (type.equals(String.valueOf(GoodEnum.TYPE_STOCK_SALES))) {
                                            if (productsModel.getStock() != null) {
                                                if (mapSalesId.contains(productsModel.getId())) {
                                                    int countItemsSale = Integer.valueOf(mapSales.get(productsModel.getId())) + 1;
                                                    mapSales.put(productsModel.getId(), String.valueOf(countItemsSale));
                                                } else {
                                                    mapSalesId.add(productsModel.getId());
                                                    mapSales.put(productsModel.getId(), "0");
                                                }
                                                StockModel list = productsModel.getStock();
                                                GoodsAdapterModel model = new GoodsAdapterModel();
                                                model.setProductsModels(productsModel);
                                                model.setStockModel(list);
                                                mapSubItem.put(items_size, model);
                                                items_size++;
                                            }
                                        } else {
                                            GoodsAdapterModel model = new GoodsAdapterModel();
                                            model.setProductsModels(productsModel);
                                            mapSubItem.put(items_size, model);
                                            items_size++;
                                        }

                                        if (mapSubItem != null && mapSubItem.size() > 0) {
                                            map.putAll(mapSubItem);
                                            mapSubItem.clear();
                                        }
                                    }
                                } catch (IndexOutOfBoundsException e) {
                                    Crashlytics.logException(e);
                                    addMoreItem(0);
                                }
                                sizeList = map.size();
                                treeMap.putAll(map);
                                recyclerView.getAdapter().notifyDataSetChanged();
                            }
                        }
                    } else {
                        rl_empty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        map.clear();
                        mapSubItem.clear();
                        items_size = 0;
                    }
                    closeRefresh();
                }

                @Override
                public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    closeRefresh();
                    if (map.size() == 0) {
                        rl_empty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                    map.clear();
                    mapSubItem.clear();
                    items_size = 0;
                }
            });
        } catch (Exception ignored) {
        }

    }

    @Override
    public void onRefresh() {
        addMoreItem(0);
    }

    private void closeRefresh() {
        try {
            dialog.dismiss();
        } catch (IllegalArgumentException e) {
            Crashlytics.logException(e);
        }
        if (swipe_container.isRefreshing()) {
            swipe_container.setRefreshing(false);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("type", type);
        outState.putString("sub_type", sub_type);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        initTitle(type);
        Log.d(TAG, "onResume:");
        super.onResume();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onStop: ");
        super.onDetach();
    }
}
