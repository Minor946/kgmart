package ru.kgmart.produce.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatDelegate;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.FaqExpandableListAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Models.FaqListModel;
import ru.kgmart.produce.Models.FaqModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class FaqFragment extends Fragment {

    private static final String TAG = "FaqFragment";
    ExpandableListView expandableListView;
    List<FaqModel> faqList = new ArrayList<FaqModel>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        final View v = inflater.inflate(R.layout.fragment_faq_list, container, false);

        expandableListView = (ExpandableListView) v.findViewById(R.id.rv_faq_list);

        APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);

        Call<RestResponse<FaqListModel>> call = apiOther.getFaq(CommonComponents.getUserKey(getContext()));

        call.enqueue(new Callback<RestResponse<FaqListModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<FaqListModel>> call, @NonNull Response<RestResponse<FaqListModel>> response) {
                assert response.body() != null;
                if (Objects.requireNonNull(response.body()).getMeta().getStatus() != null && Objects.requireNonNull(response.body()).getMeta().getStatus().equals(RestResponse.Status.SUCCESS)) {
                    if (Objects.requireNonNull(response.body()).getData().getFaq() != null) {
                        faqList.addAll(Objects.requireNonNull(response.body()).getData().getFaq());
                        if(faqList.size() > 0){
                            expandableListView.setAdapter(new FaqExpandableListAdapter(getContext(), faqList, expandableListView));
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<FaqListModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

        return v;
    }
}
