package ru.kgmart.produce.Fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.MainActivity;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.UserLoginModel;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class LoginFragment extends Fragment {

    private static final String TAG = "LoginFragmentTest";

    private Button email_sign_in_button;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mLoginFormView;
    private String email;
    private String password;
    private RelativeLayout rr_dialog_view;
    MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_login, container, false);

        TextView to_reset_pass = (TextView) v.findViewById(R.id.to_reset_pass);
        TextView to_sign_up = (TextView) v.findViewById(R.id.to_sign_up);

        mLoginFormView = (View) v.findViewById(R.id.login_form);

        email_sign_in_button = (Button) v.findViewById(R.id.email_sign_in_button);
        mEmailView = (AutoCompleteTextView) v.findViewById(R.id.email);
        mPasswordView = (EditText) v.findViewById(R.id.password);
        rr_dialog_view = (RelativeLayout) v.findViewById(R.id.rr_dialog_view);

        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: Success");
                checkForm();
            }
        });

        initData();

        boolean wrapInScrollView = true;
        dialog = DialogManager.newDialogLoading(getContext());

        to_reset_pass.setOnClickListener(fragListen);
        to_sign_up.setOnClickListener(fragListen);
        return v;
    }

    private void initData() {
        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String emailSave = getPrefs.getString("APP_PREFERENCES_LOGIN", null);
        String passwordSave = getPrefs.getString("APP_PREFERENCES_PASSWORD", null);
        if (emailSave != null && passwordSave != null &&
                !TextUtils.isEmpty(emailSave) && !TextUtils.isEmpty(passwordSave)) {
            mEmailView.setText(String.valueOf(emailSave));
            mPasswordView.setText(String.valueOf(passwordSave));
        }
    }


    void startAnim() {
        Log.d(TAG, "startAnim: ");
        dialog.show();
    }

    void stopAnim() {
        Log.d(TAG, "stopAnim: ");
        dialog.dismiss();
    }

    private void checkForm() {
        mEmailView.setError(null);
        mPasswordView.setError(null);

        email = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) || (!TextUtils.isEmpty(password) && !isPasswordValid(password))) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        Log.d(TAG, "checkForm: " + String.valueOf(cancel));

        if (cancel) {
            focusView.requestFocus();
        } else {
            startLogin();
        }
    }

    private void openMain() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 4;
    }

    public void startLogin() {
        startAnim();
        APi.Auth apiAuth = APi.getRetrofitBuild().create(APi.Auth.class);

        Call<RestResponse<UserLoginModel>> authModelCall = apiAuth.login(email.trim(), CommonComponents.md5(password).trim(), "produce_app");
        authModelCall.enqueue(new Callback<RestResponse<UserLoginModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<UserLoginModel>> call, @NonNull Response<RestResponse<UserLoginModel>> response) {

                if (response.isSuccessful()) {
                    RestResponse<UserLoginModel> authData = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(authData).getMeta();

                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext()).getApplicationContext());
                        SharedPreferences.Editor e = getPrefs.edit();
                        UserLoginModel userData = authData.getData();
                        e.putBoolean("APP_PREFERENCES_FIRST_START", false);
                        e.putBoolean("APP_PREFERENCES_LOGIN_STATUS", true);
                        e.putString("APP_PREFERENCES_LOGIN", email);
                        e.putString("APP_PREFERENCES_PASSWORD", password);
                        e.putString("APP_PREFERENCES_ACCESS_TOKEN", authData.getData().getKey());
                        e.putString("APP_PREFERENCES_USERNAME", userData.getUser().getUsername());
                        e.putString("APP_PREFERENCES_BALANCE", String.valueOf(userData.getUser().getBalance()));
                        e.putString("APP_PREFERENCES_ID", userData.getUser().getId());
                        e.putString("APP_PREFERENCES_EMAIL", userData.getUser().getEmail());
                        e.apply();
                        openMain();
                    }
                } else {
                    stopAnim();
                    try {
                        JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                        String msgError = jObjError.getJSONObject("meta").getString("message");
                        if (msgError != null) {
                            Toast.makeText(getContext(), msgError, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<UserLoginModel>> call, @NonNull Throwable t) {
                stopAnim();
                Log.e(TAG, "onFailure: " + t.getMessage());
                Toast.makeText(getContext(), "Повторите позже", Toast.LENGTH_SHORT).show();
            }
        });
    }

    View.OnClickListener fragListen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switchFragment((String) v.getTag());
        }
    };

    private void switchFragment(String type) {
        Fragment fragment = null;
        String fragment_tag = "";
        if (type.equals("SignUp")) {
            fragment = new SignUpFragment();
            fragment_tag = "SignUp";
        } else if (type.equals("ResetPass")) {
            fragment = new ResetPassFragment();
            fragment_tag = "ResetPass";
        } else if (type.equals("Login")) {
            fragment = new LoginFragment();
            fragment_tag = "Login";
        }

        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainFrame, fragment, fragment_tag)
                .addToBackStack("Login")
                .commit();
    }

    @Override
    public void onDestroy() {
        stopAnim();
        super.onDestroy();
    }
}
