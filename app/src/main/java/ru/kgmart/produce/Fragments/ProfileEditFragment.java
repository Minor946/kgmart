package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.datetimepicker.date.DatePickerDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.ProfileModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class ProfileEditFragment extends Fragment {

    private static final String TAG = "ProfileEditFragment";
    String jsonMyObject = null;
    TextView username;
    TextView birthday;
    TextView email;
    TextView phone;
    TextView phone_more;
    TextView address;
    Boolean dateChange = false;
    String date;
    String dateShort;

    Button update_profile_button;

    ProfileModel model;
    MaterialDialog dialog;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_user_profile_edit, container, false);

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.btn_edit_profile));

        username = (TextView) v.findViewById(R.id.username);
        birthday = (TextView) v.findViewById(R.id.birthday);

        birthday.setFocusable(false);
        birthday.setClickable(true);

        email = (TextView) v.findViewById(R.id.email);
        email.setClickable(false);
        email.setEnabled(false);

        phone = (TextView) v.findViewById(R.id.phone);
        phone_more = (TextView) v.findViewById(R.id.phone_more);
        address = (TextView) v.findViewById(R.id.address);

        update_profile_button = (Button) v.findViewById(R.id.update_profile_button);

        dialog = DialogManager.newDialogLoading(getContext());

        Bundle extras = getArguments();
        if (extras != null) {
            jsonMyObject = extras.getString("model");
        }
        if (jsonMyObject != null) {
            model = new Gson().fromJson(jsonMyObject, ProfileModel.class);
            if (model != null) {
                username.setText(model.getProfile().getFirstname());
                if (model.getProfile().getDob() != null && !TextUtils.isEmpty(model.getProfile().getDob())) {
                    Calendar calendar = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    try {
                        calendar.setTime(sdf.parse(String.valueOf(model.getProfile().getDob())));
                        birthday.setText(CommonComponents.getPublicDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), "."));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
                email.setText(model.getProfile().getEmail());
                phone.setText(model.getProfile().getPhone());
                phone_more.setText(model.getProfile().getOtherContact());
                address.setText(model.getProfile().getAddress());
            }
        }

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                try {
                    if (TextUtils.isEmpty(birthday.getText())) {
                        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 18);
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                        if (!dateChange) {
                            calendar.setTime(sdf.parse(String.valueOf(model.getProfile().getDob())));
                        } else {
                            calendar.setTime(sdf.parse(String.valueOf(date)));
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                date = "";
                DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                 @Override
                                                 public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                                                     dateChange = true;
                                                     date = year + "-";
                                                     if ((monthOfYear + 1) < 10) {
                                                         date += "0" + (monthOfYear + 1);
                                                     } else {
                                                         date += (monthOfYear + 1);
                                                     }
                                                     date += "-";
                                                     if (dayOfMonth < 10) {
                                                         date += "0" + dayOfMonth;

                                                     } else {
                                                         date += dayOfMonth;
                                                     }
                                                     Log.d(TAG, "onDateSet: select date" + date);
                                                     birthday.setText(CommonComponents.getPublicDate(year, monthOfYear, dayOfMonth, "."));
                                                 }
                                             }, calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show(getActivity().getFragmentManager(), "datePicker");
            }
        });

        update_profile_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkUpdateProfileForm();
            }
        });

        return v;
    }

    private void checkUpdateProfileForm() {
        username.setError(null);
        email.setError(null);
        phone.setError(null);
        birthday.setError(null);

        String emailT = email.getText().toString();
        String usernameT = username.getText().toString();
        String phoneT = phone.getText().toString();
        String birthdayT = birthday.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(usernameT) || (!TextUtils.isEmpty(usernameT))) {
            username.setError(getString(R.string.error_invalid_usernme));
            focusView = username;
            cancel = true;
        }

        if (TextUtils.isEmpty(phoneT) || (!TextUtils.isEmpty(phoneT))) {
            phone.setError(getString(R.string.error_invalid_phone));
            focusView = phone;
            cancel = true;
        }

        if (TextUtils.isEmpty(birthdayT) || (!TextUtils.isEmpty(birthdayT))) {
            birthday.setError(getString(R.string.error_invalid_bithday));
            focusView = birthday;
            cancel = true;
        }
        if (cancel) {
            focusView.requestFocus();
        } else {
            startUpdateProfile();
        }
    }

    private void startUpdateProfile() {
        ProfileModel modelUpdate = new ProfileModel();
        modelUpdate.setKey(CommonComponents.getUserKey(getContext()));


        String lastName = String.valueOf(username.getText());
        String emailText = String.valueOf(email.getText());
        String mobile = String.valueOf(phone.getText());
        String mobile_more = String.valueOf(phone_more.getText());
        String addressText = String.valueOf(address.getText());

        model.getProfile().setLastname(lastName != null ? lastName.trim() : "");
        model.getProfile().setEmail(emailText != null ? emailText.trim() : "");
        model.getProfile().setPhone(mobile != null ? mobile.trim() : "");
        model.getProfile().setOtherContact(mobile_more != null ? mobile_more.trim() : "");
        model.getProfile().setAddress(addressText != null ? addressText.trim() : "");

        if (TextUtils.isEmpty(date)) {
            date = model.getProfile().getDob();
        }
        model.getProfile().setDob(date != null ? date.trim() : "");

        modelUpdate.setProfile(model.getProfile());

        APi.ApiProfile apiProfile = APi.getRetrofitBuild().create(APi.ApiProfile.class);
        Call<RestResponse<ProfileModel>> call = apiProfile.updateProfile(modelUpdate, CommonComponents.getUserKey(getContext()));
        call.enqueue(new Callback<RestResponse<ProfileModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<ProfileModel>> call, @NonNull Response<RestResponse<ProfileModel>> response) {

                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getMeta().getStatus().equals(RestResponse.Status.SUCCESS)) {
                        dialog.dismiss();
                        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext()).getApplicationContext());
                        SharedPreferences.Editor e = getPrefs.edit();
                        e.putString("APP_PREFERENCES_EMAIL", Objects.requireNonNull(response.body()).getData().getProfile().getEmail());
                        e.apply();
                        new MaterialDialog.Builder(getContext())
                                .title(getResources().getString(R.string.title_dialog_edit_profile))
                                .content(getResources().getString(R.string.content_dialog_edit_profile))
                                .positiveText(R.string.okay)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        Objects.requireNonNull(getFragmentManager()).popBackStack();
                                    }
                                })
                                .autoDismiss(false)
                                .cancelable(false)
                                .canceledOnTouchOutside(false)
                                .show();
                    }

                } else {
                    dialog.dismiss();
                    try {
                        JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                        String msgError = jObjError.getJSONObject("meta").getString("message");
                        if (msgError != null) {
                            Toast.makeText(getContext(), msgError, Toast.LENGTH_LONG).show();
                            Log.d(TAG, "onResponse: error" + msgError);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                    }
                }
                Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<ProfileModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }
}
