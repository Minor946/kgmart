package ru.kgmart.produce.Fragments;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;

import lecho.lib.hellocharts.view.hack.HackyViewPager;
import ru.kgmart.produce.Adapters.FullScreenImageAdapter;
import ru.kgmart.produce.Components.ZoomOutPageTransformer;
import ru.kgmart.produce.R;

/**
 * Created on 5/2/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsFullscreenImageFragment extends DialogFragment {

    HackyViewPager viewPager;

    private ArrayList<String> images;
    private int defaultPosition = 0;

    TextView count_image;

    public static GoodsFullscreenImageFragment newInstance(ArrayList<String> images, int defaultPosition) {
        if (images == null || images.isEmpty()) return null;
        GoodsFullscreenImageFragment frag = new GoodsFullscreenImageFragment();
        frag.images = images;
        frag.defaultPosition = defaultPosition;
        return frag;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.dialogFullscreen);
        Crashlytics.setString("fragment", "GoodsFullscreenImageFragment");
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            Window window = d.getWindow();
            assert window != null;
            window.setLayout(width, height);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image_full_screen, container, false);

        viewPager = (HackyViewPager) view.findViewById(R.id.pager_image_full_screen);
        count_image = (TextView) view.findViewById(R.id.count_image);

        PagerAdapter mPagerAdapter = new FullScreenImageAdapter(getActivity(), images);
        viewPager.setAdapter(mPagerAdapter);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());

        ImageButton closeButton = (ImageButton) view.findViewById(R.id.ib_close);

        if (defaultPosition > 0 && defaultPosition < images.size()) {
            viewPager.setCurrentItem(defaultPosition);
            updateTitle(defaultPosition);
        } else {
            viewPager.setCurrentItem(0);
            updateTitle(0);
        }

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateTitle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        return view;
    }

    private void updateTitle(int position) {
        try{
            String title = String.valueOf(position + 1) + "/" + String.valueOf(images.size());
            count_image.setText(title);
        } catch (Exception e){}

    }

}
