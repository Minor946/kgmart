package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import lecho.lib.hellocharts.view.hack.HackyViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.ListGoodAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.DepthPageTransformer;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.GoodsActivity;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ItemAvailableModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.ReceptionModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.WarehouseModel;
import ru.kgmart.produce.R;

/**
 * Created on 4/23/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsSwipeFragment extends Fragment {

    private static final String TAG = "GoodsSwipeFragment";

    private ListGoodAdapter adapter;

    @SuppressLint("UseSparseArrays")
    Map<Integer, GoodsAdapterModel> map = new HashMap<>();
    @SuppressLint("UseSparseArrays")
    Map<Integer, GoodsAdapterModel> mapSubItem = new HashMap<>();

    Integer position = null;
    Integer all_size = null;
    int page = 1;

    String type = null;
    String sub_type = null;
    Integer count_all = 0;

    private static final int limit = 100;
    HackyViewPager viewPager;
    int items_size = 0;

    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_swipe_good, container, false);

        Crashlytics.setString("fragment", "GoodsSwipeFragment");

        viewPager = v.findViewById(R.id.rr_good_card);

        Bundle extras = getArguments();
        if (extras != null) {
            position = extras.getInt("position");
            page = extras.getInt("page");
            all_size = extras.getInt("all_size");

            type = extras.getString("type");
            sub_type = extras.getString("sub_type");
        }

        adapter = new ListGoodAdapter(Objects.requireNonNull(getActivity()).getSupportFragmentManager(), map, type);
        viewPager.setAdapter(adapter);
        viewPager.setPageTransformer(true, new DepthPageTransformer());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                updateTitle(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        init(0);
        return v;
    }

    private void updateTitle(int position) {
        try {
            String title = String.valueOf(position + 1) + "/" + String.valueOf(getSizeProducts() + " " + map.get(position).getProductsModels().getName());
            Objects.requireNonNull(((GoodsActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(title);
        } catch (NullPointerException e) {
            Crashlytics.logException(e);
            Objects.requireNonNull(((GoodsActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Продукт");
        }
    }

    private void init(int offset) {
        Call<RestResponse<GoodsModel>> call;
        int limitSet = limit;
        int pageSet = 1;
        if (page > 5) {
            limitSet = (limit / 2) * page;
            pageSet = 1;
        }
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
            call = apiGoods.getGoodsToAvailable(CommonComponents.getUserKey(getContext()), limitSet, pageSet);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
            call = apiGoods.getGoodsConsign(CommonComponents.getUserKey(getContext()), limitSet, pageSet);
        } else if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
            call = apiGoods.getGoodsToCarry(CommonComponents.getUserKey(getContext()), limitSet, pageSet);
        } else {
            if (sub_type.equals(String.valueOf(GoodEnum.SUB_TYPE_ALL))) {
                call = apiGoods.getGoods(CommonComponents.getUserKey(getContext()), null, limitSet, pageSet, null);
            } else {
                call = apiGoods.getGoods(CommonComponents.getUserKey(getContext()), sub_type, limitSet, pageSet, null);
            }
        }

        call.enqueue(new Callback<RestResponse<GoodsModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<GoodsModel> goodsModel = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(goodsModel).getMeta();
                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        count_all = goodsModel.getData().getCount();

                        if (goodsModel.getData() != null && goodsModel.getData().getProducts() != null) {
                            for (final ProductsModels productsModel : goodsModel.getData().getProducts()) {
                                if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN))) {
                                    if (productsModel.getWarehouse() != null) {
                                        List<WarehouseModel> list = productsModel.getWarehouse();
                                        for (int j = 0; j < list.size(); j++) {
                                            GoodsAdapterModel model = new GoodsAdapterModel();
                                            model.setProductsModels(productsModel);
                                            model.setWarehouseModel(list.get(j));
                                            mapSubItem.put(items_size, model);
                                            items_size++;
                                        }
                                    }
                                } else if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE))) {
                                    if (productsModel.getItem() != null) {
                                        List<ItemAvailableModel> list = productsModel.getItem();
                                        for (int j = 0; j < list.size(); j++) {
                                            GoodsAdapterModel model = new GoodsAdapterModel();
                                            model.setProductsModels(productsModel);
                                            model.setItemAvailableModel(list.get(j));
                                            mapSubItem.put(items_size, model);
                                            items_size++;
                                        }
                                    }
                                } else if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY))) {
                                    if (productsModel.getReception() != null) {
                                        List<ReceptionModel> list = productsModel.getReception();
                                        for (int j = 0; j < list.size(); j++) {
                                            GoodsAdapterModel model = new GoodsAdapterModel();
                                            model.setProductsModels(productsModel);
                                            model.setReceptionModel(list.get(j));
                                            mapSubItem.put(items_size, model);
                                            items_size++;
                                        }
                                    }
                                } else {
                                    GoodsAdapterModel model = new GoodsAdapterModel();
                                    model.setProductsModels(productsModel);
                                    mapSubItem.put(items_size, model);
                                    items_size++;
                                }
                                if (mapSubItem != null && mapSubItem.size() > 0) {
                                    map.putAll(mapSubItem);
                                    mapSubItem.clear();
                                }
                            }
                            adapter.notifyDataSetChanged();
                            viewPager.setCurrentItem(position);
                            if (position != null)
                                updateTitle(position);
                        }
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {

            }
        });
    }

    public Integer getSizeProducts() {
        if (map.size() > 0) {
            return map.size();
        }
        return all_size;
    }
}
