package ru.kgmart.produce.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.GoodSelectRecyclerViewAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Listeners.EndlessScrollListener;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created on 5/7/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsSelectFragment extends DialogFragment {


    private static final String TAG = "GoodSelectFragment";
    RecyclerView recyclerView;
    LinearLayoutManager llm;
    GoodSelectRecyclerViewAdapter goodRecyclerViewAdapter;
    ArrayList<ProductsModels> goodsList = new ArrayList<>();
    private int REQUEST_CODE_SELECT = 1;

    private static final int limit = 20;
    private static int page = 1;
    Integer count_all = 0;
    Button btn_select_model;
    RelativeLayout rl_empty;
    RelativeLayout rl_list;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_goods_select, container, false);

        Crashlytics.setString("fragment", "GoodsSelectFragment");

        recyclerView = (RecyclerView) v.findViewById(R.id.rr_goods);
        ;
        btn_select_model = (Button) v.findViewById(R.id.btn_select_model);

        rl_empty = (RelativeLayout) v.findViewById(R.id.rl_empty);
        rl_list = (RelativeLayout) v.findViewById(R.id.rl_list);

        llm = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(llm);
        goodRecyclerViewAdapter = new GoodSelectRecyclerViewAdapter(goodsList, getContext());
        recyclerView.setAdapter(goodRecyclerViewAdapter);


        if (goodsList.size() == 0) {
            addMoreItem(0);
        }

        recyclerView.addOnScrollListener(new EndlessScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (goodsList.size() >= limit && goodsList.size() < count_all) {
                    addMoreItem(totalItemsCount);
                }
            }
        });

        btn_select_model.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                ArrayList<String> selected = goodRecyclerViewAdapter.getItemProductId();

                if (selected != null && selected.size() > 0) {
                    Log.d(TAG, "onClick: size" + String.valueOf(selected.size()));
                    intent.putStringArrayListExtra("select_item", (ArrayList<String>) selected);
                }
                Objects.requireNonNull(getTargetFragment()).onActivityResult(REQUEST_CODE_SELECT, Activity.RESULT_OK, intent);
                Objects.requireNonNull(getFragmentManager()).popBackStack();
            }
        });
        return v;
    }

    private void addMoreItem(int offset) {
        Call<RestResponse<GoodsModel>> call;
        if (offset == 0) {
            page = 1;
        }
        if (offset > 0) {
            page = offset / limit + 1;
        }

        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        call = apiGoods.getSellOut(CommonComponents.getUserKey(getContext()), "0", limit, page, 0);
        call.enqueue(new Callback<RestResponse<GoodsModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                if (response.isSuccessful()) {
                    if (Objects.requireNonNull(response.body()).getData() != null) {
                        rl_list.setVisibility(View.VISIBLE);
                        rl_empty.setVisibility(View.GONE);
                        count_all = Objects.requireNonNull(response.body()).getData().getCount();
                        goodsList.addAll(Objects.requireNonNull(response.body()).getData().getProducts());
                        goodRecyclerViewAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
            }
        });

    }
}
