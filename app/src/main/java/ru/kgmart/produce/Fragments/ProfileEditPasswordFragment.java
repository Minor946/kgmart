package ru.kgmart.produce.Fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.PasswordChangeModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/18/18.
 */
public class ProfileEditPasswordFragment extends Fragment {


    private static final String TAG = "ProfileEditPassword";
    TextView password_old;
    TextView password;
    TextView password_confirm;

    Button action_save_pass;

    String password_old_text;
    String password_text;
    String password_confirm_text;

    MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_user_profile_password, container, false);

        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.title_fragment_change_pass));
        password_old = (TextView) v.findViewById(R.id.password_old);
        password = (TextView) v.findViewById(R.id.password);
        password_confirm = (TextView) v.findViewById(R.id.password_confirm);

        action_save_pass = (Button) v.findViewById(R.id.action_save_pass);

        action_save_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkFields();
            }
        });
        dialog = DialogManager.newDialogLoading(getContext());

        return v;
    }

    private void checkFields() {
        password_old.setError(null);
        password.setError(null);
        password_confirm.setError(null);

        password_old_text = password_old.getText().toString();
        password_text = password.getText().toString();
        password_confirm_text = password_confirm.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password_old_text) || (!TextUtils.isEmpty(password_old_text) && !isPasswordValid(password_old_text))) {
            password_old.setError(getString(R.string.error_invalid_password));
            focusView = password_old;
            cancel = true;
        } else if (TextUtils.isEmpty(password_text) || (!TextUtils.isEmpty(password_text) && !isPasswordValid(password_text))) {
            password.setError(getString(R.string.error_invalid_password));
            focusView = password;
            cancel = true;
        } else if (TextUtils.isEmpty(password_confirm_text) || (!TextUtils.isEmpty(password_confirm_text) && !isPasswordValid(password_confirm_text))) {
            password_confirm.setError(getString(R.string.error_invalid_password));
            focusView = password_confirm;
            cancel = true;
        } else if (!password_old_text.equals(CommonComponents.getUserPassword(getContext()))) {
            password_old.setError(getString(R.string.error_invalid_password_old));
            focusView = password_old;
            cancel = true;
        } else if (password_text.equals(password_old_text)) {
            password.setError(getString(R.string.error_invalid_password_not_be_equals));
            focusView = password;
            cancel = true;
        } else if (!password_text.equals(password_confirm_text)) {
            password.setError(getString(R.string.error_invalid_password_equals));
            password_confirm.setError(getString(R.string.error_invalid_password_equals));
            focusView = password;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            startChangePassword();
        }

    }

    private void startChangePassword() {
        dialog.show();
        Log.d(TAG, "startChangePassword: success");
        String oldPassword = (String) String.valueOf(password_old.getText());
        String newPassword = (String) String.valueOf(password.getText());
        String newPasswordConfirm = (String) String.valueOf(password_confirm.getText());

        PasswordChangeModel passwordChangeModel = new PasswordChangeModel();
        PasswordChangeModel.Details details = new PasswordChangeModel().new Details();

        details.setOldPassword(oldPassword != null ? oldPassword.trim() : "");
        details.setVerifyPassword(newPasswordConfirm.trim());
        details.setPassword(newPassword.trim());

        passwordChangeModel.setNewPasswordDetails(details);

        APi.ApiProfile apiProfile = APi.getRetrofitBuild().create(APi.ApiProfile.class);
        Call<RestResponse<String>> call = apiProfile.changePassword(passwordChangeModel, CommonComponents.getUserKey(getContext()));
        call.enqueue(new Callback<RestResponse<String>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<String>> call, @NonNull Response<RestResponse<String>> response) {
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: " + Objects.requireNonNull(response.body()).getData());
                    if (Objects.requireNonNull(response.body()).getMeta().getStatus().equals(RestResponse.Status.SUCCESS)) {
                        dialog.dismiss();
                        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getContext()).getApplicationContext());
                        SharedPreferences.Editor e = getPrefs.edit();
                        e.putString("APP_PREFERENCES_PASSWORD", newPassword);
                        e.apply();
                        new MaterialDialog.Builder(getContext())
                                .title(Objects.requireNonNull(response.body()).getMeta().getMessage())
                                .positiveText(R.string.okay)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        dialog.dismiss();
                                        Objects.requireNonNull(getFragmentManager()).popBackStack();
                                    }
                                })
                                .autoDismiss(false)
                                .cancelable(false)
                                .canceledOnTouchOutside(false)
                                .show();
                    }

                } else {
                    dialog.dismiss();
                    try {
                        JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                        String msgError = jObjError.getJSONObject("meta").getString("message");
                        if (msgError != null) {
                            Toast.makeText(getContext(), msgError, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                    }
                }
                Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<String>> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e(TAG, "onFailure: ", t);
            }
        });

    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 4;
    }
}
