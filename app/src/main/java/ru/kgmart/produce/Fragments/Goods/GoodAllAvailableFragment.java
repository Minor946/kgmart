package ru.kgmart.produce.Fragments.Goods;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.GoodRecyclerViewAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Listeners.EndlessScrollListener;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.ReceptionModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

public class GoodAllAvailableFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "GoodsFragments";

    private static int page = 1;
    private RecyclerView recyclerView;
    private LinearLayoutManager llm;

    @SuppressLint("UseSparseArrays")
    private
    Map<Integer, GoodsAdapterModel> map = new HashMap<>();
    private Map<Integer, GoodsAdapterModel> treeMap = new TreeMap<Integer, GoodsAdapterModel>();
    @SuppressLint("UseSparseArrays")
    private
    Map<Integer, GoodsAdapterModel> mapSubItem = new HashMap<>();

    private static final int limit = 20;
    private String type = null;
    private String sub_type = null;

    private SwipeRefreshLayout swipe_container;
    private RelativeLayout rl_empty;

    private MaterialDialog dialog;
    private static int sizeList = 0;
    private int items_size = 0;
    private Integer count_all = 0;

    private String product_id = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_goods, container, false);
        Crashlytics.setString("fragment", GoodAllAvailableFragment.class.toString());

        recyclerView = (RecyclerView) v.findViewById(R.id.rr_goods);
        swipe_container = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        rl_empty = (RelativeLayout) v.findViewById(R.id.rl_empty);
        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        dialog = DialogManager.newDialogLoading(getContext());

        Bundle extras = getArguments();

        if (extras != null) {
            type = extras.getString("type");
            sub_type = extras.getString("sub_type");
            product_id = extras.getString("product_id");
            Crashlytics.setString("current_level", type);
            Crashlytics.setString("sub_type", sub_type);
            Crashlytics.setString("product_id", product_id);
        }
        llm = new GridLayoutManager(getContext(), 1);

        initTitle(type);

        recyclerView.setLayoutManager(llm);
        GoodRecyclerViewAdapter goodRecyclerViewAdapter = new GoodRecyclerViewAdapter(treeMap, getContext(), type, sub_type, getActivity(), product_id, rl_empty);
        recyclerView.setAdapter(goodRecyclerViewAdapter);


        if (map.size() == 0) {
            addMoreItem(0);
        }

        recyclerView.addOnScrollListener(new EndlessScrollListener(llm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (map.size() >= limit && map.size() < count_all) {
                    addMoreItem(totalItemsCount);
                }
            }
        });
        return v;
    }

    @Override
    public void onRefresh() {
        addMoreItem(0);
    }


    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putString("type", type);
        outState.putString("sub_type", sub_type);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        initTitle(type);
        Log.d(TAG, "onResume:");
        super.onResume();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onStop: ");
        super.onDetach();
    }

    private void initTitle(String type) {
        Objects.requireNonNull(((AppCompatActivity)
                Objects.requireNonNull(getActivity())).getSupportActionBar())
                .setTitle(getResources().getString(R.string.action_need_to_carry));
    }

    public static int getCurrentPage() {
        return page;
    }

    public static int getSizeList() {
        return sizeList;
    }

    private void closeRefresh() {
        try {
            dialog.dismiss();
        } catch (IllegalArgumentException e) {
            Crashlytics.logException(e);
        }
        if (swipe_container.isRefreshing()) {
            swipe_container.setRefreshing(false);
        }
    }

    public void addMoreItem(int offset) {
        Call<RestResponse<GoodsModel>> call;
        dialog.show();
        if (offset == 0) {
            page = 1;
        }
        if (offset > 0) {
            page = offset / limit + 1;
        }
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        call = apiGoods.getGoodsToCarry(CommonComponents.getUserKey(getContext()), limit, page);
        int finalPage = page;
        try {
            call.enqueue(new Callback<RestResponse<GoodsModel>>() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                    if (response.isSuccessful()) {
                        RestResponse<GoodsModel> goodsModel = response.body();
                        RestResponse.Meta meta = Objects.requireNonNull(goodsModel).getMeta();
                        if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                            if (goodsModel.getData() != null && goodsModel.getData().getProducts() != null) {
                                rl_empty.setVisibility(View.GONE);
                                recyclerView.setVisibility(View.VISIBLE);
                                count_all = goodsModel.getData().getCount();
                                if (finalPage == 1) {
                                    map.clear();
                                    mapSubItem.clear();
                                    items_size = 0;
                                }
                                try {
                                    for (final ProductsModels productsModel : goodsModel.getData().getProducts()) {
                                        if (productsModel.getReception() != null) {
                                            List<ReceptionModel> list = productsModel.getReception();
                                            for (int j = 0; j < list.size(); j++) {
                                                GoodsAdapterModel model = new GoodsAdapterModel();
                                                model.setProductsModels(productsModel);
                                                model.setReceptionModel(list.get(j));
                                                mapSubItem.put(items_size, model);
                                                items_size++;
                                            }
                                        }
                                        if (mapSubItem != null && mapSubItem.size() > 0) {
                                            map.putAll(mapSubItem);
                                            mapSubItem.clear();
                                        }
                                    }
                                } catch (IndexOutOfBoundsException e) {
                                    Crashlytics.logException(e);
                                    addMoreItem(0);
                                }
                                sizeList = map.size();
                                treeMap.putAll(map);
                                Objects.requireNonNull(recyclerView.getAdapter()).notifyDataSetChanged();
                            }
                        }
                    } else {
                        rl_empty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                        map.clear();
                        mapSubItem.clear();
                        items_size = 0;
                    }
                    closeRefresh();
                }

                @Override
                public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                    Log.e(TAG, "onFailure: ", t);
                    closeRefresh();
                    if (map.size() == 0) {
                        rl_empty.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                    map.clear();
                    mapSubItem.clear();
                    items_size = 0;
                }
            });
        } catch (Exception ignored) {
        }
    }
}
