package ru.kgmart.produce.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.PasswordRecoveryModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class ResetPassFragment extends Fragment {

    private static final String TAG = "ResetPassFragment";
    private AutoCompleteTextView mEmailOrLogin;
    private Button reset_pass_button;
    String email_or_login;
    private TextView reset_pass_hint;
    MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_reset_pass, container, false);

        TextView to_sign_up = (TextView) v.findViewById(R.id.to_sign_up);

        reset_pass_hint = (TextView) v.findViewById(R.id.reset_pass_hint);

        to_sign_up.setOnClickListener(fragListen);

        mEmailOrLogin = (AutoCompleteTextView) v.findViewById(R.id.email);
        reset_pass_button = (Button) v.findViewById(R.id.reset_pass_button);

        reset_pass_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkResetForm();
            }
        });

        dialog = DialogManager.newDialogLoading(getContext());

        return v;
    }

    private void checkResetForm() {
        mEmailOrLogin.setError(null);

        email_or_login = mEmailOrLogin.getText().toString();

        boolean cancel = false;
        View focusView = null;
        // Check for a valid email address.

        if (TextUtils.isEmpty(email_or_login)) {
            mEmailOrLogin.setError(getString(R.string.error_field_required));
            focusView = mEmailOrLogin;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            startResetPass();
        }
    }

    private void startResetPass() {
        dialog.show();
        APi.Auth apiResetPass = APi.getRetrofitBuild().create(APi.Auth.class);

        PasswordRecoveryModel passwordRecoveryDto = new PasswordRecoveryModel();
        PasswordRecoveryModel.Details details = new PasswordRecoveryModel().new Details();
        details.setLoginOrEmail(email_or_login.trim());
        passwordRecoveryDto.setUserRecoveryForm(details);
        final Call<RestResponse<PasswordRecoveryModel>> call = apiResetPass.resetPassword(passwordRecoveryDto);

        call.enqueue(new Callback<RestResponse<PasswordRecoveryModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<PasswordRecoveryModel>> call, Response<RestResponse<PasswordRecoveryModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<PasswordRecoveryModel> resetData = response.body();

                    RestResponse.Meta meta = Objects.requireNonNull(resetData).getMeta();

                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        dialog.dismiss();

                        PasswordRecoveryModel userData = resetData.getData();
                        reset_pass_hint.setVisibility(View.VISIBLE);

                        reset_pass_button.setClickable(false);
                        reset_pass_button.setBackgroundColor(getResources().getColor(R.color.light_grey));
                        mEmailOrLogin.setEnabled(false);
                    }
                } else {
                    dialog.dismiss();
                    try {
                        JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                        String msgError = jObjError.getJSONObject("meta").getString("message");
                        if (msgError != null) {
                            Toast.makeText(getContext(), msgError, Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<PasswordRecoveryModel>> call, @NonNull Throwable t) {
                dialog.dismiss();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }

    View.OnClickListener fragListen = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switchFragment((String) v.getTag());
        }
    };

    private void switchFragment(String type) {
        Fragment fragment = null;
        String fragment_tag = "";
        if (type.equals("SignUp")) {
            fragment = new SignUpFragment();
            fragment_tag = "SignUp";
        } else if (type.equals("ResetPass")) {
            fragment = new ResetPassFragment();
            fragment_tag = "ResetPass";
        } else if (type.equals("Login")) {
            fragment = new LoginFragment();
            fragment_tag = "Login";
        }

        Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.mainFrame, fragment, fragment_tag)
                .commit();
    }
}
