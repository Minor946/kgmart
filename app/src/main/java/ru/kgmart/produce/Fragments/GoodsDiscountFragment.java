package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.datetimepicker.date.DatePickerDialog;
import com.crashlytics.android.Crashlytics;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.MainActivity;
import ru.kgmart.produce.Models.DiscountItemShopModel;
import ru.kgmart.produce.Models.DiscountShopModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created on 5/7/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsDiscountFragment extends Fragment {

    private static final String TAG = "GoodsDiscountFragment";
    TextView discount_models;
    TextView discount_value;
    TextView discount_begin;
    TextView discount_end;
    TextView discount_desc;

    Spinner discount_type;

    private int REQUEST_CODE_SELECT = 1;
    ArrayList<String> select_item = new ArrayList<>();

    Calendar calendar;
    String date;
    String dateShort;
    Boolean dateChange = false;

    Button btn_add_discount;

    MaterialDialog dialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_discount_form, container, false);

        Crashlytics.setString("fragment", "GoodsDiscountFragment");

        discount_models = (TextView) v.findViewById(R.id.discount_models);
        discount_value = (TextView) v.findViewById(R.id.discount_value);
        discount_begin = (TextView) v.findViewById(R.id.discount_begin);
        discount_end = (TextView) v.findViewById(R.id.discount_end);
        discount_desc = (TextView) v.findViewById(R.id.discount_desc);

        discount_type = (Spinner) v.findViewById(R.id.discount_type);
        btn_add_discount = (Button) v.findViewById(R.id.btn_add_discount);

        discount_models.setFocusable(false);
        discount_models.setClickable(true);

        discount_begin.setFocusable(false);
        discount_begin.setClickable(true);

        discount_end.setFocusable(false);
        discount_end.setClickable(true);

        calendar = Calendar.getInstance();

        discount_begin.setText(CommonComponents.getPublicDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), "."));

        dialog = DialogManager.newDialogLoading(getContext());

        discount_begin.setOnClickListener(onClickSelectDate);
        discount_end.setOnClickListener(onClickSelectDate);

        discount_models.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
                GoodsSelectFragment newFragment = new GoodsSelectFragment();
                newFragment.setTargetFragment(GoodsDiscountFragment.this, REQUEST_CODE_SELECT);

                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                transaction.add(android.R.id.content, newFragment)
                        .addToBackStack(this.getClass().getName()).commit();
            }
        });

        btn_add_discount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                btn_add_discount.setClickable(false);
                if (checkForm()) {
                    sendDiscount();
                } else {
                    dialog.dismiss();
                    btn_add_discount.setClickable(true);
                }
            }
        });

        return v;
    }

    private boolean checkForm() {
        discount_models.setError(null);
        discount_value.setError(null);
        discount_begin.setError(null);
        discount_end.setError(null);

        boolean cancel = false;
        View focusView = null;

        if (select_item.size() == 0) {
            discount_models.setError("Выберите хотя бы одну модель");
            cancel = true;
            focusView = discount_models;
        }
        if (TextUtils.isEmpty(discount_value.getText().toString())) {
            discount_value.setError("Укажите значение скидки");
            cancel = true;
            focusView = discount_value;
        }
        if (TextUtils.isEmpty(discount_begin.getText().toString())) {
            discount_begin.setError("Укажите дату начала скидки");
            cancel = true;
            focusView = discount_begin;
        }
        if (TextUtils.isEmpty(discount_end.getText().toString())) {
            discount_end.setError("Укажите дату завершения скидки");
            cancel = true;
            focusView = discount_end;
        }

        if (cancel) {
            focusView.requestFocus();
            return false;
        }

        return true;
    }

    private void sendDiscount() {
        GoodsModel goodsModel = new GoodsModel();
        DiscountItemShopModel discountItemShopModel = new DiscountItemShopModel();
        discountItemShopModel.setDiscountBegin(discount_begin.getText().toString());
        discountItemShopModel.setDiscountEnd(discount_begin.getText().toString());
        discountItemShopModel.setDiscountValue(discount_value.getText().toString());
        discountItemShopModel.setDiscountDesc(discount_desc.getText().toString());
        discountItemShopModel.setDiscountDesc(discount_desc.getText().toString());
        discountItemShopModel.setEndless("0");
        discountItemShopModel.setItems(select_item);
        discountItemShopModel.setDiscountType(String.valueOf(discount_type.getSelectedItemPosition()));

        goodsModel.setDiscountItemShopModel(discountItemShopModel);

        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);

        Call<RestResponse<DiscountShopModel>> call = apiGoods.addDiscount(goodsModel, CommonComponents.getUserKey(getContext()));
        call.enqueue(new Callback<RestResponse<DiscountShopModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<DiscountShopModel>> call, @NonNull Response<RestResponse<DiscountShopModel>> response) {
                Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "Скидки добавлены на рассмотрение", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();

                    Intent intent = new Intent(getContext(), MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Objects.requireNonNull(getActivity()).startActivity(intent);

                } else {
                    dialog.dismiss();
                    btn_add_discount.setClickable(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<DiscountShopModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
                dialog.dismiss();
                btn_add_discount.setClickable(true);
            }
        });
    }

    View.OnClickListener onClickSelectDate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (!TextUtils.isEmpty(date)) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    calendar.setTime(sdf.parse(String.valueOf(date)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            date = "";
            DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                             @Override
                                             public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                                                 dateChange = true;
                                                 date = year + "-";
                                                 if ((monthOfYear + 1) < 10) {
                                                     date += "0" + (monthOfYear + 1);
                                                 } else {
                                                     date += (monthOfYear + 1);
                                                 }
                                                 date += "-";
                                                 if (dayOfMonth < 10) {
                                                     date += "0" + dayOfMonth;

                                                 } else {
                                                     date += dayOfMonth;
                                                 }
                                                 if (v.getTag().equals("begin")) {
                                                     discount_begin.setText(CommonComponents.getPublicDate(year, monthOfYear, dayOfMonth, "."));
                                                 } else {
                                                     discount_end.setText(CommonComponents.getPublicDate(year, monthOfYear, dayOfMonth, "."));
                                                 }

                                             }
                                         }, calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show(Objects.requireNonNull(getActivity()).getFragmentManager(), "datePicker");
        }
    };

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_SELECT && resultCode == Activity.RESULT_OK) {
            select_item = data.getStringArrayListExtra("select_item");
            int count_items = 0;
            if (select_item != null) {
                count_items = select_item.size();
            }
            discount_models.setText("Выбранно: " + String.valueOf(count_items));
        }
    }
}
