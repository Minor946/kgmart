package ru.kgmart.produce.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import ru.kgmart.produce.MainActivity;
import ru.kgmart.produce.R;

/**
 * Created on 4/20/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class StockFragment extends Fragment {

    private static final String TAG = "StockFragment";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_stock_wait, container, false);


        try {
            FloatingActionButton fab = ((MainActivity) Objects.requireNonNull(this.getActivity())).getFab();
            fab.hide();
        } catch (Exception ignored) {
        }

        return v;
    }
}
