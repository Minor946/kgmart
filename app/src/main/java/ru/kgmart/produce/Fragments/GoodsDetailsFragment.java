package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.ColorsRecyclerAdapter;
import ru.kgmart.produce.Adapters.ImageHorizontalAdapter;
import ru.kgmart.produce.Adapters.SizesRecyclerAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.CommonItemSpaceDecoration;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Models.ColorModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.SizeModel;
import ru.kgmart.produce.R;

/**
 * Created on 6/28/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsDetailsFragment extends Fragment {

    private static final String TAG = "GoodsDetailsFragment";
    String id = null;
    String image_url = null;
    boolean isMain = false;

    TextView product_name;
    TextView product_price;
    TextView product_price_discount;
    TextView product_price_label;
    TextView product_id;
    TextView product_stock;
    TextView product_category;
    TextView product_weight;
    TextView product_description;
    TextView product_number_in_line;
    TextView product_wholesale;
    TextView product_trade_price;

    CardView cv_product_in_stock;
    CardView card_colors;
    CardView card_sizes;

    RecyclerView rr_product_colors;
    RecyclerView rr_product_size;
    RecyclerView rv_product_images;

    RelativeLayout product_price_layout;
    RelativeLayout rl_product_images;
    RelativeLayout rl_product_stock;

    LinearLayout ll_product_wholesale;
    LinearLayout ll_product_trade_price;

    ArrayList<String> imageList = new ArrayList<>();
    ArrayList<String> imageThumbList = new ArrayList<>();

    Button product_update;

    ColorsRecyclerAdapter colorsRecyclerAdapter;
    List<ColorModel> colorsList = new ArrayList<>();
    SizesRecyclerAdapter sizesRecyclerAdapter;
    List<SizeModel> sizeList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_goods_details, container, false);
        Bundle extras = getArguments();
        if (extras != null) {
            id = extras.getString("product_id");
        }
        product_name = (TextView) v.findViewById(R.id.product_name);
        product_price = (TextView) v.findViewById(R.id.product_price);
        product_price_discount = (TextView) v.findViewById(R.id.product_price_discount);
        product_price_label = (TextView) v.findViewById(R.id.product_price_label);
        product_id = (TextView) v.findViewById(R.id.product_id);
        product_stock = (TextView) v.findViewById(R.id.product_stock);
        product_weight = (TextView) v.findViewById(R.id.product_weight);
        product_description = (TextView) v.findViewById(R.id.product_description);
        product_number_in_line = (TextView) v.findViewById(R.id.product_number_in_line);
        product_wholesale = (TextView) v.findViewById(R.id.product_wholesale);
        product_trade_price = (TextView) v.findViewById(R.id.product_trade_price);
        product_category = (TextView) v.findViewById(R.id.product_category);

        cv_product_in_stock = (CardView) v.findViewById(R.id.cv_product_in_stock);
        card_colors = (CardView) v.findViewById(R.id.card_colors);
        card_sizes = (CardView) v.findViewById(R.id.card_sizes);

        rr_product_colors = (RecyclerView) v.findViewById(R.id.rr_product_colors);
        rr_product_size = (RecyclerView) v.findViewById(R.id.rr_product_size);
        rv_product_images = (RecyclerView) v.findViewById(R.id.rv_product_images);

        product_price_layout = (RelativeLayout) v.findViewById(R.id.product_price_layout);
        rl_product_images = (RelativeLayout) v.findViewById(R.id.rl_product_images);
        rl_product_stock = (RelativeLayout) v.findViewById(R.id.rl_product_stock);

        ll_product_wholesale = (LinearLayout) v.findViewById(R.id.ll_product_wholesale);
        ll_product_trade_price = (LinearLayout) v.findViewById(R.id.ll_product_trade_price);

        product_update = (Button) v.findViewById(R.id.product_update);

        Crashlytics.setString("fragment", "GoodsDetailsFragment");
        Crashlytics.setString("product_id", id);

        initActivity(id);
        return v;
    }

    private void initActivity(String id) {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        Call<RestResponse<GoodsModel>> call = apiGoods.getGoodInfo(CommonComponents.getUserKey(getContext()), id);
        call.enqueue(new Callback<RestResponse<GoodsModel>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                RestResponse<GoodsModel> goodsModel = response.body();
                RestResponse.Meta meta = Objects.requireNonNull(goodsModel).getMeta();
                if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                    ProductsModels model = goodsModel.getData().getProducts().get(0);

                    product_name.setText(String.valueOf(model.getName()));
                    try {
                        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(String.valueOf(model.getName()));
                    } catch (NullPointerException e) {
                        Crashlytics.logException(e);
                    }
                    product_id.setText(String.valueOf(model.getId()));
                    product_description.setText(String.valueOf(model.getDescription()));
                    product_weight.setText(String.valueOf(model.getWeight()) + " гр.");
                    product_number_in_line.setText(String.valueOf(model.getNumberInLine()) + " ед.");
                    product_category.setText(model.getCategoryName());
                    product_stock.setText(GoodsComponents.getStockLabel(model.getVisible()));
                    if (model.getStatusVisible() != null) {
                        product_stock.setText(GoodsComponents.getStockLabel(model.getStatusVisible()));
                    } else {
                        product_stock.setText(GoodsComponents.getStockLabel(model.getVisible()));
                    }
                    if (image_url == null) {
                        image_url = model.getMainImage();
                        isMain = true;
                    }
                    if (imageList.size() == 0) {
                        imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
                        imageThumbList.addAll(GoodsComponents.getMainThumbImageList(model.getMainImage(), model.getImages()));
                    }
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

                    if (model.getColors() != null && model.getColors().size() > 0) {
                        LinearLayoutManager layoutManagerColor = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                        colorsList = model.getColors();
                        colorsRecyclerAdapter = new ColorsRecyclerAdapter(getContext(), colorsList);
                        rr_product_colors.setLayoutManager(layoutManagerColor);
                        rr_product_colors.setAdapter(colorsRecyclerAdapter);
                    } else {
                        card_colors.setVisibility(View.GONE);
                    }

                    if (model.getSizes() != null && model.getSizes().size() > 0) {
                        int mNoOfColumns = Utility.calculateNoOfColumns(getContext());
                        GridLayoutManager mGridLayoutManager = new GridLayoutManager(getContext(), mNoOfColumns);
                        sizeList = model.getSizes();
                        sizesRecyclerAdapter = new SizesRecyclerAdapter(getActivity().getApplicationContext(), sizeList);
                        rr_product_size.setLayoutManager(mGridLayoutManager);
                        rr_product_size.setAdapter(sizesRecyclerAdapter);
                    } else {
                        card_sizes.setVisibility(View.GONE);
                    }

                    rv_product_images.setLayoutManager(layoutManager);
                    rv_product_images.addItemDecoration(new CommonItemSpaceDecoration(5));
                    rv_product_images.setAdapter(new ImageHorizontalAdapter(getActivity(), imageThumbList, imageList));

                    if (model.getSellOut().equals("1")) {
                        product_price.setText(String.valueOf(model.getSelloutPrice()));
                        product_price.setPaintFlags(product_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        product_price_discount.setText(String.valueOf(model.getPrice()));
                    } else {
                        product_price.setText(String.valueOf(model.getPrice()));
                    }

                    if (Integer.valueOf(model.getWholesale()) > 0) {
                        ll_product_wholesale.setVisibility(View.VISIBLE);
                        ll_product_trade_price.setVisibility(View.VISIBLE);

                        product_wholesale.setText(model.getWholesale() + " ЛР");
                        product_trade_price.setText(model.getTradePrice() + " сом");
                    }

                    if (model.getConsigment() != null && model.getConsigment().size() > 0) {
                        cv_product_in_stock.setVisibility(View.VISIBLE);

                        rl_product_stock.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                GoodsFragment goodsFragment = new GoodsFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("product_id", String.valueOf(id));
                                bundle.putString("type", String.valueOf(GoodEnum.TYPE_ALL));
                                bundle.putString("sub_type", String.valueOf(GoodEnum.SUB_TYPE_ON_CONSIGN));
                                goodsFragment.setArguments(bundle);

                                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                                FragmentTransaction transaction = fragmentManager.beginTransaction();
                                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);
                                transaction.replace(R.id.mainFrame, goodsFragment, "GoodsStockDetails");
                                transaction.addToBackStack("GoodsDetails");
                                transaction.commit();
                            }
                        });
                    }
                    product_update.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            GoodsFormFragment goodsFormFragment = new GoodsFormFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("product_id", String.valueOf(id));
                            bundle.putSerializable("model", new Gson().toJson(model));
                            goodsFormFragment.setArguments(bundle);

                            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                            FragmentTransaction transaction = fragmentManager.beginTransaction();
                            transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit);

                            transaction.replace(R.id.mainFrame, goodsFormFragment, "goodForm");
                            transaction.addToBackStack("GoodsDetails");
                            transaction.commit();
                        }
                    });
                } else {
                    Log.d(TAG, "onResponse: error");
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    public static class Utility {
        static int calculateNoOfColumns(Context context) {
            if(context != null && context.getResources() != null){
                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
                return (int) (dpWidth / 180);
            }
            return 0;
        }
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume:");
        super.onResume();
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart: ");
        super.onStart();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause: ");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop: ");
        super.onStop();
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onStop: ");
        super.onDetach();
    }
}
