package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.datetimepicker.date.DatePickerDialog;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.LineChartView;
import lecho.lib.hellocharts.view.PieChartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.GoodsActivity;
import ru.kgmart.produce.Models.BookkeepingsModel;
import ru.kgmart.produce.Models.GraphicModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.StatisticModel;
import ru.kgmart.produce.R;

public class StatisticFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private static final String TAG = "StatisticFragment";
    private PieChartView pieChartView;
    private PieChartData pieChartData;
    private LineChartView lineChartView;
    private LineChartData lineChartData;

    private LineChartView lineChartViewStock;
    private LineChartData lineChartStockData;

    SwipeRefreshLayout swipe_container;

    Call<RestResponse<StatisticModel>> call;
    APi.ApiProfile apiProfile;

    TextView value_accept_balance;
    TextView value_wait_balance;
    TextView value_lr_income_consign;
    TextView value_lr_income;
    TextView value_nl_consign;
    TextView value_nl;
    TextView value_return_consign;
    TextView value_return;
    TextView value_sum_return_consign;
    TextView value_sum_return;
    TextView value_sum_consign;
    TextView value_sum;
    TextView value_full_sum;
    TextView label_period;

    TextView value_income_stock;
    TextView value_income_stock_sum;
    TextView value_sold_stock;
    TextView value_sold_stock_sum;
    TextView value_return_stock;
    TextView value_return_stock_sum;
    TextView value_ost_stock;
    TextView value_ost_stock_sum;
    TextView value_full_sum_stock;

    Button btn_collect_money;
    Button sendProduct;

    MaterialDialog.Builder dialogAlert;
    MaterialDialog dialog;
    private String start_date;
    private String finish_date;
    private String select_date;
    Calendar calendar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_statistic, container, false);
        Crashlytics.setString("fragment", "StatisticFragment");

        swipe_container = (SwipeRefreshLayout) v.findViewById(R.id.swipe_container);
        swipe_container.setOnRefreshListener(this);
        swipe_container.setColorSchemeColors(getResources().getColor(R.color.colorAccent));

        btn_collect_money = (Button) v.findViewById(R.id.btn_collect_money);
        sendProduct = (Button) v.findViewById(R.id.sendProduct);

        dialog =  new MaterialDialog.Builder(Objects.requireNonNull(getContext()))
                .autoDismiss(false)
                .cancelable(false)
                .customView(R.layout.dialog_loading, true).build();

        pieChartView = (PieChartView) v.findViewById(R.id.chart_income);
        lineChartView = (LineChartView) v.findViewById(R.id.chart_diff);
        lineChartViewStock = (LineChartView) v.findViewById(R.id.chart_stock);

        value_accept_balance = (TextView) v.findViewById(R.id.value_accept_balance);
        value_wait_balance = (TextView) v.findViewById(R.id.value_wait_balance);
        value_lr_income_consign = (TextView) v.findViewById(R.id.value_lr_income_consign);
        value_lr_income = (TextView) v.findViewById(R.id.value_lr_income);
        value_nl_consign = (TextView) v.findViewById(R.id.value_nl_consign);
        value_nl = (TextView) v.findViewById(R.id.value_nl);
        value_return_consign = (TextView) v.findViewById(R.id.value_return_consign);
        value_return = (TextView) v.findViewById(R.id.value_return);
        value_sum_return_consign = (TextView) v.findViewById(R.id.value_sum_return_consign);
        value_sum_return = (TextView) v.findViewById(R.id.value_sum_return);
        value_sum_consign = (TextView) v.findViewById(R.id.value_sum_consign);
        value_sum = (TextView) v.findViewById(R.id.value_sum);
        value_full_sum = (TextView) v.findViewById(R.id.value_full_sum);
        label_period = (TextView) v.findViewById(R.id.label_period);

        value_income_stock = (TextView) v.findViewById(R.id.value_income_stock);
        value_income_stock_sum = (TextView) v.findViewById(R.id.value_income_stock_sum);
        value_sold_stock = (TextView) v.findViewById(R.id.value_sold_stock);
        value_sold_stock_sum = (TextView) v.findViewById(R.id.value_sold_stock_sum);
        value_return_stock = (TextView) v.findViewById(R.id.value_return_stock);
        value_return_stock_sum = (TextView) v.findViewById(R.id.value_return_stock_sum);
        value_ost_stock = (TextView) v.findViewById(R.id.value_ost_stock);
        value_ost_stock_sum = (TextView) v.findViewById(R.id.value_ost_stock_sum);
        value_full_sum_stock = (TextView) v.findViewById(R.id.value_full_sum_stock);

        initStatistic(-1, null, null);

        btn_collect_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> disabledDate = new ArrayList<>();
                APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);
                Call<RestResponse<BookkeepingsModel>> callDisabled = apiOther.getDisabledDate(CommonComponents.getUserKey(getContext()));
                callDisabled.enqueue(new Callback<RestResponse<BookkeepingsModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Response<RestResponse<BookkeepingsModel>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                disabledDate.addAll(response.body().getData().getNewBookkeping().getDisabledDate());
                            } catch (Exception e) {
                                Log.e(TAG, "onResponse: ", e);
                            }
                        }
                    }
                    @Override
                    public void onFailure(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Throwable t) {
                        Log.e(TAG, "onFailure: ", t );
                    }
                });
                Calendar maxDate = Calendar.getInstance();
                maxDate.set(Calendar.MONTH, maxDate.get(Calendar.MONTH) + 2);
                DatePickerDialog finishPicker = new DatePickerDialog();
                finishPicker.setMinDate(Calendar.getInstance());
                finishPicker.setMaxDate(maxDate);

                finishPicker.show(Objects.requireNonNull(getActivity()).getFragmentManager(), "datePicker");
                finishPicker.registerOnDateChangedListener(new DatePickerDialog.OnDateChangedListener() {
                    @Override
                    public void onDateChanged() {
                        String selectDate = CommonComponents.getMysqlDate(finishPicker.getSelectedDay().getYear(),
                                finishPicker.getSelectedDay().getMonth(),
                                finishPicker.getSelectedDay().getDay(), "-");
                        if (disabledDate.contains(selectDate)) {
                            select_date = null;
                            Toast.makeText(getContext(), "Эта дата занята, выберите другую", Toast.LENGTH_SHORT).show();
                        }else{
                            select_date = selectDate;
                        }
                    }
                });
                finishPicker.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                        if(!TextUtils.isEmpty(select_date)){
                            BookkeepingsModel model = new BookkeepingsModel();
                            BookkeepingsModel.Details details = new BookkeepingsModel.Details();
                            details.setAmount("0");
                            details.setBooking_date(select_date);
                            model.setNewBookkeping(details);
                            Call<RestResponse<BookkeepingsModel>> call = apiOther.claimBooking(model, CommonComponents.getUserKey(getContext()));
                            call.enqueue(new Callback<RestResponse<BookkeepingsModel>>() {
                                @Override
                                public void onResponse(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Response<RestResponse<BookkeepingsModel>> response) {
                                    if (response.isSuccessful()) {
                                        Toast.makeText(getContext(), "Заявка принята. Будет отправлено уведомление о потверждении", Toast.LENGTH_LONG).show();

                                    } else {
                                        Toast.makeText(getContext(), "Недостаточно средст", Toast.LENGTH_LONG).show();
                                    }
                                    Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
                                }

                                @Override
                                public void onFailure(@NonNull Call<RestResponse<BookkeepingsModel>> call, @NonNull Throwable t) {
                                    Log.e(TAG, "onFailure: ", t);
                                }
                            });
                        }
                    }
                });
            }
        });

        sendProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), GoodsActivity.class);
                intent.putExtra("type", String.valueOf(GoodEnum.TYPE_STOCK_SALES));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
        return v;
    }

    @SuppressLint("SetTextI18n")
    private void initStatistic(int position, String first_date, String second_date) {
        dialog.show();
        apiProfile = APi.getRetrofitBuild().create(APi.ApiProfile.class);
        if (position == -1 || position == 6) {
            call = apiProfile.getStatistic(CommonComponents.getUserKey(getContext()), null, first_date, second_date);
            if(position == -1){
                label_period.setText("за последний год");
            }else {
                label_period.setText(first_date + " - " + second_date);
            }
        } else {
            call = apiProfile.getStatistic(CommonComponents.getUserKey(getContext()), String.valueOf(position), first_date, second_date);
            String[] periodArray = getContext().getResources().getStringArray(R.array.periods);
           if(periodArray[position] != null){
               if(position == 3){
                   label_period.setText("за неделю");
               }else{
                   label_period.setText("за " +periodArray[position].toLowerCase());
               }
           }
        }
        call.enqueue(new Callback<RestResponse<StatisticModel>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RestResponse<StatisticModel>> call, @NonNull Response<RestResponse<StatisticModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<StatisticModel> restResponse = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(restResponse).getMeta();
                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        StatisticModel model = restResponse.getData();
                        value_accept_balance.setText(String.valueOf(model.getAccessBalance()) + " сом");
                        if (String.valueOf(model.getAccessBalance()).equals("0")) {
                            btn_collect_money.setVisibility(View.GONE);
                        }
                        value_wait_balance.setText(String.valueOf(model.getWaitBalance()) + " сом");
                        if (model.getStatisticDetails() != null) {
                            if (model.getStatisticDetails().getIncome() != null) {
                                value_lr_income.setText(String.valueOf(model.getStatisticDetails().getIncome().getReceiveQuantity()) + " ЛР");
                                value_nl.setText(String.valueOf(model.getStatisticDetails().getIncome().getReceiveNl()) + " ед");
                                value_return.setText(String.valueOf(model.getStatisticDetails().getIncome().getRevertNl()) + " ед");
                                value_sum_return.setText(String.valueOf(model.getStatisticDetails().getIncome().getSumRevert()) + " сом");
                                value_sum.setText(String.valueOf(model.getStatisticDetails().getIncome().getSum()) + " сом");
                            }
                            if (model.getStatisticDetails().getConsign() != null) {
                                value_lr_income_consign.setText(String.valueOf(model.getStatisticDetails().getConsign().getReceiveQuantity()) + " ЛР");
                                value_nl_consign.setText(String.valueOf(model.getStatisticDetails().getConsign().getReceiveNl()) + " ед");
                                value_return_consign.setText(String.valueOf(model.getStatisticDetails().getConsign().getRevertNl()) + " ед");
                                value_sum_return_consign.setText(String.valueOf(model.getStatisticDetails().getConsign().getSumRevert()) + " сом");
                                value_sum_consign.setText(String.valueOf(model.getStatisticDetails().getConsign().getSum()) + " сом");
                            }
                            value_full_sum.setText(String.valueOf(model.getStatisticDetails().getFullSum()) + " сом");
                        }
                        if (model.getStatisticStock() != null) {
                            value_income_stock.setText(String.valueOf(model.getStatisticStock().getQuantity()) + " ед");
                            value_income_stock_sum.setText(String.valueOf(model.getStatisticStock().getSum()) + " сом");
                            value_sold_stock.setText(String.valueOf(model.getStatisticStock().getSold()) + " ед");
                            value_sold_stock_sum.setText(String.valueOf(model.getStatisticStock().getSoldSum()) + " сом");
                            value_return_stock.setText(String.valueOf(model.getStatisticStock().getReturnVal()) + " ед");
                            value_return_stock_sum.setText(String.valueOf(model.getStatisticStock().getReturnSum()) + " сом");
                            value_ost_stock.setText(String.valueOf(model.getStatisticStock().getOstQuantity()) + " ед");
                            value_ost_stock_sum.setText(String.valueOf(model.getStatisticStock().getOstSum()) + " сом");
                            value_full_sum_stock.setText(String.valueOf(model.getStatisticStock().getFullSumGet()) + " сом");
                        }

                        generateDataPie(model);
                        if (model.getDiagramIncome() != null) {
                            generateDataLine(model.getDiagramIncome());
                        }
                        if (model.getDiagramStock() != null) {
                            generateDataStockLine(model.getDiagramStock());
                        }
                    }
                } else {
                    Log.d(TAG, "onResponse: " + String.valueOf(response.code()));
                }
                closeRefresh();
                dialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<StatisticModel>> call, @NonNull Throwable t) {
                closeRefresh();
                dialog.dismiss();
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }

    private void generateDataLine(List<GraphicModel> diagramIncome) {
        lineChartView.setInteractive(true);
        lineChartView.setZoomEnabled(false);
        lineChartView.setPadding(5, 25, 25, 35);

        ArrayList<AxisValue> labelAxisX = new ArrayList<AxisValue>();
        ArrayList<AxisValue> labelAxisY = new ArrayList<AxisValue>();
        int maxNumberOfLines = 1;
        float[][] randomNumbersTab = new float[maxNumberOfLines][diagramIncome.size()];

        for (int i = 0; i < maxNumberOfLines; ++i) {
            for (int j = 0; j < diagramIncome.size(); ++j) {
                randomNumbersTab[i][j] = (float) Float.valueOf(diagramIncome.get(j).getSum());
                labelAxisX.add(new AxisValue((float) j).setLabel(CommonComponents.getMount(getContext(), diagramIncome.get(j).getMonth())));
            }
        }
        List<Line> lines = new ArrayList<Line>();
        for (int i = 0; i < maxNumberOfLines; ++i) {
            List<PointValue> values = new ArrayList<PointValue>();
            for (int j = 0; j < diagramIncome.size(); ++j) {
                values.add(new PointValue(j, randomNumbersTab[i][j]));
            }
            Line line = new Line(values);
            line.setColor(ChartUtils.COLOR_GREEN);
            line.setCubic(true);
            line.setFilled(true);
            line.setHasLabels(true);
            line.setHasLabelsOnlyForSelected(false);
            line.setHasLines(true);
            line.setHasPoints(true);
            line.setPointColor(ChartUtils.COLOR_BLUE);
            lines.add(line);
        }

        lineChartData = new LineChartData(lines);

        Axis axisY = new Axis().setHasLines(true);
        lineChartData.setAxisXBottom(new Axis(labelAxisX));
        lineChartData.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(false));

        lineChartData.setBaseValue(Float.NEGATIVE_INFINITY);
        lineChartView.setLineChartData(lineChartData);

    }

    private void generateDataStockLine(List<GraphicModel> diagramIncome) {
        lineChartViewStock.setInteractive(true);
        lineChartViewStock.setZoomEnabled(false);
        lineChartViewStock.setPadding(5, 25, 25, 35);

        ArrayList<AxisValue> labelAxisT = new ArrayList<AxisValue>();
        int maxNumberOfLines = 2;
        float[][] randomNumbersTab = new float[maxNumberOfLines][diagramIncome.size()];

        for (int i = 0; i < maxNumberOfLines; ++i) {
            for (int j = 0; j < diagramIncome.size(); ++j) {
                if (i == 0) {
                    randomNumbersTab[i][j] = (float) Float.valueOf(diagramIncome.get(j).getSumIncome());
                } else {
                    randomNumbersTab[i][j] = (float) Float.valueOf(diagramIncome.get(j).getSumSold());
                }
                labelAxisT.add(new AxisValue((float) j).setLabel(CommonComponents.getMount(getContext(), diagramIncome.get(j).getMonth())));
            }
        }
        List<Line> lines = new ArrayList<Line>();
        for (int i = 0; i < maxNumberOfLines; ++i) {
            List<PointValue> values = new ArrayList<PointValue>();
            for (int j = 0; j < diagramIncome.size(); ++j) {
                values.add(new PointValue(j, randomNumbersTab[i][j]));
            }
            Line line = new Line(values);

            line.setCubic(true);
            line.setFilled(true);
            line.setHasLabels(false);
            line.setHasLabelsOnlyForSelected(false);
            line.setHasLines(true);
            line.setHasPoints(true);
            if (i == 0) {
                line.setPointColor(ChartUtils.COLOR_BLUE);
                line.setColor(ChartUtils.COLOR_BLUE);
            } else {
                line.setPointColor(ChartUtils.COLOR_GREEN);
                line.setColor(ChartUtils.COLOR_GREEN);
            }
            lines.add(line);
        }

        lineChartStockData = new LineChartData(lines);

        lineChartStockData.setAxisXBottom(new Axis(labelAxisT));
        lineChartStockData.setAxisYLeft(new Axis().setHasLines(true).setHasTiltedLabels(false));

        lineChartStockData.setBaseValue(Float.NEGATIVE_INFINITY);
        lineChartViewStock.setLineChartData(lineChartStockData);
    }

    private void generateDataPie(StatisticModel model) {
        List<SliceValue> values = new ArrayList<SliceValue>();
        if (model.getSumStatusConsign() != 0) {
            values.add(new SliceValue((float) model.getSumStatusConsign(), ChartUtils.COLOR_GREEN));
        }
        if (model.getSumStatusIncome() != 0) {
            values.add(new SliceValue((float) model.getSumStatusIncome(), ChartUtils.COLOR_ORANGE));
        }
        if (model.getSumStatusNoPay() != 0) {
            values.add(new SliceValue((float) model.getSumStatusNoPay(), ChartUtils.DEFAULT_COLOR));
        }
        if (model.getSumStatusReturn() != 0) {
            values.add(new SliceValue((float) model.getSumStatusReturn(), ChartUtils.COLOR_RED));
        }

        pieChartData = new PieChartData(values);
        pieChartData.setHasLabels(true);
        pieChartData.setHasLabelsOnlyForSelected(false);
        pieChartData.setHasLabelsOutside(false);
        pieChartData.setHasCenterCircle(false);
        pieChartView.setPieChartData(pieChartData);
    }

    DatePickerDialog finishPicker;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        MenuItem action_schedule = menu.findItem(R.id.action_schedule);
        action_schedule.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                dialogAlert = new MaterialDialog.Builder(Objects.requireNonNull(getActivity()))
                        .title("Выберите период")
                        .items(R.array.periods)
                        .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                            @Override
                            public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                                switch (which) {
                                    case 6:
                                        calendar = Calendar.getInstance();
                                        Calendar calendar_finish = Calendar.getInstance();
                                        DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
                                                                         @Override
                                                                         public void onDateSet(DatePickerDialog dialog, int year, int monthOfYear, int dayOfMonth) {
                                                                             start_date = CommonComponents.getMysqlDate(year, monthOfYear, dayOfMonth, "-");
                                                                             Log.d(TAG, "onSelection: start_date " + start_date);
                                                                             calendar_finish.set(year, monthOfYear, dayOfMonth);
                                                                             finishPicker = new DatePickerDialog();
                                                                             finishPicker.setMinDate(calendar_finish);
                                                                             finishPicker.show(Objects.requireNonNull(getActivity()).getFragmentManager(), "datePicker");
                                                                             finishPicker.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
                                                                                 @Override
                                                                                 public void onDateSet(DatePickerDialog dialog, int year2, int monthOfYear2, int dayOfMonth2) {
                                                                                     finish_date = CommonComponents.getMysqlDate(year2, monthOfYear2, dayOfMonth2, "-");
                                                                                     Log.d(TAG, "onSelection: finish_date " + finish_date);
                                                                                     initStatistic(which, start_date, finish_date);
                                                                                 }
                                                                             });
                                                                         }
                                                                     },
                                                calendar.get(Calendar.YEAR),
                                                calendar.get(Calendar.MONTH),
                                                calendar.get(Calendar.DAY_OF_MONTH)).show(Objects.requireNonNull(getActivity()).getFragmentManager(), "datePicker");
                                        break;
                                    default:
                                        initStatistic(which, null, null);
                                        break;
                                }
                                return true;
                            }
                        })
                        .positiveText(R.string.select);
                dialogAlert.show();

                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }


    private void closeRefresh() {
        if (swipe_container.isRefreshing()) {
            swipe_container.setRefreshing(false);
        }
    }

    @Override
    public void onRefresh() {
        initStatistic(-1, null, null);
    }
}
