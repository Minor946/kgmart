package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.ProfileModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = "ProfileFragment";

    TextView profile_username;
    TextView profile_birthday;
    TextView profile_email;
    TextView profile_phone;
    TextView profile_more_phone;
    TextView profile_address;
    TextView profile_login;
    TextView profile_value_date_registration;
    TextView profile_value_number_contract;

    ProfileModel model;

    Button action_logout;

    RelativeLayout rl_change_password;
    RelativeLayout rr_user_login;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_user_profile, container, false);

        Crashlytics.setString("fragment", "ProfileFragment");

        profile_username = (TextView) v.findViewById(R.id.profile_username);
        profile_birthday = (TextView) v.findViewById(R.id.profile_birthday);
        profile_email = (TextView) v.findViewById(R.id.profile_email);
        profile_phone = (TextView) v.findViewById(R.id.profile_phone);
        profile_more_phone = (TextView) v.findViewById(R.id.profile_more_phone);
        profile_login = (TextView) v.findViewById(R.id.profile_login);
        profile_address = (TextView) v.findViewById(R.id.profile_address);
        profile_value_date_registration = (TextView) v.findViewById(R.id.profile_value_date_registration);
        profile_value_number_contract = (TextView) v.findViewById(R.id.profile_value_number_contract);

        rl_change_password = (RelativeLayout) v.findViewById(R.id.rl_change_password);
        rr_user_login = (RelativeLayout) v.findViewById(R.id.rr_user_login);

        action_logout = (Button) v.findViewById(R.id.action_logout);

        Button action_edit_profile = v.findViewById(R.id.action_edit_profile);
        action_edit_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileEditFragment profileFragment = new ProfileEditFragment();
                Bundle bundle = new Bundle();

                bundle.putSerializable("model", new Gson().toJson(model));
                profileFragment.setArguments(bundle);
                Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.mainFrame, profileFragment, "ProfileEdit")
                        .addToBackStack("Profile")
                        .commit();
            }
        });

        action_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MaterialDialog dialog = new MaterialDialog.Builder(Objects.requireNonNull(getContext()))
                        .title(R.string.logout_title)
                        .content(R.string.logout_content)
                        .positiveText(R.string.logout_agree)
                        .negativeText(R.string.logout_disagree)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                PreferenceManager.getDefaultSharedPreferences(getContext()).edit().clear().apply();
                                Intent i = Objects.requireNonNull(getContext()).getPackageManager().getLaunchIntentForPackage(getContext().getPackageName());
                                Objects.requireNonNull(i).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(i);
                            }
                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onAny(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .show();
            }
        });

        rl_change_password.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("CommitTransaction")
            @Override
            public void onClick(View v) {
                ProfileEditPasswordFragment profileEditPasswordFragment = new ProfileEditPasswordFragment();

                Objects.requireNonNull(getActivity()).getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right, R.anim.pop_enter, R.anim.pop_exit)
                        .replace(R.id.mainFrame, profileEditPasswordFragment, "ProfileEditPassword")
                        .addToBackStack("Profile")
                        .commit();
            }
        });

        APi.ApiProfile apiProfile = APi.getRetrofitBuild().create(APi.ApiProfile.class);
        Log.d(TAG, "onCreateView: key_id" + CommonComponents.getUserKey(getContext()));
        Call<RestResponse<ProfileModel>> call = apiProfile.getProfile(CommonComponents.getUserKey(getContext()));

        call.enqueue(new Callback<RestResponse<ProfileModel>>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<RestResponse<ProfileModel>> call, @NonNull Response<RestResponse<ProfileModel>> response) {
                if (response.isSuccessful()) {
                    RestResponse<ProfileModel> profileData = response.body();
                    RestResponse.Meta meta = Objects.requireNonNull(profileData).getMeta();
                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                        model = profileData.getData();
                        profile_username.setText(model.getProfile().getFirstname());
                        profile_email.setText(model.getProfile().getEmail());
                        profile_phone.setText(model.getProfile().getPhone());
                        if (model.getShop() != null) {
                            profile_login.setText(model.getShop().getLogin());
                            profile_value_number_contract.setText(model.getShop().getCondit());
                        } else {
                            rr_user_login.setVisibility(View.GONE);
                        }
                        if (model.getProfile().getAddress() != null && !TextUtils.isEmpty(model.getProfile().getAddress())) {
                            profile_address.setText(model.getProfile().getAddress());
                        }
                        if (model.getProfile().getDob() != null && !TextUtils.isEmpty(model.getProfile().getDob())) {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                            try {
                                calendar.setTime(sdf.parse(String.valueOf(model.getProfile().getDob())));
                                profile_birthday.setText(CommonComponents.getPublicDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), "."));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d(TAG, "onResponse: " + model.getProfile().getOtherContact());
                        if (model.getProfile().getOtherContact() != null && !TextUtils.isEmpty(model.getProfile().getOtherContact())) {
                            profile_more_phone.setText(model.getProfile().getOtherContact());
                        }
                        if (model.getProfile().getCreateAt() != null) {
                            Calendar calendar = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH);
                            try {
                                calendar.setTime(sdf.parse(String.valueOf(model.getProfile().getCreateAt())));
                                profile_value_date_registration.setText(CommonComponents.getPublicDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE), "."));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<ProfileModel>> call, @NonNull Throwable t) {

            }
        });

        return v;
    }

    @Override
    public void onResume() {
        if (!Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).isShowing()) {
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).show();
        }
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(getResources().getString(R.string.action_my_profile));
        super.onResume();
    }
}

