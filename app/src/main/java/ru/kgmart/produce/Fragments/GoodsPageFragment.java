package ru.kgmart.produce.Fragments;

import android.annotation.SuppressLint;
import android.app.FragmentManager;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Adapters.ImageHorizontalAdapter;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Components.CommonItemSpaceDecoration;
import ru.kgmart.produce.Components.GoodEnum;
import ru.kgmart.produce.Components.GoodsComponents;
import ru.kgmart.produce.Dialogs.DialogManager;
import ru.kgmart.produce.Models.GoodsAdapterModel;
import ru.kgmart.produce.Models.GoodsModel;
import ru.kgmart.produce.Models.ItemAvailableModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.R;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsPageFragment extends Fragment {

    private static final String TAG = "GoodsPageFragments";
    String jsonMyObject = null;
    String type = null;
    String image_url = null;
    boolean isMain = false;
    String selectColor = null;

    TextView goods_name;
    TextView goods_color;
    TextView goods_size;
    TextView goods_id;
    TextView goods_number_in_line;
    TextView goods_stock;
    TextView goods_price;
    TextView goods_count_carry;
    TextView good_price_discount;
    TextView goods_planned_date;
    RecyclerView rv_goods_images;

    LinearLayout ll_to_carry;
    LinearLayout ll_actions;
    LinearLayout ll_planned_date;
    RelativeLayout good_price_layout;

    ArrayList<String> imageList = new ArrayList<>();
    ArrayList<String> imageThumbList = new ArrayList<>();

    ScrollView sv_good;
    View good_bg;

    Button btn_no_available;
    Button btn_available;

    MaterialDialog.Builder dialog;

    ProductsModels model;
    GoodsAdapterModel full_model;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.item_good_card, container, false);

        Crashlytics.setString("fragment", "GoodsPageFragment");

        rv_goods_images = (RecyclerView) v.findViewById(R.id.rv_goods_images);
        goods_name = (TextView) v.findViewById(R.id.goods_name);
        goods_color = (TextView) v.findViewById(R.id.goods_color);
        goods_size = (TextView) v.findViewById(R.id.goods_size);
        goods_id = (TextView) v.findViewById(R.id.goods_id);
        goods_number_in_line = (TextView) v.findViewById(R.id.goods_number_in_line);
        goods_stock = (TextView) v.findViewById(R.id.goods_stock);
        goods_price = (TextView) v.findViewById(R.id.good_price);
        good_price_discount = (TextView) v.findViewById(R.id.good_price_discount);
        goods_count_carry = (TextView) v.findViewById(R.id.goods_count_carry);
        goods_planned_date = (TextView) v.findViewById(R.id.goods_planned_date);

        ll_to_carry = (LinearLayout) v.findViewById(R.id.ll_to_carry);
        ll_actions = (LinearLayout) v.findViewById(R.id.ll_actions);
        ll_planned_date = (LinearLayout) v.findViewById(R.id.ll_planned_date);
        good_price_layout = (RelativeLayout) v.findViewById(R.id.good_price_layout);

        btn_available = (Button) v.findViewById(R.id.btn_available);
        btn_no_available = (Button) v.findViewById(R.id.btn_no_available);

        sv_good = (ScrollView) v.findViewById(R.id.sv_good);
        good_bg = (View) v.findViewById(R.id.bg_good);

        btn_available.setOnClickListener(checkAvailable);
        btn_no_available.setOnClickListener(checkAvailable);

        Bundle extras = getArguments();
        if (extras != null) {
            jsonMyObject = extras.getString("model");
            type = extras.getString("type");
        }

        sv_good.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            private boolean alphaFull = false;

            @Override
            public void onScrollChanged() {
                int scrollY = sv_good.getScrollY();
                if (rv_goods_images != null) {
                    float alphaRatio;
                    if (rv_goods_images.getHeight() > scrollY) {
                        rv_goods_images.setTranslationY(scrollY / 2);
                        alphaRatio = (float) scrollY / rv_goods_images.getHeight();
                    } else {
                        alphaRatio = 1;
                    }
                    if (alphaFull) {
                        if (alphaRatio <= 0.99) alphaFull = false;
                    } else {
                        if (alphaRatio >= 0.95) alphaFull = true;
                        good_bg.setAlpha(alphaRatio);
                    }
                }
            }
        });


        if (jsonMyObject != null) {
            full_model = new Gson().fromJson(jsonMyObject, GoodsAdapterModel.class);
            model = full_model.getProductsModels();

            goods_name.setText(model.getName());

            if (model.getSizes().get(0) != null) {
                goods_size.setText(model.getSizes().get(0).getName());
            }
            if (model.getColors().get(0) != null) {
                goods_color.setText(model.getColors().get(0).getName());
            }
            good_price_layout.setVisibility(View.VISIBLE);
            if (type.equals(String.valueOf(GoodEnum.TYPE_CONSIGN)) && full_model.getWarehouseModel() != null) {
                goods_color.setText(full_model.getWarehouseModel().getColorName());
                goods_size.setText(full_model.getWarehouseModel().getSizeName());
                selectColor = full_model.getWarehouseModel().getColorId();
                image_url = GoodsComponents.getImageByColor(model.getColors(), full_model.getWarehouseModel().getColorId());

                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), full_model.getWarehouseModel().getColorId()));
                imageThumbList.addAll(GoodsComponents.getColorThumbImageList(model.getColors(), model.getId(), full_model.getWarehouseModel().getColorId()));

                goods_count_carry.setText(String.valueOf(full_model.getWarehouseModel().getNeedToBring()) + " ЛР");
                ll_to_carry.setVisibility(View.VISIBLE);

                good_price_layout.setVisibility(View.GONE);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_AVAILABLE)) && full_model.getItemAvailableModel() != null) {
                image_url = GoodsComponents.getImageByColor(model.getColors(), full_model.getItemAvailableModel().getColorId());
                goods_color.setText(full_model.getItemAvailableModel().getColorName());
                goods_size.setText(full_model.getItemAvailableModel().getSizeName());
                selectColor = full_model.getItemAvailableModel().getColorId();

                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), full_model.getItemAvailableModel().getColorId()));
                imageThumbList.addAll(GoodsComponents.getColorThumbImageList(model.getColors(), model.getId(), full_model.getItemAvailableModel().getColorId()));

                goods_count_carry.setText(full_model.getItemAvailableModel().getQuantity() + " ЛР");
                ll_to_carry.setVisibility(View.VISIBLE);
                good_price_layout.setVisibility(View.GONE);
                ll_actions.setVisibility(View.VISIBLE);
            }
            if (type.equals(String.valueOf(GoodEnum.TYPE_TO_CARRY)) && full_model.getReceptionModel() != null) {
                image_url = GoodsComponents.getImageByColor(model.getColors(), full_model.getReceptionModel().getColorId());
                goods_color.setText(full_model.getReceptionModel().getColorName());
                goods_size.setText(full_model.getReceptionModel().getSizeName());
                selectColor = full_model.getReceptionModel().getColorId();

                imageList.addAll(GoodsComponents.getColorFullImageList(model.getColors(), model.getId(), full_model.getReceptionModel().getColorId()));
                imageThumbList.addAll(GoodsComponents.getColorThumbImageList(model.getColors(), model.getId(), full_model.getReceptionModel().getColorId()));

                goods_count_carry.setText(full_model.getReceptionModel().getQuantity() + " ЛР");
                ll_to_carry.setVisibility(View.VISIBLE);

                ll_planned_date.setVisibility(View.VISIBLE);
                goods_planned_date.setText(full_model.getReceptionModel().getPlanedDate());
                good_price_layout.setVisibility(View.GONE);
            }

            goods_id.setText(model.getId());

            goods_number_in_line.setText(model.getNumberInLine() + " ед.");

            if (model.getStatusVisible() != null) {
                goods_stock.setText(GoodsComponents.getStockLabel(model.getStatusVisible()));
            } else {
                goods_stock.setText(GoodsComponents.getStockLabel(model.getVisible()));
            }

            if (model.getSellOut().equals("1")) {
                goods_price.setText(String.valueOf(model.getSelloutPrice()));
                goods_price.setPaintFlags(goods_price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                good_price_discount.setText(String.valueOf(model.getPrice()));
            } else {
                goods_price.setText(String.valueOf(model.getPrice()));
            }

            if (image_url == null) {
                image_url = model.getMainImage();
                isMain = true;
            }
            if (imageList.size() == 0) {
                imageList.addAll(GoodsComponents.getMainFullImageList(model.getMainImage(), model.getImages(), model.getId()));
                imageThumbList.addAll(GoodsComponents.getMainThumbImageList(model.getMainImage(), model.getImages()));
            }

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);

            rv_goods_images.setLayoutManager(layoutManager);
            rv_goods_images.addItemDecoration(new CommonItemSpaceDecoration(5));
            rv_goods_images.setAdapter(new ImageHorizontalAdapter(getActivity(), imageThumbList, imageList));

        }

        return v;
    }

    View.OnClickListener checkAvailable = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (v.getTag().equals("0")) {
                dialog = DialogManager.goodAvailableDialog(getContext(), "Вы уверены что товара не будет в наличии");
            } else {
                sendAvailable(v);
            }

            dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    sendAvailable(v);
                    dialog.dismiss();
                }
            });
            dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    };

    private void sendAvailable(View v) {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        GoodsModel goodsModel = new GoodsModel();
        ItemAvailableModel modelAvailable = new ItemAvailableModel();
        if (full_model.getItemAvailableModel().getColorId() != null)
            modelAvailable.setColorId(full_model.getItemAvailableModel().getColorId());
        modelAvailable.setItemId(full_model.getItemAvailableModel().getItemId());
        modelAvailable.setProductId(model.getId());
        modelAvailable.setSizeId(full_model.getItemAvailableModel().getSizeId());
        modelAvailable.setStatus(v.getTag().toString());

        goodsModel.setItemAvailableModel(modelAvailable);

        Call<RestResponse<GoodsModel>> call = apiGoods.setAvailable(goodsModel, CommonComponents.getUserKey(getContext()));
        call.enqueue(new Callback<RestResponse<GoodsModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Response<RestResponse<GoodsModel>> response) {
                Log.d(TAG, "onResponse: code" + String.valueOf(response.code()));
                if (response.isSuccessful()) {
                    Toast.makeText(getContext(), "Запись успешно обновлена", Toast.LENGTH_SHORT).show();
                    Objects.requireNonNull(getActivity()).getSupportFragmentManager().popBackStack("Goods", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<GoodsModel>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: ", t);
            }
        });
    }
}
