package ru.kgmart.produce.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.fxn.pix.Pix;
import com.fxn.utility.PermUtil;
import com.google.gson.Gson;
import com.libaml.android.view.chip.ChipLayout;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.APi;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Models.CategoryModel;
import ru.kgmart.produce.Models.ColorModel;
import ru.kgmart.produce.Models.ProductShopItemModel;
import ru.kgmart.produce.Models.ProductShopModel;
import ru.kgmart.produce.Models.ProductsModels;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.SizeModel;
import ru.kgmart.produce.R;

/**
 * Created on 7/3/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */
public class GoodsFormFragment extends Fragment {

    private static final String TAG = "GoodsFormFragment";
    String id = null;
    String jsonMyObject = null;

    String[] data = {"1", "2", "4", "6", "8", "10", "20"};
    String[] dataStatus = {"В наличии", "Скоро будет", "Нет в наличии"};

    private static final int REQUEST_CODE = 1;
    private static final int REQUEST_CODE_IMAGE = 2;
    private static final int EXTERNAL_STORAGE = 10;

    List<String> colorsId = new ArrayList<>();
    List<String> sizesId = new ArrayList<>();

    List<String> colorNames = new ArrayList<>();
    List<String> sizeNames = new ArrayList<>();
    String categoryId = null;


    EditText et_product_name;
    AutoCompleteTextView et_category;
    EditText et_product_price;
    EditText et_product_wholesale;
    EditText et_product_trade_price;
    ChipLayout et_product_colors;
    ChipLayout et_product_sizes;
    EditText et_product_weight;
    Spinner et_product_status;
    EditText et_product_description;

    Button sendProduct;
    LinearLayout ll_product_image;
    ImageView iv_product_image;

    TextView et_product_in_line;
    TextView et_product_colors_error;
    TextView et_product_sizes_error;

    int count_model = 1;

    ImageView btn_add_count_model;
    ImageView btn_remove_count_model;

    FloatingActionButton product_upload;
    Uri selectedUri;

    String product_name;
    String product_category;
    String product_price;
    String product_colors;
    String product_sizes;
    String product_weight;
    String product_description;
    String product_status;
    String product_wholesale;
    String product_trade_price;
    String product_number_in_line;

    ArrayAdapter<String> adapterCategory;
    ArrayAdapter<String> adapterColor;
    ArrayAdapter<String> adapterSize;

    List<String> category = new ArrayList<String>();
    List<String> categoryResult = new ArrayList<String>();
    List<String> colors = new ArrayList<>();
    List<String> sizes = new ArrayList<>();

    List<SizeModel> sizesModel = new ArrayList<>();
    List<ColorModel> colorsModel = new ArrayList<>();
    List<CategoryModel> categoryModel = new ArrayList<>();

    ProductsModels models;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_goods_form, container, false);
        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Создать новый товар");
        Crashlytics.setString("fragment", "GoodsFormFragment");

        et_product_name = (EditText) v.findViewById(R.id.et_product_name);
        et_category = (AutoCompleteTextView) v.findViewById(R.id.et_category);
        et_product_price = (EditText) v.findViewById(R.id.et_product_price);
        et_product_wholesale = (EditText) v.findViewById(R.id.et_product_wholesale);
        et_product_trade_price = (EditText) v.findViewById(R.id.et_product_trade_price);
        et_product_colors = (ChipLayout) v.findViewById(R.id.et_product_colors);
        et_product_sizes = (ChipLayout) v.findViewById(R.id.et_product_sizes);
        et_product_weight = (EditText) v.findViewById(R.id.et_product_weight);
        et_product_status = (Spinner) v.findViewById(R.id.et_product_status);
        et_product_description = (EditText) v.findViewById(R.id.et_product_description);

        et_product_in_line = (TextView) v.findViewById(R.id.et_product_in_line);
        et_product_colors_error = (TextView) v.findViewById(R.id.et_product_colors_error);
        et_product_sizes_error = (TextView) v.findViewById(R.id.et_product_sizes_error);

        product_upload = (FloatingActionButton) v.findViewById(R.id.product_upload);
        ll_product_image = (LinearLayout) v.findViewById(R.id.ll_product_image);
        iv_product_image = (ImageView) v.findViewById(R.id.iv_product_image);

        sendProduct = (Button) v.findViewById(R.id.sendProduct);

        btn_add_count_model = (ImageView) v.findViewById(R.id.btn_add_count_model);
        btn_remove_count_model = (ImageView) v.findViewById(R.id.btn_remove_count_model);
        btn_add_count_model.setOnClickListener(onClickCountModel);
        btn_remove_count_model.setOnClickListener(onClickCountModel);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        ArrayAdapter<String> adapterStatus = new ArrayAdapter<String>(Objects.requireNonNull(getActivity()), android.R.layout.simple_spinner_item, dataStatus);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        et_product_status.setAdapter(adapterStatus);
        et_product_status.setPrompt(getActivity().getResources().getString(R.string.label_status_model));
        et_product_status.setSelection(0);
        product_status = "0";

        try {
            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    uploadData();
                }
            });

        } catch (Exception e) {
            Crashlytics.logException(e);
        }

        et_product_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                product_status = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Bundle extras = getArguments();
        if (extras != null) {
            id = extras.getString("product_id");
            jsonMyObject = extras.getString("model");
        }
        if (id != null && !TextUtils.isEmpty(id)) {
            Crashlytics.setString("product_id", id);
            try {
                initData(id);
            } catch (Exception e) {
                Log.e(TAG, "onCreateView: error ", e);
                Crashlytics.logException(e);
            }
            Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle("Изменить");
            sendProduct.setText("Изменить");
        }

        product_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermissions()) {
                    onPermissoinsGranted();
                }
            }
        });

        sendProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendProduct.setClickable(false);
                checkFields();
            }
        });

        adapterColor = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, colors);
        et_product_colors.setAdapter(adapterColor);

        et_product_colors.setOnChipItemChangeListener(new ChipLayout.ChipItemChangeListener() {
            @Override
            public void onChipAdded(int pos, String txt) {
                addColor(txt);
            }

            @Override
            public void onChipRemoved(int pos, String txt) {
                removeColor(txt);
            }
        });

        adapterSize = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, sizes);
        et_product_sizes.setAdapter(adapterSize);

        et_product_sizes.setOnChipItemChangeListener(new ChipLayout.ChipItemChangeListener() {
            @Override
            public void onChipAdded(int pos, String txt) {
                addSize(txt);
            }

            @Override
            public void onChipRemoved(int pos, String txt) {
                removeSize(txt);
            }
        });

        adapterCategory = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, categoryResult);

        et_category.setAdapter(adapterCategory);
        et_category.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (s == null) {
                    categoryResult.addAll(category);
                    adapterCategory.notifyDataSetChanged();
                } else {
                    categoryResult.clear();
                    for (int i = 0; i < category.size(); i++) {
                        if (category.get(i) != null && !TextUtils.isEmpty(category.get(i))) {
                            if (category.get(i).toLowerCase().contains(String.valueOf(s).toLowerCase())) {
                                categoryResult.add(category.get(i));
                            }
                        }
                    }
                    Log.d(TAG, "beforeTextChanged: " + String.valueOf(categoryResult.size()));
                    adapterCategory.notifyDataSetChanged();
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        et_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                categoryId = getCategoryId(adapterView.getItemAtPosition(i).toString().trim());
            }
        });

        return v;
    }

    private void addSize(String size) {
        for (int i = 0; i < sizesModel.size(); i++) {
            if (sizesModel.get(i) != null && !TextUtils.isEmpty(sizesModel.get(i).getName())) {
                if (sizesModel.get(i).getName().toLowerCase().equals(size.toLowerCase())) {
                    sizesId.add(sizesModel.get(i).getId());
                    break;
                }
            }
        }
    }

    private void addColor(String color) {
        for (int i = 0; i < colorsModel.size(); i++) {
            if (colorsModel.get(i) != null && !TextUtils.isEmpty(colorsModel.get(i).getName())) {
                if (colorsModel.get(i).getName().toLowerCase().equals(color.toLowerCase())) {
                    colorsId.add(colorsModel.get(i).getId());
                    break;
                }
            }
        }
    }

    private void removeSize(String position) {
        Integer id = -1;
        for (int i = 0; i < sizesModel.size(); i++) {
            if (sizesModel.get(i) != null && !TextUtils.isEmpty(sizesModel.get(i).getName())) {
                if (sizesModel.get(i).getName().toLowerCase().equals(position.toLowerCase())) {
                    id = Integer.valueOf(sizesModel.get(i).getId());
                }
            }
        }
        if (id > -1) {
            for (int j = 0; j < sizesId.size(); j++) {
                if (sizesId.get(j).equals(id)) {
                    sizesId.remove(j);
                }
            }
        }
    }

    private void removeColor(String position) {
        Integer id = -1;
        for (int i = 0; i < colorsModel.size(); i++) {
            if (colorsModel.get(i) != null && !TextUtils.isEmpty(colorsModel.get(i).getName())) {
                if (colorsModel.get(i).getName().toLowerCase().equals(position.toLowerCase())) {
                    id = Integer.valueOf(colorsModel.get(i).getId());
                }
            }
        }
        if (id > -1) {
            for (int j = 0; j < colorsId.size(); j++) {
                if (colorsId.get(j).equals(id)) {
                    colorsId.remove(j);
                }
            }
        }
    }


    private String getCategoryId(String string) {
        for (int i = 0; i < categoryModel.size(); i++) {
            if (categoryModel.get(i) != null && !TextUtils.isEmpty(categoryModel.get(i).getTitle())) {
                if (categoryModel.get(i).getTitle().toLowerCase().equals(string.toLowerCase())) {
                    return categoryModel.get(i).getId();
                }
            }
        }
        return string;
    }


    private void uploadData() {
        APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);

        Call<RestResponse<List<SizeModel>>> callSize = apiOther.getSizes();
        callSize.enqueue(new Callback<RestResponse<List<SizeModel>>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<List<SizeModel>>> call, @NonNull Response<RestResponse<List<SizeModel>>> response) {
                RestResponse<List<SizeModel>> listRestResponse = response.body();
                RestResponse.Meta meta = Objects.requireNonNull(listRestResponse).getMeta();
                if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                    sizesModel.addAll(listRestResponse.getData());
                    for (int i = 0; i < sizesModel.size(); i++) {
                        sizes.add(sizesModel.get(i).getName());
                    }
                    adapterSize.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<List<SizeModel>>> call, @NonNull Throwable t) {

            }
        });

        Call<RestResponse<List<CategoryModel>>> callCategory = apiOther.getCategory();
        callCategory.enqueue(new Callback<RestResponse<List<CategoryModel>>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<List<CategoryModel>>> call, @NonNull Response<RestResponse<List<CategoryModel>>> response) {
                RestResponse<List<CategoryModel>> listRestResponse = response.body();
                RestResponse.Meta meta = Objects.requireNonNull(listRestResponse).getMeta();

                if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                    categoryModel.addAll(listRestResponse.getData());
                    for (int i = 0; i < categoryModel.size(); i++) {
                        if (categoryModel.get(i).getTitleH1() != null && !TextUtils.isEmpty(categoryModel.get(i).getTitleH1())) {
                            category.add(categoryModel.get(i).getTitleH1());
                            categoryResult.add(categoryModel.get(i).getTitleH1());
                        } else {
                            category.add(categoryModel.get(i).getTitle());
                            categoryResult.add(categoryModel.get(i).getTitle());
                        }

                    }
                    adapterCategory.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<List<CategoryModel>>> call, @NonNull Throwable t) {
                Log.e(TAG, "onFailure: category", t);
            }
        });

        Call<RestResponse<List<ColorModel>>> callColors = apiOther.getColors();
        callColors.enqueue(new Callback<RestResponse<List<ColorModel>>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<List<ColorModel>>> call, @NonNull Response<RestResponse<List<ColorModel>>> response) {
                RestResponse<List<ColorModel>> listRestResponse = response.body();
                RestResponse.Meta meta = Objects.requireNonNull(listRestResponse).getMeta();
                if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                    colorsModel.addAll(listRestResponse.getData());
                    for (int i = 0; i < colorsModel.size(); i++) {
                        colors.add(colorsModel.get(i).getName());
                    }
                    adapterColor.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<List<ColorModel>>> call, @NonNull Throwable t) {

            }
        });
    }

    private void checkFields() {
        et_product_name.setError(null);
        et_product_price.setError(null);
        et_product_colors_error.setVisibility(View.GONE);
        et_product_sizes_error.setVisibility(View.GONE);

        boolean cancel = false;
        View focusView = null;

        product_name = et_product_name.getText().toString().trim();
        product_category = et_category.getText().toString().trim();
        product_price = et_product_price.getText().toString().trim();
        product_colors = et_product_colors.getText().toString().trim();
        product_sizes = et_product_sizes.getText().toString().trim();
        product_weight = et_product_weight.getText().toString().trim();
        product_description = et_product_description.getText().toString().trim();
        product_wholesale = et_product_wholesale.getText().toString().trim();
        product_trade_price = et_product_trade_price.getText().toString().trim();
        product_number_in_line = et_product_in_line.getText().toString().trim();

        if (TextUtils.isEmpty(product_name)) {
            et_product_name.setError("Это поле обязательно");
            focusView = et_product_name;
            cancel = true;
        } else if (TextUtils.isEmpty(product_category) && TextUtils.isEmpty(categoryId)) {
            et_category.setError("Это поле обязательно");
            focusView = et_category;
            cancel = true;
        } else if (TextUtils.isEmpty(product_price)) {
            et_product_price.setError("Это поле обязательно");
            focusView = et_product_price;
            cancel = true;
        } else if (TextUtils.isEmpty(product_colors)) {
            et_product_colors_error.setVisibility(View.VISIBLE);
            focusView = et_product_colors;
            cancel = true;
        } else if (TextUtils.isEmpty(product_sizes)) {
            et_product_sizes_error.setVisibility(View.VISIBLE);
            focusView = et_product_sizes;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
            sendProduct.setClickable(true);
        } else {
            isChangeFields();
        }
    }

    private void isChangeFields() {
        if (id != null && !TextUtils.isEmpty(id)) {
            boolean changes = false;
            if (!product_name.equals(models.getName())) {
                changes = true;
            }
            if (!categoryId.equals(models.getCategoryId())) {
                changes = true;
            }
            if (!product_price.equals(String.valueOf(models.getPrice()))) {
                changes = true;
            }
            if (!product_number_in_line.equals(models.getNumberInLine())) {
                changes = true;
            }
            if (!product_wholesale.equals(models.getWholesale())) {
                changes = true;
            }
            if (!product_trade_price.equals(models.getTradePrice())) {
                changes = true;
            }
            if (!product_weight.equals(models.getWeight())) {
                changes = true;
            }
            if (!product_status.equals(models.getVisible())) {
                changes = true;
            }
            if (!product_description.equals(models.getDescription())) {
                changes = true;
            }
            if (colorsId != null && models.getColors() != null) {
                if (colorsId.size() != models.getColors().size()) {
                    changes = true;
                } else {
                    for (int i = 0; i < colorsId.size(); i++) {
                        if (!colorsId.contains(models.getColors().get(i).getId())) {
                            changes = true;
                        }
                    }
                }
            }
            if (sizesId != null && models.getSizes() != null) {
                if (sizesId.size() != models.getSizes().size()) {
                    changes = true;
                } else {
                    for (int i = 0; i < sizesId.size(); i++) {
                        if (!sizesId.contains(models.getSizes().get(i).getId())) {
                            changes = true;
                        }
                    }
                }
            }
            sendProductData();
        } else {
            sendProductData();
        }
    }


    private void sendProductData() {
        APi.ApiGoods apiGoods = APi.getRetrofitBuild().create(APi.ApiGoods.class);
        Map<String, RequestBody> product = initProductData();
        List<MultipartBody.Part> params = new ArrayList<>();
        if (selectedUri != null) {
            File file = new File(selectedUri.getPath());
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("main_image", file.getName(), requestFile);
            params.add(body);
        }
        String msg = "Товар будет добавлен в скором времени";
        if (id != null && !TextUtils.isEmpty(id)) {
            msg = "Запрос на обновление товара отправлен";
        }
        Call<RestResponse<ProductShopModel>> call = apiGoods.createGoods(CommonComponents.getUserKey(getContext()), product, params);
        String finalMsg = msg;
        call.enqueue(new Callback<RestResponse<ProductShopModel>>() {
            @Override
            public void onResponse(@NonNull Call<RestResponse<ProductShopModel>> call, @NonNull Response<RestResponse<ProductShopModel>> response) {
                if (response.code() == 200) {
                    Toast.makeText(getContext(), finalMsg, Toast.LENGTH_LONG).show();
                    FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
                    fragmentManager.popBackStack();
                }
                sendProduct.setClickable(true);
            }

            @Override
            public void onFailure(@NonNull Call<RestResponse<ProductShopModel>> call, @NonNull Throwable t) {
                Toast.makeText(getContext(), "Что-то пошло не так. Попробуйте позже", Toast.LENGTH_LONG).show();
                sendProduct.setClickable(true);
                Crashlytics.logException(t);
            }
        });
    }

    private Map<String, RequestBody> initProductData() {
        Map<String, RequestBody> product = new HashMap<>();

        Gson gson = new Gson();
        ProductShopItemModel item = new ProductShopItemModel();

        String cat = getCategoryId(et_category.getText().toString());
        if (!TextUtils.isEmpty(cat)) {
            categoryId = cat;
        }

        item.setName(product_name);
        item.setCategory_id(categoryId);
        item.setPrice(product_price);
        item.setNumber_in_line(product_number_in_line);
        if (!TextUtils.isEmpty(product_wholesale)) {
            item.setWholesale(product_wholesale);
        }
        if (!TextUtils.isEmpty(product_trade_price)) {
            item.setTrade_price(product_trade_price);
        }
        if (!TextUtils.isEmpty(product_weight)) {
            item.setWeight(product_weight);
        }
        item.setColors(colorsId);
        item.setSizes(sizesId);
        item.setVisible(product_status);
        item.setDescription(product_description);

        if (id != null && !TextUtils.isEmpty(id)) {
            item.setProduct_id(id);
        }

        String customDishListString = gson.toJson(item);

        product.put("ProductShop", RequestBody.create(MediaType.parse("text/plain"), customDishListString));

        return product;
    }

    private boolean checkPermissions() {
        if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                ) {
            return true;
        } else {
            requestPermissions();
            return false;
        }
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                EXTERNAL_STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PermUtil.REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPermissoinsGranted();
                } else {
                    Toast.makeText(getActivity(), "Потвердите разрешение чтобы добавить фотографию", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private void onPermissoinsGranted() {
        Pix.start(Objects.requireNonNull(getActivity()), REQUEST_CODE_IMAGE, 1);
    }


    private void initData(String id) {
        if (jsonMyObject != null) {
            models = new Gson().fromJson(jsonMyObject, ProductsModels.class);
            et_product_name.setText(models.getName());
            categoryId = models.getCategoryId();
            et_product_price.setText(String.valueOf(models.getPrice()));

            et_product_wholesale.setText(models.getWholesale());
            et_product_trade_price.setText(models.getTradePrice());
            if (models.getColors() != null && models.getColors().size() > 0) {
                for (int i = 0; i < models.getColors().size(); i++) {
                    colorNames.add(models.getColors().get(i).getName());
                    colorsId.add(models.getColors().get(i).getId());
                }
            }
            if (models.getSizes() != null && models.getSizes().size() > 0) {
                for (int i = 0; i < models.getSizes().size(); i++) {
                    sizeNames.add(models.getSizes().get(i).getName());
                    sizesId.add(models.getSizes().get(i).getId());
                }
            }
            et_product_in_line.setText(String.valueOf(models.getNumberInLine()));
            count_model = Integer.valueOf(models.getNumberInLine());

            et_product_colors.setText(colorNames);
            et_product_sizes.setText(sizeNames);

            et_product_weight.setText(models.getWeight());
            et_product_description.setText(models.getDescription());

            if (TextUtils.isEmpty(et_category.getText())) {
                et_category.setText(String.valueOf(models.getCategoryName()));
            }
            if (models.getVisible().equals("2")) {
                et_product_status.setSelection(2);
                product_status = "2";
            } else {
                et_product_status.setSelection(0);
                product_status = "0";
            }
        }
    }

    View.OnClickListener onClickCountModel = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            count_model = Integer.valueOf(et_product_in_line.getText().toString());

            if (count_model > 1) {
                if (v.getTag().equals("remove")) {
                    count_model = count_model - 1;
                    et_product_in_line.setText(String.valueOf(count_model));
                    return;
                }
            }

            if (count_model <= 1000) {
                if (v.getTag().equals("add")) {
                    count_model = count_model + 1;
                    et_product_in_line.setText(String.valueOf(count_model));
                    return;
                }
            }
        }
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_IMAGE) {
            ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);
            if (returnValue != null && returnValue.size() > 0) {
                ll_product_image.setVisibility(View.VISIBLE);
                selectedUri = Uri.parse(new File(returnValue.get(0)).toString());
                iv_product_image.setImageURI(selectedUri);
            }
        }
    }
}
