package ru.kgmart.produce;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.Objects;

import ru.kgmart.produce.Fragments.FaqFragment;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class FaqActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setTitle(getResources().getString(R.string.action_faq));
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(true);

        FaqFragment faqFragment = new FaqFragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainFrame, faqFragment, "Faq")
                .commit();
    }

}
