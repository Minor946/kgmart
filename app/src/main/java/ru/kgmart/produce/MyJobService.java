package ru.kgmart.produce;


import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;

/**
 * Created on 4/24/18.
 *
 * @author Minor946 (minor946@yandex.com)
 */

public class MyJobService extends JobService {

  @Override
  public boolean onStartJob(JobParameters job) {
    return false;
  }

  @Override
  public boolean onStopJob(JobParameters job) {
    return false;
  }
}
