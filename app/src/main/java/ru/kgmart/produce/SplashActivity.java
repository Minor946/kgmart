package ru.kgmart.produce;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.perf.metrics.AddTrace;

import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.kgmart.produce.Components.CommonComponents;
import ru.kgmart.produce.Models.NotificationModel;
import ru.kgmart.produce.Models.RestResponse;
import ru.kgmart.produce.Models.UserLoginModel;

/**
 * Created by Minor946 (minor946@yandex.com) on 4/17/18.
 */
public class SplashActivity extends AppCompatActivity {

    private static final String TAG = "SplashActivityTest";
    SharedPreferences getPrefs;
    boolean isFirstStart;
    boolean isLogIn;

    @Override
    @AddTrace(name = "onCreateTraceSplash", enabled = true)
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getApplicationContext();
        getPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        isFirstStart = getPrefs.getBoolean("APP_PREFERENCES_FIRST_START", true);
        isLogIn = getPrefs.getBoolean("APP_PREFERENCES_LOGIN_STATUS", false);
        init();
    }

    private void init() {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                if (isFirstStart) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startLoginActivity();
                        }
                    });
                } else {
                    if (!isLogIn) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                startLoginActivity();
                            }
                        });
                    } else {
                        APi.ApiOther apiOther = APi.getRetrofitBuild().create(APi.ApiOther.class);
                        Call<RestResponse<NotificationModel>> call = apiOther.getNotification(CommonComponents.getUserKey(getApplicationContext()));
                        call.enqueue(new Callback<RestResponse<NotificationModel>>() {
                            @Override
                            public void onResponse(@NonNull Call<RestResponse<NotificationModel>> call, @NonNull Response<RestResponse<NotificationModel>> response) {
                                if (response.isSuccessful()) {
                                    RestResponse<NotificationModel> notificationModel = response.body();
                                    RestResponse.Meta meta = Objects.requireNonNull(notificationModel).getMeta();
                                    if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                                        openMain();
                                    } else {
                                        reLogin();
                                    }
                                } else {
                                    reLogin();
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<RestResponse<NotificationModel>> call, @NonNull Throwable t) {
                                Log.e(TAG, "onFailure: ", t);
                                reLogin();
                            }
                        });
                    }
                }
            }
        });
        t.start();
    }

    private void reLogin() {

        APi.Auth apiAuth = APi.getRetrofitBuild().create(APi.Auth.class);
        String email = getPrefs.getString("APP_PREFERENCES_LOGIN", null);
        String password = getPrefs.getString("APP_PREFERENCES_PASSWORD", null);
        if (email != null && !TextUtils.isEmpty(email) && password != null && !TextUtils.isEmpty(password)) {
            Call<RestResponse<UserLoginModel>> authModelCall = apiAuth.login(email.trim(), CommonComponents.md5(password).trim(), "produce_app");
            authModelCall.enqueue(new Callback<RestResponse<UserLoginModel>>() {
                @Override
                public void onResponse(@NonNull Call<RestResponse<UserLoginModel>> call, @NonNull Response<RestResponse<UserLoginModel>> response) {
                    if (response.isSuccessful()) {
                        RestResponse<UserLoginModel> authData = response.body();
                        RestResponse.Meta meta = Objects.requireNonNull(authData).getMeta();

                        if (meta.getStatus().equals(RestResponse.Status.SUCCESS)) {
                            SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(Objects.requireNonNull(getApplicationContext()));
                            SharedPreferences.Editor e = getPrefs.edit();
                            UserLoginModel userData = authData.getData();
                            e.putBoolean("APP_PREFERENCES_LOGIN_STATUS", true);
                            e.putBoolean("APP_PREFERENCES_FIRST_START", false);
                            e.putString("APP_PREFERENCES_LOGIN", email);
                            e.putString("APP_PREFERENCES_PASSWORD", password);
                            e.putString("APP_PREFERENCES_ACCESS_TOKEN", authData.getData().getKey());
                            e.putString("APP_PREFERENCES_USERNAME", userData.getUser().getUsername());
                            e.putString("APP_PREFERENCES_BALANCE", String.valueOf(userData.getUser().getBalance()));
                            e.putString("APP_PREFERENCES_ID", userData.getUser().getId());
                            e.putString("APP_PREFERENCES_EMAIL", userData.getUser().getEmail());
                            e.apply();
                            openMain();
                        } else {
                            startLoginActivity();
                        }
                    } else {
                        try {
                            JSONObject jObjError = new JSONObject(Objects.requireNonNull(response.errorBody()).string());
                            String msgError = jObjError.getJSONObject("meta").getString("message");
                            if (msgError != null) {
                                Toast.makeText(getApplicationContext(), msgError, Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Log.e(TAG, "onResponse: 401 error" + e.getMessage());
                        }
                        startLoginActivity();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RestResponse<UserLoginModel>> call, @NonNull Throwable t) {
                    startLoginActivity();
                }
            });
        } else {
            startLoginActivity();
        }
    }

    private void openMain() {
        startActivity(new Intent(SplashActivity.this, MainActivity.class));
    }

    private void startLoginActivity() {
        final Intent i = new Intent(SplashActivity.this, LoginActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
